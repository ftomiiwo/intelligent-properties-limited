<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserUpline extends Model
{
    protected $fillable = [
        'user_id', 'upline_id',
        ];
    //
}
