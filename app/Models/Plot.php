<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plot extends Model
{
    protected $fillable = [
        'name', 'project_id', 'price', 'one_time_price', 'initial_deposit', 'status_id',
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

//    public function project_plans()
//    {
//        return $this->hasMany(ProjectPlan::class, 'plot_id', 'id');
//    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }
}
