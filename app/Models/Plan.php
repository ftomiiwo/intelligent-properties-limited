<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    public function interval()
    {
        return $this->hasOne(Interval::class, 'id', 'interval_id');
    }
}
