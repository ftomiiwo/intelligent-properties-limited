<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'name', 'description', 'image', 'location', 'country', 'project_type_id', 'status_id', 'active'
    ];

    public function projecttype()
    {
        return $this->hasOne(ProjectType::class, 'id', 'project_type_id');
    }

    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    public function plot()
    {
        return $this->hasMany(Plot::class, 'id', 'project_id');
    }
}
