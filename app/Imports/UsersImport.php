<?php

namespace App\Imports;

use App\Helper;
use App\Mail\BulkAccountCreated;
use App\Mail\Welcome;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Http\Controllers\UserController;

class UsersImport implements ToModel, withHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $paystack = new UserController();
        $email = trim(strtolower($row['email']));
        $user = User::where('email', $email)->first();
        if(is_null($user)) {
            Log::info("Attempting to create a user record for {$email}.");
            $pk = $paystack->paystackusercreation($email);
            if($pk) {
                $password = Str::random(12);
                $user = new User([
                    'name' => $row['name'],
                    'email' => $email,
                    'phone' => $row['phone'],
                    'password' => Hash::make($password),
                    'paystack_customer_id' => $pk->id,
                    'paystack_customer_code' => $pk->customer_code,
                    'created_by_user_id' => 1,
                    'status_id' => 1,
                    'role_id' => 1,
                ]);
                // Send Email to User
                if($user) {
                    //send a verification email to the enrolled user.
                    $recipient_name = $user->name;
                    $d = [
                        'recipient_name' => $recipient_name,
                    ];
                    $configData = [
                        'sender_email'=> Helper::config('site_email'),
                        'sender_name'=> Helper::config('site_name'),
                        'recipient_email'=> $user->email,
                        'recipient_name'=> $user->name,
                        'subject'=> 'Welcome to '.Helper::config('site_name'),
                    ];

                    Mail::to($user->email)
                        ->send(new BulkAccountCreated($user, $password));
                    return $user;
                }
                Log::info("Created Record for User with Email {$user->email}");
                return $user;
            }
        } else {
            return $user;
        }
    }
}
