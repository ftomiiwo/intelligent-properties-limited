<?php

namespace App;

use App\Models\Payment;
use App\Models\Role;
use App\Models\Status;
use App\Models\Subscription;
use App\Models\Transaction;
use App\Models\UserUpline;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'address', 'gender', 'role_id', 'status_id', 'date_of_birth', 'nationality', 'state_of_origin', 'occupation',
        'next_of_kin_name', 'next_of_kin_address', 'next_of_kin_phone', 'avatar', 'paystack_customer_id', 'paystack_customer_code', 'created_by_user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    public function uplines()
    {
        return $this->hasOne(UserUpline::class, 'upline_id', 'created_by_user_id');
    }

    public function downlines()
    {
        return $this->hasMany(UserUpline::class, 'user_id', 'id');
    }

    public function verifyUser()
    {
        return $this->hasOne(VerifyUser::class, 'user_id', 'id');
    }

    /**
     * @param $password
     * @return string
     */
    public static function protectPassword($password){
        return Hash::make($password);
    }

    public function isAdmin(){
        if($this->role->id == 1){
            return false;
        }

        return true;
    }

    public function isRealtor(){
        if($this->role->id == 2) return true;
        return false;
    }

    public function isSuperAdmin(){
        if($this->role->id == 3) return true;
        return false;
    }

    public function isVerified(){
        if($this->status->id != 1) return false;
        return true;
    }
}
