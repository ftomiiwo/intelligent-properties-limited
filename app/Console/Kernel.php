<?php

namespace App\Console;

use App\Console\Commands\GenerateConfirmationPDFs;
use App\Console\Commands\sendInvoice;
use App\Console\Commands\verifyInvoice;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;


class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        sendInvoice::class,
        verifyInvoice::class,
        GenerateConfirmationPDFs::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('invoice:send')->everyFiveMinutes()->withoutOverlapping();
        $schedule->command('invoice:verify')->everyFiveMinutes()->withoutOverlapping();
        $schedule->command('confirmations:generate')->everyTenMinutes()->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
