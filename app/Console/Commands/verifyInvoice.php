<?php

namespace App\Console\Commands;

use App\Http\Controllers\Admin\ScheduleController;
use Illuminate\Console\Command;

class verifyInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoice:verify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command verifies all the Invoices that has been sent. it checks to know if payment has been made';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $invoices = new ScheduleController();
        $invoices->verifySentInvoices();
    }
}
