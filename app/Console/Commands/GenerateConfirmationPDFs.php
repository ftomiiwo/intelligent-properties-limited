<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Admin\PaymentController;

class GenerateConfirmationPDFs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'confirmations:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will generate all the Confirmation PDFs for all successful payments that has been made';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $payment = new PaymentController();
        $payment->createPendingConfirmations();
    }
}
