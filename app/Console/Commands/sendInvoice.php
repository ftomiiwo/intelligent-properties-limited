<?php

namespace App\Console\Commands;

use App\Http\Controllers\Admin\ScheduleController;
use Illuminate\Console\Command;

class sendInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoice:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command Sends all the Invoices that are scheduled for the current day.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $invoices = new ScheduleController();
        $invoices->sendDueInvoices();
    }
}
