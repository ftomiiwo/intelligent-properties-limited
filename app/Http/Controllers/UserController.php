<?php

namespace App\Http\Controllers;

use App\Helper;
use App\User;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\User  $model
     * @return \Illuminate\View\View
     */
    public function index(User $model)
    {
        return view('users.index');
    }

    /**
     * @param string $email
     * @return bool|string
     */
    public function createPaystackUser(string $email)
    {
        $curl = curl_init();
        $paystack_sk = Helper::config('paystack_secret_key');
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.paystack.co/customer",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array('email' => $email),
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer ".$paystack_sk,
                "Accept: application/json",
                "Cookie: __cfduid=d6d883614f424045c433b19facd09ed0d1589924969; sails.sid=s%3AalgIMROrdDOjz6x4ZeJP9bzXu_MR4gtT.ADc0gZ8YQliVzcLU%2BAspBU69N%2BHrTP%2B0Q9YFwULt8Ms"
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $responseJson = response()->json(json_decode($response));
        return json_decode($responseJson->content())->data;
    }

    public function paystackusercreation(string $email)
    {
        $curl = curl_init();
        $paystack_sk = Helper::config('paystack_secret_key');
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.paystack.co/customer",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array('email' => $email),
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer ".$paystack_sk,
                "Accept: application/json",
                "Cookie: __cfduid=d133b9b401d7eb701f938b6fc260b80a61593630200; sails.sid=s%3AR54SIO5H6Rz4wHHlZUriRnxgUDzAi79k.7AmDjeogjsLe%2FghS1fgcQ4O8rRjrtACG%2FxCufnBp0Lo"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        if($response) {
            $retData = json_decode($response);
            if($retData) {
                $retArray = $retData->data;
                if($retArray) {
                    return $retArray;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
