<?php

namespace App\Http\Controllers\Auth;

use App\Helper;
use App\Http\Controllers\Controller;
use App\Mail\Welcome;
use App\Models\UserUpline;
use App\Providers\RouteServiceProvider;
use App\User;
use App\VerifyUser;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\UserController;
use App\Http\Controllers\API\EmailController;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Admin\UsersController as AdminUsersController;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    protected function registered($user)
    {
        $this->guard()->logout();
        return redirect('/login')->with('status', 'A verification email has been sent to '.$user->email.'. Kindly check your email and click on the link to verify.');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     * @throws \SendGrid\Mail\TypeException
     */
    protected function create(array $data)
    {
        // Create Paystack User
        $paystack = new UserController();
        $pk = $paystack->createPaystackUser($data['email']);
        $customer_code = $pk->customer_code;
        $paystack_customer_id = $pk->id;
        $user_account = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'paystack_customer_code' => $customer_code,
            'paystack_customer_id' => $paystack_customer_id,
            'status_id' => 1,
            'role_id' => 1,
            'password' => Hash::make($data['password']),
        ]);

        if($user_account) {
            $upline = UserUpline::create([
               'user_id' => $user_account->id,
               'upline_id' => 1,
            ]);
            //send a verification email to the enrolled user.
            $recipient_name = $user_account->name;
            $d = [
                'recipient_name' => $recipient_name
            ];
            $configData = [
                'sender_email'=> Helper::config('site_email'),
                'sender_name'=> Helper::config('site_name'),
                'recipient_email'=> $user_account->email,
                'recipient_name'=> $user_account->name,
                'subject'=> 'Welcome'
            ];

//            EmailController::dispatchMail($configData, 'emails.welcome', $d);
            $auser = new AdminUsersController();
            $auser->verifyemail($user_account->id, $user_account->email, $user_account);

            return $this->registered($user_account)
                ?: redirect($this->redirectPath());

//            Mail::to($user_account->email)->send(new Welcome($user_account));
//            return back()->with('success', 'Please check your inbox for our welcome message.');
        }

    }



//    protected function registered(Request $request, $user)
//    {resend_signup_mail
//        //
//        $this->guard()->logout();
//        return redirect('/confirmmail')->with('status', 'A verification email has been sent to '.$user->email.'.Kindly check your email and click on the link to verify.');
//
//    }


}
