<?php

namespace App\Http\Controllers;

use App\Helper;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\PasswordRequest;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        return view('profile.edit');
    }

    /**
     * Show the form for editing the Password.
     *
     * @return \Illuminate\View\View
     */
    public function passwordEdit()
    {
        return view('profile.password');
    }
    /**
     * Update the profile
     *
     * @param  \App\Http\Requests\ProfileRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProfileRequest $request)
    {
        auth()->user()->update($request->all());

        return back()->withStatus(__('Profile successfully updated.'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changeProfilePicture(Request $request)
    {
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        if($request->hasFile('avatar')) {
            $request->validate([
                'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
            ]);
            $imageName = 'user_image_'.time().'.'.request()->avatar->getClientOriginalExtension();
            $request->avatar->storeAs('images/avatars', $imageName);
            $user->avatar = $imageName;
        }
        $user->save();
        if($user) {
            return redirect('/profile')->with('status', 'Profile Image updated successfully.');
        } else {
            return back()->with('error', 'Failed to update Profile Image.');
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function userNextOfKin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'next_of_kin_name' => ['required', 'string', 'max:255'],
            'next_of_kin_phone' => ['required', 'string', 'min:11'],
            'next_of_kin_address' => ['required', 'string'],
            ]
        );
        if ($validator->fails()) {
            return redirect('/profile')
                ->withErrors($validator)
                ->withInput();
        }
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        $user->next_of_kin_name = $request->post('next_of_kin_name');
        $user->next_of_kin_address = $request->post('next_of_kin_address');
        $user->next_of_kin_phone = $request->post('next_of_kin_phone');
        $user->save();

        if ($user) return redirect('/profile')->with('status', 'Next of Kin information updated successfully.');
        else back()->with('error', 'Failed to update Next of Kin information.');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function contact(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'address' => ['required', 'string', 'max:255'],
                'state_of_origin' => ['required', 'string'],
                'nationality' => ['required', 'string'],
            ]
        );
        if ($validator->fails()) {
            return redirect('/profile')
                ->withErrors($validator)
                ->withInput();
        }
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        $user->address = $request->post('address');
        $user->state_of_origin = $request->post('state_of_origin');
        $user->nationality = $request->post('nationality');
        $user->save();

        if ($user) return redirect('/profile')->with('status', 'Contact information updated successfully.');
        else back()->with('error', 'Failed to update Contact information.');
    }

    /**
     * Change the password
     *
     * @param  \App\Http\Requests\PasswordRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function password(PasswordRequest $request)
    {
        auth()->user()->update(['password' => Hash::make($request->get('password'))]);

        return back()->withPasswordStatus(__('Password successfully updated.'));
    }
}
