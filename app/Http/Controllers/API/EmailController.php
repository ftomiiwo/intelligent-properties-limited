<?php

namespace App\Http\Controllers\API;

use App\Helper;
use App\Models\Email;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Mail\MailNotify;
use App\User;
use App\UserCode;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use SendGrid\Mail\Attachment;
use SendGrid\Mail\TypeException;

class EmailController extends Controller
{
    /**
     * this is the SendGrid API key
     * @return string
     */
    private static function getKey(){
        return $api_key =  Helper::config('sendgrid_api_key');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * sends a copy of the Plentywaka database file
     */
    public static function sendDatabaseFile(){
        $data = [];
        $config = [
            'sender_email' => Helper::config('site_email'),
            'sender_name' => Helper::config('site_name'),
            'subject' => 'Database Backup',
            'recipient_email' => Helper::config('site_email'),
            'recipient_name' => Helper::config('site_name')
        ];

        try {
            self::dispatchMail($config, 'emails.db_mail', $data, true);

        } catch (TypeException $e) {}

    }

    /**
     * THIS IS A MULTIPURPOSE EMAIL DISPATCHER
     * @param array $emailConfig
     * @param string $bladeTemplate
     * @param array $bladeData
     * @param bool $withAttachment
     * @return bool
     * @throws \SendGrid\Mail\TypeException
     */
    public static function dispatchMail(array $emailConfig, string $bladeTemplate = '', $bladeData = [], $withAttachment = false){
        $api_key = self::getKey();

        $email = new \SendGrid\Mail\Mail();
        $email->setFrom($emailConfig['sender_email'], $emailConfig['sender_name']);
        $email->setSubject($emailConfig['subject']);
        $email->addTo($emailConfig['recipient_email'], $emailConfig['recipient_name']);

        if(!$withAttachment){
            $view = View::make($bladeTemplate, $bladeData);
            $html = $view->render();//fetch the content of the blade template
            $email->addContent( "text/html", $html);
        }

        //if this is set to true
        if($withAttachment){
            $email->addContent( "text/plain", 'Latest DB Backup');
            $path =  storage_path('db_backup/');
            $file_name = Helper::config('site_name').'_dump.sql';
            $attachment = $path.$file_name;
            $content    = file_get_contents($attachment);
            //$content    = chunk_split(base64_encode($content));

            $attachment = new Attachment();
            $attachment->setContent($content);
            $attachment->setType("application/sql");
            $attachment->setFilename(env('APP_ENV') . '_' .date('Y-m-d') . "_$file_name");
            $attachment->setDisposition("attachment");
            $email->addAttachment($attachment);
        }

        $sendgrid = new \SendGrid($api_key);

        try {
            $response = $sendgrid->send($email);

        } catch (TypeException $e) {
            return false;
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Email  $email
     * @return \Illuminate\Http\Response
     */
    public function show(Email $email)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Email  $email
     * @return \Illuminate\Http\Response
     */
    public function edit(Email $email)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Email  $email
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Email $email)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Email  $email
     * @return \Illuminate\Http\Response
     */
    public function destroy(Email $email)
    {
        //
    }
}
