<?php

namespace App\Http\Controllers\API;

use App\Models\Plan;
use App\Models\Plot;
use App\Models\Project;
use App\Models\Schedule;
use App\Models\Status;
use App\Models\Subscription;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Admin\UsersController;

use App\Http\Controllers\Admin\SubscriptionController as AdminSubscriptionController;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscriptions = Subscription::where('user_id', auth()->id())->get();
        return view('users.dashboard.subscriptions', compact('subscriptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $statuses = Status::all();
        $plans = Plan::all();
        $projects = Project::where('status_id', 1)->paginate(9);
        return view('project.show', compact('statuses', 'plans', 'projects'));
    }


    public function v_checkuserdata()
    {
        $user = \auth()->user();
        $phone = $user->phone;
        $avatar = $user->avatar;
        $address = $user->address;
        $date_of_birth = $user->date_of_birth;

        if((is_null($phone) || $phone == "") || (is_null($avatar) || $avatar == "") || (is_null($address) || $address == "")
        || (is_null($date_of_birth) || $date_of_birth == ""))
        {
            $returnArray = ['status' => 'incomplete', 'message' => 'Go to Profile Settings to update your profile.'];
        } else {
            $returnArray = ['status' => 'complete'];
        }
        return response()->json($returnArray, 200);

    }

    public function neededUserData($value)
    {

        return redirect('profile')->with('status', 'Please complete your profile');
//        return redirect('profile');
//            ->with('error', "Your ".$value." is required, please go to `Dashboard -> Profile Setting` on the side bar  to update your profile.");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Check if Customer has fulfilled all requirements
//        self::checkuserdata();

        $validator = Validator::make($request->all(), [
            'plotOption' => ['required', 'numeric',],
            'planOption' => ['required', 'numeric',],
            'unitOption' => ['required', 'numeric',],
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $plan_id = (int) $request->post('planOption');
        $plot_id = (int) $request->post('plotOption');
        $units = (double) $request->post('unitOption');
        $subscriptionObject = new AdminSubscriptionController();
        $user_id = auth()->user()->id;
        $subscription = $subscriptionObject->createSubscription($user_id, $plan_id, $plot_id, $units, 3, \Illuminate\Support\Carbon::now());
        if($subscription) {
            $scheduleCreated = $subscriptionObject->prunePaymentSchedule($subscription, $plan_id, $plot_id, $units);
            if($scheduleCreated) {
                return redirect('dashboard/subscription/all')->with('status', 'Subscription created successfully. Please check your email. ');
            } else {
                return back()->with('error', 'Failed to create subscription. ');
            }
        } else {
            return back()->with('error', 'Failed to create subscription. ');
        }

    }

    /**
     * @param int $user_id
     * @return mixed
     */
    public static function subscriptionProgress(int $user_id)
    {
        $subscriptions = Subscription::where('user_id', $user_id)->get();
        foreach ($subscriptions as $subscription) {
             $paid_amount = (double) DB::table('schedules')->where('subscription_id', $subscription->id)
                ->where('status_id', 12)
                ->sum('installment_amount');
            if($paid_amount == 0) {
                $subscription->completion_percentage = 0.0;
            } else {
                if($subscription->plot->price == 0) {
                    $subscription->completion_percentage = 0.0;
                } else {
                    if($subscription->plan->id == 1) {
                        $subscription->completion_percentage = round(($paid_amount/$subscription->plot->one_time_price) * 100);
                        $subscription->paid_amount = (double) $paid_amount;
                        $subscription->price = (double) $subscription->plot->one_time_price;
                    } else {
                        $subscription->completion_percentage = round(($paid_amount/$subscription->plot->price) * 100);
                        $subscription->paid_amount = (double) $paid_amount;
                        $subscription->price = (double) $subscription->plot->project->price;
                    }
                }
            }
        }

        return $subscriptions;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function show(Subscription $subscription)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function edit(Subscription $subscription)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subscription $subscription)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subscription $subscription)
    {
        //
    }
}
