<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\Status;
use App\Models\Transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::all();

        return view('admin.transaction.index', compact('transactions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $statuses = Status::all();

        return view('admin.transaction.create', compact('statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'reference' => ['required', 'string', 'max:255',],
            'status_id' => ['required', 'numeric',],
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $payment = Payment::where('reference', $request->post('reference'))->first();

        if($payment) {
            $user_id = User::where('id', $payment->user->id)->pluck('id')->first();
            if($user_id) {
                $transaction = new Transaction();
                $transaction->user_id = $user_id;
                $transaction->payment_id = $payment->id;
                $transaction->status_id = $request->post('status_id');
                $transaction->save();

                if($transaction) {
                    return redirect('admin/transaction/all')->with('success', 'Transaction created successfully.');
                } else {
                    return back()->with('error', 'Failed to create Transaction');
                }
            } else {
                return back()->with('status', 'User record not found.');
            }
        } else {
            return back()->with('status', 'Payment record not found.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = Transaction::find($id);

        return view('admin.transaction.show', compact('transaction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaction = Transaction::find($id);
        $statuses = Status::all();

        return view('admin.transaction.edit', compact('transaction', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $payment = Payment::where('reference', $request->post('reference'))->first();
        if($payment) {
            $user_id = User::where('id', $payment->user->id)->pluck('id')->first();
            if($user_id) {
                $transaction = Transaction::find($id);
                $transaction->user_id = $user_id;
                $transaction->payment_id = $payment->id;
                $transaction->status_id = $request->post('status_id');
                $transaction->save();

                if ($transaction) {
                    return redirect('admin/transaction/all')->with('success', 'Transaction updated successfully.');
                } else {
                    return back()->with('error', 'Failed to update Transaction');
                }
            } else {
                return back()->with('error', 'User record not found.');
            }

        } else {
            return back()->with('status', 'Payment record not found.');
        }
        $transaction = Transaction::find($id);
        $transaction->status_id = $request->post('status_id');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaction = Transaction::where('id', $id)->delete();
        if($transaction) {
            return back()->with('success', 'Transaction record deleted successfully.');
        } else {
            return back()->with('error', 'Failed to delete Transaction record.');
        }
    }
}
