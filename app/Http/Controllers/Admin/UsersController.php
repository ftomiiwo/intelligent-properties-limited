<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\Welcome;
use App\Models\Project;
use App\Models\Role;
use App\Models\Status;
use App\Models\UserUpline;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\UserController;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\VerifyUser;
use App\Helper;

class UsersController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::all();
        if(\auth()->user()->isRealtor()) {
            $customers = User::where('role_id', 1)->where('created_by_user_id', \auth()->user()->id)->get();
        } else {
            $customers = User::where('role_id', 1)->get();
        }

        return view('admin.user.index', compact('users', 'customers'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function userlist()
    {
        $users = User::all();
        $customers = User::where('role_id','!=', 1)->get();

        return view('admin.user.index', compact('users', 'customers'));
    }

    /**
     * @param $user_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($user_id)
    {
        $customer = User::where('id', $user_id)->get();

        return view('admin.user.show', compact('customer'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $roles = Role::all();
        $statuses = Status::find([1, 2]);
        return view('admin.user.create', compact('roles', 'statuses'));
    }

    /**
     * Create a new user instance after a valid registration.
     * @param  array  $data
     * @return \App\User
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string', 'min:11'],
            'address' => ['required', 'string'],
            'role_id' => ['required', 'numeric'],
            'status_id' => ['required', 'numeric'],
            'gender' => ['required', 'string'],
            'date_of_birth' => ['required',],
            'nationality' => ['required', 'string', 'max:255',],
            'occupation' => ['required', 'string', 'max:255',],
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
            'state_of_origin' => ['required', 'string', 'max:255',],
            'next_of_kin_name' => ['string', 'max:255',],
            'next_of_kin_address' => ['string', 'string',],
            'next_of_kin_phone' => ['string', 'string', 'min:11',],
            'password' => ['required', 'string', 'min:8'],
        ]);
        $imageName = 'users.jpg';
        if($request->hasFile('avatar')) {
            $imageName = 'user_image_'.time().'.'.request()->avatar->getClientOriginalExtension();
            $request->avatar->storeAs('images/avatars', $imageName);
        }

        if ($validator->fails()) {
            return redirect('admin/user/add')
                ->withErrors($validator)
                ->withInput();
        }

        $paystack = new UserController();
        $pk = $paystack->createPaystackUser($request->post('email'));
        $logged_in_user = \auth()->user()->id;

        $user = User::create([
            'name' => $request->post('name'),
            'email' => $request->post('email'),
            'phone' => $request->post('phone'),
            'address' => $request->post('address'),
            'gender' => $request->post('gender'),
            'date_of_birth' => $request->post('date_of_birth'),
            'nationality' => $request->post('nationality'),
            'occupation' => $request->post('occupation'),
            'avatar' => $imageName,
            'paystack_customer_id' => $pk->id,
            'paystack_customer_code' => $pk->customer_code,
            'state_of_origin' => $request->post('state_of_origin'),
            'next_of_kin_name' => $request->post('next_of_kin_name'),
            'next_of_kin_address' => $request->post('next_of_kin_address'),
            'next_of_kin_phone' => $request->post('next_of_kin_phone'),
            'role_id' => $request->post('role_id'),
            'created_by_user_id' => $logged_in_user,
            'status_id' => $request->post('status_id'),
            'password' => Hash::make($request->post('password')),
        ]);

        $upline = UserUpline::create([
           'user_id' => $user->id,
           'upline_id' => $logged_in_user,
        ]);

        if($user) {
            Log::info("Created Record for User with ID {$user->id}, and Email {$user->email}");
            return redirect('/admin/user/all')->with('success', $user->role->name.' Created Successfully.');
        } else {
            return back()->with('error','Failed to create Customer.');
        }
    }

    /**
     * @param $user_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($user_id)
    {
        $customer = User::where('id', $user_id)->first();
        $roles = Role::all();
        $statuses = Status::find([1, 2]);
        $uplines = User::where('role_id', '>', 1)->get();

        return view('admin.user.edit', compact('customer', 'roles', 'statuses', 'uplines'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
//            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string', 'min:11'],
            'address' => ['required', 'string'],
            'role_id' => ['required', 'numeric'],
            'status_id' => ['required', 'numeric'],
            'gender' => ['required', 'string'],

            'date_of_birth' => ['required',],
            'nationality' => ['required', 'string', 'max:255',],
            'occupation' => ['required', 'string', 'max:255',],
            'state_of_origin' => ['required', 'string', 'max:255',],
            'next_of_kin_name' => ['required', 'string', 'max:255',],
            'next_of_kin_address' => ['required', 'string',],
            'next_of_kin_phone' => ['required', 'string', 'min:11',],
        ]);
        $customer = User::find($request->post('user_id'));

        if($request->hasFile('avatar')) {
            $request->validate([
                'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
            ]);
            $imageName = 'user_image_'.time().'.'.request()->avatar->getClientOriginalExtension();
            $request->avatar->storeAs('images/avatars', $imageName);
            $customer->avatar = $imageName;
        }

        if ($validator->fails()) {
            return redirect('admin/user/edit/'.$request->post('user_id'))
                ->withErrors($validator)
                ->withInput();
        }


        $customer->name = $request->post('name');
        $customer->phone = $request->post('phone');
        $customer->role_id = $request->post('role_id');
        $customer->status_id = $request->post('status_id');
        $customer->address = $request->post('address');
        $customer->gender = $request->post('gender');

        $customer->date_of_birth = $request->post('date_of_birth');
        $customer->nationality = $request->post('nationality');
        $customer->occupation = $request->post('occupation');
        $customer->state_of_origin = $request->post('state_of_origin');
        $customer->next_of_kin_name = $request->post('next_of_kin_name');
        $customer->next_of_kin_address = $request->post('next_of_kin_address');
        $customer->next_of_kin_phone = $request->post('next_of_kin_phone');
        $customer->save();

        if($customer) {
            return back()->with('success', $customer->role->name.' details edited successfully.');
        } else {
            return back()->with('error','Failed to edit User details.');
        }
    }

    public function updateUpline(Request $request)
    {
        $user_id = $request->post('user_id');
        $upline_id = $request->post('upline_id');

        $user = User::find($user_id);
        $user->created_by_user_id = $upline_id;
        $user->save();

        $upline = UserUpline::where('user_id', $user_id)->first();
        if(is_null($upline)) {
            UserUpline::create([
                'user_id' => $user_id,
                'upline_id' => $upline_id
            ]);
        } else {
            $upline->upline_id = $upline_id;
            $upline->save();
        }

        if($user) {
            return back()->with('success', $user->role->name.' details edited successfully.');
        } else {
            return back()->with('error','Failed to edit User details.');
        }

    }

    /**
     * @param $user_id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($user_id)
    {
        $delete = User::where('id', $user_id)->delete();
        if($delete) {
            return redirect('/admin/user/all')->with('success', 'User deleted successfully.');
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string', 'min:11'],
            'address' => ['required', 'string'],
            'role_id' => ['required', 'numeric'],
            'gender' => ['required', 'string'],
            'password' => ['required', 'string', 'min:8'],
        ]);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function importExportView()
    {
        return view('import');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function import(Request $request)
    {
        $uImport = new UsersImport();
        Excel::import($uImport, $request->file('file'));
        return back()->with('status', 'Uploaded Users successfully.');
    }

    public function verifyemail($userid, $useremail, $user)
    {
        $this->createVerifyUserRecord($userid);

        Mail::to($useremail)->send(new Welcome($user));

        return $user;
    }

    public function createVerifyUserRecord($user_id)
    {
        $verifyUser = VerifyUser::create([
            'user_id' => $user_id,
            'token' => Helper::strrand(40)
        ]);
    }

    public function resend_signup_mail($user_id)
    {
        $user = User::findorFail($user_id);
        $this->createVerifyUserRecord($user_id);
        Mail::to($user->email)->send(new Welcome($user));

        return redirect('/admin/user/all')->with('success', 'Confirmation Email re-sent successfully');
    }

    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if(isset($verifyUser) ){
            $user = $verifyUser->user;
            if(!$user->verified) {
                $verifyUser->user->email_verified_at = Carbon::now();
                $verifyUser->user->save();
                $status = "Your e-mail is verified. You can now login.";

                //Mail::to($user->email)->send(new WelcomeMail($user));

            } else{
                $status = "Your e-mail is already verified. You can now login.";
            }
        } else{
            return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
        }

        return redirect('/login')->with('status', $status);
    }

}
