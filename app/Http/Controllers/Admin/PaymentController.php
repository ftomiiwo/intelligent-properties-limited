<?php

namespace App\Http\Controllers\Admin;

use App\Helper;
use App\Http\Controllers\Controller;
use App\Mail\Payment as PaymentMail;
use App\Models\Gateway;
use App\Models\Payment;
use App\Models\Schedule;
use App\Models\Status;
use App\Models\Subscription;
use App\User;
use Carbon\Carbon;
use Dompdf\Dompdf;
use Dompdf\Options;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Barryvdh\DomPDF\Facade as PDF;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->isRealtor()) {
            $logged_in_user_id = auth()->user()->id;
            $payments = [];
            $userpayments = Payment::all();
            foreach ($userpayments as $uPayments) {
                if($uPayments->user->created_by_user_id == $logged_in_user_id) {
                    array_push($payments, $uPayments);
                }
            }
        } else {
            $payments = Payment::all();
        }
        return view('admin.payment.index', compact('payments'));
    }

    /**
     * @param int $user_id
     * @param int $gateway_id
     * @param float $amount
     * @param int $plot_id
     * @param int $subscription_id
     * @param int $status_id
     * @param string $reference
     * @param string $customer
     * @return string
     */

    public function createPaymentRecordAndSendPaymentMail(
        int $user_id,
        int $gateway_id,
        float $amount,
        int $plot_id,
        int $subscription_id,
        int $status_id,
        string $reference,
        string $customer
    )
    {
        $payment = new Payment();
        $payment->user_id = $user_id;
        $payment->subscription_id = $subscription_id;
        $payment->amount = $amount;
        $payment->status_id = $status_id;
        $payment->gateway_id = $gateway_id;
        $payment->plot_id = $plot_id;
        $payment->reference = $reference;
        $payment->save();
        Log::info(
            "Verified and created a payment record for the payment of {$amount} by {$customer}");
        if($payment) {
            $confirmation = self::confirmation($payment->id);
            Mail::to($payment->subscription->user->email)
                ->send(new PaymentMail($payment));
            return 'successful';
        } else {
            return 'failed';
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $statuses = Status::find([8, 10, 11, 12]);
        $gateways = Gateway::all();
        $subscriptions = Subscription::where('status_id', 3)->orWhere('status_id', 4)->orWhere('status_id', 6)->get();

        return view('admin.payment.create', compact( 'gateways', 'statuses', 'subscriptions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount' => ['required', 'numeric',],
            'status_id' => ['required', 'numeric',],
            'subscription_id' => ['required', 'numeric'],
            'gateway_id' => ['required', 'numeric',],
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $subscription = Subscription::where('id', $request->post('subscription_id'))->first();
//        $payment = new Payment();
//        $payment->user_id = $subscription->user->id;
//        $payment->subscription_id = $subscription->id;
//        $payment->amount = $request->post('amount');
//        $payment->status_id = $request->post('status_id');
//        $payment->gateway_id = $request->post('gateway_id');
//        $payment->plot_id = $subscription->plot_id;
//        $payment->reference = Str::random(8);
//        $payment->save();

        $payment = $this->generatePaymentRecord(
            $subscription,
            $request->post('amount'),
            $request->post('status_id'),
            $request->post('gateway_id')
        );

        if($payment) {
//            self::confirmation($payment->id);
            Mail::to($payment->subscription->user->email)
                ->send(new PaymentMail($payment));

            return redirect('admin/payment/all')->with('success', 'Payment created successfully.');
        } else {
            return back()->with('error', 'Failed to create payment');
        }
    }

    /**
     * @param Subscription $subscription
     * @param float $amount
     * @param int $status_id
     * @param int $gateway_id
     * @return Payment|\Illuminate\Http\RedirectResponse
     */
    public function generatePaymentRecord(Subscription $subscription, float $amount, int $status_id, int $gateway_id)
    {
        $payment = new Payment();
        $payment->user_id = $subscription->user->id;
        $payment->subscription_id = $subscription->id;
        $payment->amount = $amount;
        $payment->status_id = $status_id;
        $payment->gateway_id = $gateway_id;
        $payment->plot_id = $subscription->plot_id;
        $payment->reference = 'AG_'.Str::random(8);
        $payment->save();

        if($payment) {
            Log::info(
            "created a payment record for the payment of {$amount} by {$payment->subscription->user->name}");
            self::confirmation($payment->id);
            return $payment;
        } else {
            return back()->with('error', 'Failed to create payment');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $payment = Payment::find($id);
        $statuses = Status::find([8, 10, 11, 12]);
        $gateways = Gateway::all();
        $subscriptions = Subscription::where('status_id', 3)->get();

        return view('admin.payment.edit', compact('payment','gateways', 'statuses', 'subscriptions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'subscription_id' => ['required', 'numeric',],
            'amount' => ['required', 'numeric',],
            'status_id' => ['required', 'numeric',],
            'gateway_id' => ['required', 'numeric',],
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $subscription = Subscription::where('id', $request->post('subscription_id'))->first();
        $payment = Payment::find($id);
        $payment->user_id = $subscription->user->id;
        $payment->subscription_id = $request->post('subscription_id');
        $payment->amount = $request->post('amount');
        $payment->status_id = $request->post('status_id');
        $payment->gateway_id = $request->post('gateway_id');
        $payment->save();

        if($payment) {
            return redirect('admin/payment/all')->with('success', 'Payment updated successfully.');
        } else {
            return back()->with('error', 'Failed to updated payment');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $payment = Payment::where('id', $id)->delete();

        if($payment) {
            return back()->with('success', 'Payment deleted successfully.');
        } else {
            return back()->with('error', 'Failed to delete Payment.');
        }
    }

    public function createPendingConfirmations()
    {
        $payments = Payment::where('status_id', 12)->where('id', 2)->where('confirmation_file', null)->get();
        if(sizeof($payments) > 0) {
//            Log::info('Starting confirmation PDFs generation.');
            foreach($payments as $payment) {
                self::confirmation($payment->id);
            }
        }
//        else {
//            Log::info('There are no pending successful payments without confirmation PDFs');
//        }

        return true;
    }

    public function confirmation(int $id)
    {
        $payment = Payment::find($id);
        Log::info(
            "Started the creation of payment confirmation PDF for payment with the ID of {$payment->id} by
             {$payment->user->name}");
        $date_created = new Carbon($payment->created_at);
        $units = $payment->subscription->units > 1 ? $payment->subscription->units. ' Units' : $payment->subscription->units. ' Unit';
        $date_created = $date_created->format('l, jS \\of F Y');
        $subscription_price = $payment->subscription->plan->id == 1 ?
            $payment->subscription->plot->one_time_price : $payment->subscription->plot->price;
        $subscription_price_in_words = Helper::convert_number_to_words($subscription_price);

        $months = $payment->subscription->plan->interval->months;
        $years = $payment->subscription->plan->years;
        $interval = (12 * $years )/ $months;
        $process_amount = ($subscription_price - $payment->subscription->plot->initial_deposit) * $payment->subscription->units;
        $installment = round($process_amount / $interval, 1, 1);

        $plan_months = $payment->subscription->plan->years * 12;

        $payment_received = round($payment->amount, 1, 1);
        $all_scheduled_payments_sum = round(Schedule::where('subscription_id', $payment->subscription_id)->sum('installment_amount'), 1, 1);
        $all_paid_scheduled_payments_sum = Schedule::where('subscription_id', $payment->subscription_id)->where('status_id', 12)->sum('installment_amount');
        $all_pending_scheduled_payments_sum = Schedule::where('subscription_id', $payment->subscription_id)->where('status_id', 1)->sum('installment_amount');

        $payment_received_in_words = Helper::convert_number_to_words($payment_received);
        $all_paid_scheduled_payments_sum_in_words = Helper::convert_number_to_words($all_paid_scheduled_payments_sum);
        $all_pending_scheduled_payments_sum_in_words = Helper::convert_number_to_words($all_pending_scheduled_payments_sum);
        $all_scheduled_payments_sum_in_words = Helper::convert_number_to_words($all_scheduled_payments_sum);
        $installment_in_words = Helper::convert_number_to_words($installment);

        $confirmation_view = view(
            'admin.payment.confirmation',
            compact(
                'payment',
                'date_created',
                'units',
                'plan_months',
                'installment',
                'subscription_price',
                'subscription_price_in_words',
                'all_scheduled_payments_sum',
                'all_paid_scheduled_payments_sum',
                'all_pending_scheduled_payments_sum',
                'payment_received',

                'all_paid_scheduled_payments_sum_in_words',
                'all_pending_scheduled_payments_sum_in_words',
                'all_scheduled_payments_sum_in_words',
                'payment_received_in_words',
                'installment_in_words'
            )
        );
        $doc_name = 'confirmation-'.$payment->id.'-'.$payment->subscription->id.'-'.$payment->user->id.'-'.Str::random(8).'.pdf';
        $options = new Options();
        $options->isHtml5ParserEnabled(true);
        $options->setIsRemoteEnabled(true);
        $pdf = new Dompdf($options);
        $pdf->loadHtml($confirmation_view);
        $pdf->render();
        $output_pdf = $pdf->output();
        file_put_contents('storage/documents/confirmations/'.$doc_name, $output_pdf);
        $payment->confirmation_file = $doc_name;
        $payment->save();
        Log::info(
            "Successfully generated confirmation PDF for payment with the ID of {$payment->id} by
             {$payment->user->name}");
        if(app()->runningInConsole()) {
            return true;
        } else {
            return back()->with('Payment confirmation file generated successfully.');
        }
    }

}
