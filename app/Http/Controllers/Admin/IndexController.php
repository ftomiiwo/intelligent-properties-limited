<?php

namespace App\Http\Controllers\Admin;

use App\Helper;
use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\Plan;
use App\Models\Project;
use App\Models\ProjectType;
use App\Models\Schedule;
use App\Models\Subscription;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    /**
     * This function checks if all configuration variables needed for the site to function are available
     */
    private function checkConfigs()
    {
        $site_name = Helper::config('site_name');
        $site_email = Helper::config('site_email');
        $website_url = Helper::config('site_url');
        $paystack_secret_key = Helper::config('paystack_secret_key');
        $sendgrid_api_key = Helper::config('sendgrid_api_key');
        $site_logo = Helper::config('site_logo');
        if(is_null($site_name) || $site_name == "") {self::neededConfig('Company Name');}
        if(is_null($site_email) || $site_email == "") {self::neededConfig('Company contact Email');}
        if(is_null($website_url) || $website_url == "") {self::neededConfig('Company Website');}
        if(is_null($paystack_secret_key) || $paystack_secret_key == "") {self::neededConfig('Paystack Secret Key and Public Key');}
        if(is_null($sendgrid_api_key) || $sendgrid_api_key == "") {self::neededConfig('SendGrid API Key');}
        if(is_null($site_logo) || $site_logo == "") {self::neededConfig('Company Logo');}
    }

    /**
     * @param $value
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    private function neededConfig($value)
    {
        return redirect('admin/onboarding')
            ->with('error', "The ".$value." configuration detail is needed for the site to be functional, go to `Site Configuration -> Onboard Company` on the side bar  to correct this error.");
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        self::checkConfigs();
        if(auth()->user()->isRealtor()) {
            $logged_in_user_id = auth()->user()->id;
            $customers = User::where('role_id', 1)->where('created_by_user_id', $logged_in_user_id)->count();
            $payments = DB::table('payments')->where('payments.status_id', 12)
                ->join('user_uplines', 'user_uplines.user_id', 'payments.user_id')
                ->where('user_uplines.upline_id', $logged_in_user_id)
                ->sum('payments.amount');

            $schedules = DB::table('schedules')
                ->join('subscriptions', 'subscriptions.id', 'schedules.subscription_id')
                ->join('users', 'users.id', 'subscriptions.user_id')
                ->join('user_uplines', 'user_uplines.user_id', 'users.id')
                ->where('user_uplines.upline_id', $logged_in_user_id)
                ->count('schedules.id');

            $subscriptions = DB::table('subscriptions')
                ->leftjoin('users', 'users.id', 'subscriptions.user_id')
                ->join('user_uplines', 'user_uplines.user_id', 'users.id')
                ->where('user_uplines.upline_id', $logged_in_user_id)
                ->count('subscriptions.id');
        } else {
            $customers = User::where('role_id', 1)->count();
            $payments = Payment::where('status_id', 12)->sum('amount');
            $subscriptions = Subscription::all()->count();
            $schedules = Schedule::all()->count();
        }

        $admins = User::where('role_id', '!=', 1)->count();
        $projects = Project::all()->count();
        $projectTypes = ProjectType::all()->count();
        $plans = Plan::all()->count();
        $transactions = Transaction::all()->count();



        return view(
            'admin.index',
            compact(
                'customers',
                'admins',
                'projects',
                'projectTypes',
                'plans',
                'subscriptions',
                'transactions',
                'payments',
                'schedules'
            )
        );
    }

}
