<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Interval;
use App\Models\Plan;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plans = Plan::all();
        $intervals = Interval::all();
        return view('admin.plan.index', compact('plans', 'intervals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $statuses = Status::find([1, 2]);
        $intervals = Interval::all();
        return view('admin.plan.create', compact('statuses', 'intervals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255',],
            'years' => ['required', 'numeric',],
            'interval_id' => ['required', 'numeric',],
            'status_id' => ['required', 'numeric',],
            'description' => ['required', 'string',],
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $plan = new Plan();
        $plan->name = $request->post('name');
        $plan->years = $request->post('years');
        $plan->interval_id = $request->post('interval_id');
        $plan->status_id = $request->post('status_id');
        $plan->description = $request->post('description');
        $plan->save();

        if($plan) {
            return redirect('admin/plan/all')->with('success', 'Payment Plan created successfully.');
        } else {
            return back()->with('error', 'Failed to create Payment Plan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $plan = Plan::find($id);
        return view('admin.plan.show', compact('plan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $statuses = Status::find([1, 2]);
        $plan = Plan::find($id);
        $intervals = Interval::all();

        return view('admin.plan.edit', compact('plan', 'statuses', 'intervals'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255',],
            'years' => ['required', 'numeric',],
            'interval_id' => ['required', 'numeric',],
            'status_id' => ['required', 'numeric',],
            'description' => ['required', 'string',],
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $plan = Plan::find($id);
        $plan->name = $request->post('name');
        $plan->years = $request->post('years');
        $plan->interval_id = $request->post('interval_id');
        $plan->status_id = $request->post('status_id');
        $plan->description = $request->post('description');
        $plan->save();

        if($plan) {
            return redirect('admin/plan/all')->with('success', 'Payment Plan updated successfully.');
        } else {
            return back()->with('error', 'Failed to update Payment Plan');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $plan = Plan::where('id', $id)->delete();
        if($plan) {
            return redirect('admin/plan/all')->with('success', 'Payment Plan deleted successfully.');
        } else {
            return back()->with('error', 'Failed to delete Payment Plan');
        }
    }
}
