<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        return view('admin.role.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'unique:statuses', 'max:255',],
            'description' => ['required', 'string',],
        ]);

        if ($validator->fails()) return back()->withErrors($validator)->withInput();

        $role = new Role();
        $role->name = $request->post('name');
        $role->description = $request->post('description');
        $role->save();

        if($role) {
            return redirect('/admin/role/all')->with('status', 'Role record created successfully. ');
        } else {
            return back()->with('error', 'Failed to create Role record.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        return view('admin.role.show', compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        return view('admin.role.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string',],
            'description' => ['required', 'string',],
        ]);

        if ($validator->fails()) return back()->withErrors($validator)->withInput();

        $role = Role::find($id);
        $role->name = $request->post('name');
        $role->description = $request->post('description');
        $role->save();

        if($role) {
            return redirect('/admin/role/all')->with('status', 'Role record edited successfully.');
        } else {
            return back()->with('status', 'Failed to update Role Record');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::where('id', $id)->delete();
        if($role) {
            return redirect('/admin/role/all')->with('status', 'Role record deleted successfully.');
        } else {
            return back()->with('status', 'Failed to delete Role Record');
        }
    }
}
