<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\ProjectType;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();
        return view('admin.project.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $project_types = ProjectType::all();
        $admin_id = Auth::user()->getAuthIdentifier();
        $statuses = Status::find([1, 2]);

        return view('admin.project.create', compact('project_types', 'admin_id', 'statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:projects',],
            'project_type_id' => ['required', 'numeric',],
            'status_id' => ['required', 'numeric',],
            'description' => ['required',],
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
            'location' => ['required', 'string',],
            'country' => ['required', 'string',],
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $user_id = Auth::user()->getAuthIdentifier();

        $imageName = 'project_image_'.time().'.'.request()->image->getClientOriginalExtension();
        $request->image->storeAs('images/projects', $imageName);

        $project  = new Project();
        $project->name = $request->post('name');
        $project->project_type_id = $request->post('project_type_id');
        $project->status_id = $request->post('status_id');
        $project->description = $request->post('description');
        $project->location = $request->post('location');
        $project->country = $request->post('country');
        $project->map_link = $request->post('map_link');
        $project->slug = Str::random(7);
        $project->image = $imageName;
        $project->save();

        if($project) {
            return redirect('admin/project/all')->with('success', 'Project Location details created successfully.');
        } else {
            return back()->with('error', 'Failed to create Project Location Details');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::find($id);
        return view('admin.project.show', compact('project'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project_types = ProjectType::all();
        $admin_id = Auth::user()->getAuthIdentifier();
        $statuses = Status::find([1, 2]);
        $project = Project::where('id', $id)->first();
        if($project) {
            return view(
                'admin.project.edit',
                compact('project_types', 'admin_id', 'statuses', 'project')
            );
        } else {
            return back()->with('error', 'Project Location not found!.');
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $project = Project::find($id);
        $request->validate([
            'name' => ['required', 'string', 'max:255',],
            'project_type_id' => ['required', 'numeric',],
            'status_id' => ['required', 'numeric',],
            'description' => ['required',],
            'location' => ['required', 'string',],
            'country' => ['required', 'string',],
        ]);

        if($request->hasFile('image')) {
            $request->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
            ]);
            $imageName = 'project_image_'.time().'.'.request()->image->getClientOriginalExtension();
            $request->image->storeAs('images/projects', $imageName);

            $project->image = $imageName;
        }

        $project->name = $request->post('name');
        $project->project_type_id = $request->post('project_type_id');
        $project->status_id = $request->post('status_id');
        $project->location = $request->post('location');
        $project->country = $request->post('country');
        $project->map_link = $request->post('map_link');
        $project->description = $request->post('description');

        $project->save();

        if($project) {
            return redirect('admin/project/all')->with('success', 'Project details updated successfully.');
        } else {
            return back()->with('error', 'Failed to update Project Details');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::where('id', $id)->delete();
        if($project) {
            return redirect('admin/project/all')->with('success', 'Project location deleted successfully.');
        } else {
            return back()->with('error', 'Failed to delete Project location');
        }
    }

}
