<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Plan;
use App\Models\Plot;
use App\Models\Project;
use App\Models\ProjectPlan;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PlotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plots = Plot::all();
        return view('admin.plot.index', compact('plots'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $project_locations = Project::all();
        $statuses = Status::find([1, 2]);
        $plans = Plan::all();
        return view('admin.plot.create', compact('project_locations', 'statuses', 'plans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:projects',],
            'project_id' => ['required', 'numeric',],
            'status_id' => ['required', 'numeric',],
            'price' => ['required', 'numeric',],
            'one_time_price' => ['required', 'numeric'],
            'initial_deposit' => ['numeric'],
            'payment_plans' => ['required'],
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $plot  = new Plot();
        $plot->name = $request->post('name');
        $plot->project_id = $request->post('project_id');
        $plot->status_id = $request->post('status_id');
        $plot->price = $request->post('price');
        $plot->one_time_price = $request->post('one_time_price');
        $plot->initial_deposit = $request->post('initial_deposit');
        $plot->save();

        if($plot) {
            foreach($request->post('payment_plans') as $key => $value) {
                $project_plan = new ProjectPlan();
                $project_plan->plan_id = $value;
                $project_plan->plot_id = $plot->id;
                $project_plan->save();
            }
            return redirect('admin/plot/all')->with('success', 'Project details created successfully.');
        } else {
            return back()->with('error', 'Failed to create Project Details');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $plot = Plot::find($id);
        return view('admin.plot.show', compact('plot'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $plot = Plot::find($id);
        $project_plans = [];
        $project_locations = Project::all();
        $statuses = Status::find([1, 2]);
        $p_plans = ProjectPlan::where('plot_id', $id)->get();
        foreach ($p_plans as $key => $value) {
            array_push($project_plans, $value->plan_id);
        }
        $plans = Plan::where('status_id', 1)->get();

        return view('admin.plot.edit', compact('plot', 'project_locations', 'statuses', 'plans', 'project_plans'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:projects',],
            'project_id' => ['required', 'numeric',],
            'status_id' => ['required', 'numeric',],
            'price' => ['required', 'numeric',],
            'one_time_price' => ['required', 'numeric'],
            'initial_deposit' => ['required', 'numeric'],
            'payment_plans' => ['required'],
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $plot = Plot::find($id);
        $plot->name = $request->post('name');
        $plot->project_id = $request->post('project_id');
        $plot->status_id = $request->post('status_id');
        $plot->price = $request->post('price');
        $plot->one_time_price = $request->post('one_time_price');
        $plot->initial_deposit = $request->post('initial_deposit');
        $plot->save();
        if($plot) {
            ProjectPlan::where('plot_id', $id)->delete();
            foreach($request->post('payment_plans') as $key => $value) {
                $temp = ProjectPlan::where('plot_id', $plot->id)->where('plan_id', $value)->get();
                if (sizeof($temp) < 1) {
                    $project_plan = new ProjectPlan();
                    $project_plan->plan_id = $value;
                    $project_plan->plot_id = $plot->id;
                    $project_plan->save();
                }

            }
            return redirect('admin/plot/all')->with('success', 'Project details updated successfully.');
        } else {
            return back()->with('error', 'Failed to update Project Details');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $plot = Plot::where('id', $id)->delete();
        if($plot) {
            return redirect('admin/plot/all')->with('success', 'Project deleted successfully.');
        } else {
            return back()->with('error', 'Failed to delete Project');
        }
    }
}
