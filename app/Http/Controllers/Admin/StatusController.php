<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $statuses = Status::all();
        return view('admin.status.index', compact('statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.status.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'unique:statuses', 'max:255',],
            'description' => ['required', 'string',],
        ]);

        if ($validator->fails()) return back()->withErrors($validator)->withInput();

        $status = new Status();
        $status->name = $request->post('name');
        $status->description = $request->post('description');
        $status->save();

        if($status) {
            return redirect('/admin/status/all')->with('status', 'Status record created successfully. ');
        } else {
            return back()->with('error', 'Failed to create Status record.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = Status::find($id);
        return view('admin.status.show', compact('status'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $status = Status::find($id);
        return view('admin.status.edit', compact('status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string',],
            'description' => ['required', 'string',],
        ]);

        if ($validator->fails()) return back()->withErrors($validator)->withInput();

        $status = Status::find($id);
        $status->name = $request->post('name');
        $status->description = $request->post('description');
        $status->save();

        if($status) {
            return redirect('/admin/status/all')->with('status', 'Status record edited successfully.');
        } else {
            return back()->with('status', 'Failed to update Status Record');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = Status::where('id', $id)->delete();
        if($status) {
            return redirect('/admin/status/all')->with('status', 'Status record deleted successfully.');
        } else {
            return back()->with('status', 'Failed to delete Status Record');
        }
    }
}
