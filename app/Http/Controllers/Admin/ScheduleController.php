<?php

namespace App\Http\Controllers\Admin;

use App\Helper;
use App\Models\Payment;
use App\Mail\Payment as PaymentMail;
use App\Http\Controllers\Admin\PaymentController;
use App\Models\Schedule;
use App\Models\Status;
use App\Models\Subscription;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Ixudra\Curl\Facades\Curl;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->isRealtor()) {
            $schedules = [];
            $logged_in_user_id = auth()->user()->id;
            $usersSchedules = Schedule::all();
            foreach ($usersSchedules as $uSchedules) {
                if($uSchedules->subscription->user->created_by_user_id == $logged_in_user_id) {
                    array_push($schedules, $uSchedules);
                }
            }
        } else {
            $schedules = Schedule::all();
        }

        return view('admin.schedule.index', compact('schedules'));
    }

    /**
     * Create Schedules
     * Parameters:
     * subscription_id,
     * amount[$request->('amount')],
     * plan->months,
     * Create Schedules with reference string
     * @param array $array
     * @return bool
     */
    public function storeSchedule(Array $schedule_array)
    {
        $interval = (12 * $schedule_array['years'] )/ $schedule_array['months'];
        $installment = $schedule_array['process_amount'] / $interval;
        $schedule_date = $this->morphDate($schedule_array['subscription']->start_date, $schedule_array['months']);

        for ($counter = 0; $counter < $interval; $counter++) {
            $this->createScheduleRecord($schedule_array['subscription']->id, $installment, $schedule_date);
            /** Increment start_date by $months [Months Interval] */
            $schedule_date = $this->morphDate($schedule_date, $schedule_array['months']);
        }
        return true;
    }

    /**
     * @param int $subscription_id
     * @param double $installment
     * @param date $schedule_date
     * @return Schedule|bool
     */
    public function createScheduleRecord(int $subscription_id, float $installment, string $schedule_date)
    {
        $schedule = new Schedule();
        $schedule->subscription_id = $subscription_id;
        $schedule->installment_amount = $installment;
        $schedule->schedule_date = $schedule_date;
        $schedule->status_id = 1; // Active
        $schedule->save();

        if($schedule) {
            return $schedule;
        } else {
            return false;
        }
    }

    /**
     * @param $date
     * @param $interval
     * @return string
     */
    public function morphDate($date, $interval)
    {
        return Carbon::parse($date)->addMonths($interval)->format('Y-m-d');//->addMonths($interval)->format('Y-m-d');
    }

    /**
     * Call Paystack Invoice create API
     * @param $schedule_id
     * @return bool
     */
    public function initiateInvoice($schedule_id)
    {
        $schedule = Schedule::find($schedule_id);
        $invoiceArray = $this->sendPaystackInvoice($schedule);
        /**
         * When Invoice is created, it sends back invoice code,
         * store Invoice Code into the Scheduled Payment (invoice_code)
         */

        $schedule->invoice_code = $invoiceArray->request_code;
        $schedule->offline_reference = $invoiceArray->offline_reference;
        $schedule->status_id = 5;
        $schedule->save();

        if($schedule) {
            return back()->with('status', 'Invoice sent successfully!.');
        } else {
            return back()->with('error', 'Failed to send Invoice.');
        }

    }

    /**
     * @return bool
     */
    public function scheduleCron()
    {
        $date = Carbon::now()->calendar();
        $schedules = Schedule::where('schedule_date', $date)->get();
        foreach ($schedules as $schedule) {
            $this->initiateInvoice($schedule->id);
            Log::info("Initiated the Schedule with ID {$schedule->id}");
        }
        return true;
    }

    /**
     * @param $schedule
     * @return bool|string
     */
    public function sendPaystackInvoice(Schedule $schedule)
    {
        $paystack_sk = Helper::config('paystack_secret_key');
        $installment = $schedule->installment_amount * 100;
        $customer = $schedule->subscription->user->name;
        $sendArray = array(
            'description' => $schedule->subscription->plot->project->description,
            'customer' => $schedule->subscription->user->paystack_customer_code,
            'due_date' => $schedule->schedule_date,
            'send_notification' => 'true',
            'amount' => $installment
        );
        $response = Curl::to('https://api.paystack.co/paymentrequest')
            ->withHeader( 'Authorization: Bearer ' . $paystack_sk )
//            ->withContentType('application/json')
            ->withData($sendArray)
            ->asJson()->post();
        if ($response->status === true) {
            Log::info("Sent a Payment Request of {$installment} to  {$customer}");
            return $response->data;
        } else {
            return false;
        }
    }

    /**
     * @param string $request_code
     * @return bool
     */
    public function verifyPaystackInvoice(string $request_code)
    {
        $paystack_sk = Helper::config('paystack_secret_key');
        $response = Curl::to('https://api.paystack.co/paymentrequest/' . $request_code)
            ->withContentType("application/json")
            ->withHeader( 'Authorization: Bearer ' . $paystack_sk )
            ->asJson()
            ->get();
        if ($response->status == true) {
            $schedule = Schedule::where('invoice_code', $request_code)->first();
            if($schedule->invoice_code == $response->data->request_code) {
                if($response->data->status == 'pending') {
                    return back()->with('error', 'Invoice payment is still pending.');
                } elseif ($response->data->status == 'success') {
                    $installment = $response->data->amount;
                    $customer = $schedule->subscription->user->name;

                    if($installment == round($schedule->installment_amount * 100)) {
                        $schedule->status_id = 12; // Change status to Paid
                    } else {
                        $schedule->status_id = 11; // Change status to Paid
                    }
                    $schedule->save();


                    $user_id = $schedule->subscription->user_id;
                    $gateway_id = 1;
                    $amount = $schedule->installment_amount;
                    $plot_id = $schedule->subscription->plot->project_id;
                    $subscription_id = $schedule->subscription_id;
                    $status_id = 12;
                    $reference = $response->data->request_code;

                    $paymentControllerInstance = new PaymentController();
                    $PCI = $paymentControllerInstance->createPaymentRecordAndSendPaymentMail(
                        $user_id, $gateway_id, $amount, $plot_id, $subscription_id, $status_id, $reference, $customer );

                    return back()->with('success', 'Invoice Payment '.$PCI);
                }
            }

            return $response->status;
        } else {
            return $response;
        }
    }

    /**
     * @return bool
     */
    public function sendDueInvoices()
    {
        $schedules = Schedule::where('status_id', 1)->get();
        $counter = 0;
        foreach ($schedules as $schedule) {
            if(Carbon::parse($schedule->schedule_date)->isToday() ) {
                $this->initiateInvoice($schedule->id);
                $counter += 1;
                Log::info("Payment Invoice for Schedule ID: {$schedule->id}, sent successfully. ");
            }
        }
        return true;
    }

    /**
     * @return bool
     */
    public function verifySentInvoices()
    {
        $schedules = Schedule::where('status_id', 5)->where('invoice_code', '!=', '')->get();
        $counter = 0;
        foreach ($schedules as $schedule) {
            $this->verifyPaystackInvoice($schedule->invoice_code);
            $counter += 1;
        }
        return true;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subscriptions = Subscription::all();
        $statuses = Status::find([1, 2, 4, 5, 10, 11, 12]);

        return view('admin.schedule.create', compact('subscriptions', 'statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subscription_id' => ['required', 'numeric',],
            'installment_amount' => ['required', 'numeric',],
            'status_id' => ['required', 'numeric',],
            'schedule_date' => ['required', 'date'],
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $subscription = Subscription::find($request->post('subscription_id'));
        $schedule = new Schedule();
        $schedule->subscription_id = $subscription->id;
        $schedule->status_id = $request->post('status_id');
        $schedule->installment_amount = $request->post('installment_amount');
        $schedule->schedule_date = $request->post('schedule_date');
        $schedule->save() ;

        if ($schedule) {
            return redirect('admin/schedule/all')->with('success', 'Payment Schedule created successfully.');
        } else {
            return back()->with('error', 'Failed to create Payment Schedule');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function show($schedule)
    {
        $schedule = Schedule::find($schedule);

        return view('admin.schedule.show', compact('schedule'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function edit($schedule)
    {
        $schedule = Schedule::find($schedule);
        $subscriptions = Subscription::all();
        $statuses = Status::find([1, 2, 4, 5, 10, 11, 12]);

        return view('admin.schedule.edit', compact('schedule', 'subscriptions', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $schedule_id)
    {
        $validator = Validator::make($request->all(), [
            'subscription_id' => ['required', 'numeric',],
            'installment_amount' => ['required', 'numeric',],
            'status_id' => ['required', 'numeric',],
            'schedule_date' => ['required', 'date'],
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $schedule = Schedule::find($schedule_id);
        $schedule->subscription_id = $request->post('subscription_id');
        $schedule->installment_amount = $request->post('installment_amount');
        $schedule->status_id = $request->post('status_id');
        $schedule->schedule_date = $request->post('schedule_date');
        $schedule->save();

        if($schedule) {
            return redirect('admin/schedule/all')->with('success', 'Payment Schedule updated successfully.');
        } else {
            return back()->with('error', 'Failed to update Payment Schedule');
        }

    }

    public function createPaymentRecordForSchedule(int $schedule_id)
    {
        $schedule = Schedule::find($schedule_id);
        $subscription = $schedule->subscription;
        $amount = $schedule->installment_amount;
        $paymentObject = new PaymentController();
        $paymentRecord = $paymentObject->generatePaymentRecord($subscription, $amount, 12, 1);

        $schedule->status_id = 12;
        $schedule->invoice_code = 'AGIC-'.Str::random(8);
        $schedule->save();
        if($paymentRecord) {
            return back()->with('success', 'Payment Record created successfully.');
        } else {
            return bac()->with('error', 'Failed to create Payment Record');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy($schedule_id)
    {
        $schedule = Schedule::where('id', $schedule_id)->delete();
        if($schedule) {
            return back()->with('success', 'Payment Schedule record deleted successfully.');
        } else {
            return back()->with('error', 'Failed to delete Payment Schedule record.');
        }
    }
}
