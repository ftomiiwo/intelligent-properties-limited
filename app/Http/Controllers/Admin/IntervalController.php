<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Interval;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\In;

class IntervalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $intervals = Interval::all();
        return view('admin.interval.index', compact('intervals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.interval.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'unique:statuses', 'max:255',],
            'months' => ['required', 'numeric',],
        ]);

        if ($validator->fails()) return back()->withErrors($validator)->withInput();

        $interval = new Interval();
        $interval->name = $request->post('name');
        $interval->months = $request->post('months');
        $interval->save();

        if($interval) {
            return redirect('/admin/interval/all')->with('status', 'Interval record created successfully. ');
        } else {
            return back()->with('error', 'Failed to create Interval record.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $interval = Interval::find($id);
        return view('admin.interval.show', compact('interval'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $interval = Interval::find($id);
        return view('admin.interval.edit', compact('interval'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string',],
            'months' => ['required', 'numeric',],
        ]);

        if ($validator->fails()) return back()->withErrors($validator)->withInput();

        $interval = Interval::find($id);
        $interval->name = $request->post('name');
        $interval->months = $request->post('months');
        $interval->save();

        if($interval) {
            return redirect('/admin/interval/all')->with('status', 'Interval record edited successfully.');
        } else {
            return back()->with('status', 'Failed to update Interval Record');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $interval = Interval::where('id', $id)->delete();
        if($interval) {
            return redirect('/admin/interval/all')->with('status', 'Interval record deleted successfully.');
        } else {
            return back()->with('status', 'Failed to delete Interval Record');
        }
    }
}
