<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\ScheduleController;
use App\Http\Controllers\Controller;
use App\Mail\Subscription as SubscriptionMail;
use App\Models\Plan;
use App\Models\Plot;
use App\Models\Project;
use App\Models\Schedule;
use App\Models\Status;
use App\Models\Subscription;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->isRealtor()) {
            $subscriptions = [];
            $logged_in_user_id = auth()->user()->id;
            $userssubscriptions = Subscription::all();
            foreach ($userssubscriptions as $usubscriptions) {
                if($usubscriptions->user->created_by_user_id == $logged_in_user_id) {
                    array_push($subscriptions, $usubscriptions);
                }
            }
        } else {
            $subscriptions = Subscription::all();
        }
        return view('admin.subscription.index', compact('subscriptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->user()->isRealtor()) {
            $customers = User::where('role_id', 1)->where('created_by_user_id', auth()->user()->id)->get();
        } else {
            $customers = User::where('role_id', 1)->get();
        }


        $statuses = Status::find([3, 4, 6]);
        $plans = Plan::where('status_id', 1)->get();
        $projects = Project::where('status_id', 1)->get();
        if(sizeof($projects) == 0) {
            return back()->with('error', 'No active projects available, create a project then continue.');
        }
        return view(
            'admin.subscription.create',
            compact(
                'statuses',
                'plans',
                'projects',
                'customers'
            )
        );
    }

    public function createSubscription(int $user_id, int $plan_id, int $plot_id, int $units, int $status_id, $start_date)
    {
//        $user_id = auth()->user()->id;
        $subscription = new Subscription();
        $subscription->plan_id = $plan_id;
        $subscription->plot_id = $plot_id;
        $subscription->units = $units;
        $subscription->user_id = $user_id;
        $subscription->status_id = $status_id;
        $subscription->start_date = $start_date;
        $subscription->save();
        if($subscription) {
            Mail::to($subscription->user->email)
                ->send(new SubscriptionMail($subscription));
            return $subscription;
        } else {
            return false;
        }
    }

    /**
     * @param Subscription $subscription
     * @param float $amount
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createOneTimePaymentSchedule(Subscription $subscription, float $amount)
    {
        $schedule = new ScheduleController();
        $one_time_payment = $schedule->createScheduleRecord(
            $subscription->id,
            $amount,
            $subscription->start_date
        );
        if($one_time_payment) {
            return back()->with('success', 'Subscription created successfully.');
        } else {
            return back()->with('error', 'Failed to create Subscription');
        }
    }

    /**
     * @param Subscription $subscription
     * @param Project $project
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createInstallmentPaymentSchedule(
        Subscription $subscription,
        int $plot_id,
        float $amount,
        int $plan_id,
        float $units
    )
    {
        $plot = Plot::find($plot_id);
        $project = Project::find($plot->project_id);
        $schedule = new ScheduleController();
        $plan = Plan::find($plan_id);
        $initial_installment_schedule = $schedule->createScheduleRecord(
            $subscription->id,
            $plot->initial_deposit * $units,
            $subscription->start_date
        );
        if ($initial_installment_schedule) {
            $months = $plan->interval->months;
            $years = $plan->years;
            $process_amount = ($amount - $plot->initial_deposit) * $units;
            $createSchedule = $schedule->storeSchedule([
                'years' => $years,
                'months' => $months,
                'process_amount' => $process_amount,
                'project' => $project,
                'subscription' => $subscription,
                'subscription_id' => $subscription->id,
                'start_date' => $subscription->start_date,
            ]);
            if ($createSchedule) {
                return back()->with('success', 'Subscription created successfully.');
            } else {
                return back()->with('error', 'Failed to create Subscription');
            }
        } else {
            return back()->with('error', 'Failed to create Initial installment record');
        }
    }

    /**
     * @param Request $request
     * @return Subscription|bool
     */
    public function storeSingleSubscriptionRecord(Request $request)
    {
        $subscription = new Subscription();
        $subscription->plot_id = $request->post('plot_id');
        $subscription->user_id = $request->post('user_id');
        $subscription->plan_id = $request->post('plan_id');
        $subscription->status_id = $request->post('status_id');
        $subscription->start_date = $request->post('start_date');
        $subscription->save();

        if($subscription) {
            return $subscription;
        } else {
            return false;
        }
    }

    /**
     * Store a newly created resource in storage.
     * Create Subscription
     * User Subscription to create schedule
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'plot_id' => ['required', 'numeric',],
            'user_id' => ['required', 'numeric',],
            'plan_id' => ['required', 'numeric',],
            'units' => ['required', 'numeric',],
            'start_date' => ['required', 'date'],
            'status_id' => ['required', 'numeric',],
            'amount' => ['required', 'numeric',],
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $user_id = $request->post('user_id');
        $plan_id = $request->post('plan_id');
        $plot_id = $request->post('plot_id');
        $units = $request->post('units');
        $status_id = $request->post('status_id');
        $start_date = $request->post('start_date');
        $amount = $request->post('amount');
        $subscription = $this->createSubscription($user_id, $plan_id, $plot_id, $units, $status_id, $start_date);
        if($subscription) {
            return $this->prunePaymentSchedule($subscription, $plan_id, $plot_id, $units, $amount);
        } else {
            return back()->with('error', 'Failed to create Subscription record.');
        }
    }

    /**
     * @param Subscription $subscription
     * @param int $plan_id
     * @param int $plot_id
     * @param float $units
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function prunePaymentSchedule(Subscription $subscription, int $plan_id, int $plot_id, float $units, float $amount = null)
    {
        $plot = Plot::find($plot_id);
        if( is_null($amount) ) {
            if( $plan_id == 1 ) {
                $amount = $plot->one_time_price * $units;
            } else {
                $amount = $plot->price * $units;
            }
        }

        if( $plan_id == 1 ) {
            $schedule = $this->createOneTimePaymentSchedule($subscription, $amount);
        } else {
            $schedule = $this->createInstallmentPaymentSchedule($subscription, $plot_id, $plot->price, $plan_id, $units);
        }

        return $schedule;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subscription = Subscription::find($id);
        $schedules = Schedule::where('subscription_id', $id)->get();

        return view('admin.subscription.show', compact('subscription', 'schedules'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subscription = Subscription::find($id);
        $subscription_schedules = Schedule::where('subscription_id', $subscription->id)->get();
        $customers = User::where('status_id', 1)->get();

        $statuses = Status::find([3, 4, 6]);
        $plans = Plan::where('status_id', 1)->get();
        $projects = Project::get();
        $plots = Plot::where('project_id', $subscription->plot->project->id)->get();
        return view('admin.subscription.edit',
            compact(
                'subscription',
                'subscription_schedules',
                'customers',
                'statuses',
                'plans',
                'projects',
                'plots'
            ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'plot_id' => ['required', 'numeric',],
            'user_id' => ['required', 'numeric',],
            'plan_id' => ['required', 'numeric',],
            'units' => ['required', 'numeric',],
            'start_date' => ['required', 'date'],
            'status_id' => ['required', 'numeric',],
            'amount' => ['required', 'numeric',],
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $plan_id = $request->post('plan_id');
        $plot_id = $request->post('plot_id');
        $units = $request->post('units');
        $status_id = $request->post('status_id');
        $start_date = $request->post('start_date');
        $user_id = $request->post('user_id');

        $subscription = Subscription::find($id);
        $subscription->plan_id = $plan_id;
        $subscription->plot_id = $plot_id;
        $subscription->units = $units;
        $subscription->status_id = $status_id;
        $subscription->start_date = $start_date;
        $subscription->user_id = $user_id;
        $subscription->save();

        if($subscription) {
            return redirect('admin/subscription/all')->with('success', 'Subscription record updated successfully.');
        } else {
            return back()->with('error', 'Failed to create Subscription record.');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schedules = Schedule::where('subscription_id', $id)->delete();
        $subscription = Subscription::where('id', $id)->delete();
        if($subscription) {
            return redirect('admin/subscription/all')->with('success', 'Subscription deleted successfully.');
        } else {
            return back()->with('error', 'Failed to delete Subscription');
        }
    }
}
