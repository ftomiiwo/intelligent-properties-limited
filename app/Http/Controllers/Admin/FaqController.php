<?php

namespace App\Http\Controllers\Admin;

use App\Models\Faq;
use App\Models\Status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faqs = Faq::all();
        return view('admin.faq.index', compact('faqs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $statuses = Status::find([1, 2]);
        return view('admin.faq.create', compact('statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:faqs',],
            'description' => ['required', 'string',],
            'status_id' => ['required', 'numeric',],
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $faq = new Faq();
        $faq->name = $request->post('name');
        $faq->description = $request->post('description');
        $faq->status_id = $request->post('status_id');
        $faq->save();

        if($faq) {
            return redirect('admin/faq/all')->with('status', 'FAQ created successfully.');
        } else {
            return back()->with('error', 'Failed to create FAQ');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function show(Faq $faq)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function edit(int $faq)
    {
        $faq = Faq::find($faq);
        $statuses = Status::find([1, 2]);
        return view('admin.faq.edit', compact('faq', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $faq = Faq::find($id);
        $faq->name = $request->post('name');
        $faq->description = $request->post('description');
        $faq->status_id = $request->post('status_id');
        $faq->save();

        if($faq) {
            return redirect('admin/faq/all')->with('status', 'FAQ updated successfully.');
        } else {
            return back()->with('error', 'Failed to update FAQ');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function destroy(Faq $faq)
    {
        //
    }
}
