<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ProjectType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProjectTypeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexType()
    {
        $project_types = ProjectType::all();
        return view('admin.type.index', compact('project_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createType()
    {
        $admin_id = Auth::user()->getAuthIdentifier();

        return view('admin.type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeType(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:projects',],
            'description' => ['required',],
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $projectType  = new ProjectType();
        $projectType->name = $request->post('name');
        $projectType->description = $request->post('description');
        $projectType->save();

        if($projectType) {
            return redirect('admin/project/type/all')->with('success', 'Project type details created successfully.');
        } else {
            return back()->with('error', 'Failed to create Project Type Details');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editType($id)
    {
        $project_type = ProjectType::find($id);
        if($project_type) {
            return view(
                'admin.type.edit',
                compact('project_type')
            );
        } else {
            return back()->with('error', 'Project Type not found!.');
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateType(Request $request, $id)
    {
        $project_type = ProjectType::find($id);
        $project_type->name = $request->post('name');
        $project_type->description = $request->post('description');
        $project_type->save();

        if($project_type) {
            return redirect('admin/project/type/all')->with('success', 'Project Type details updated successfully.');
        } else {
            return back()->with('error', 'Failed to update Project Type Details');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyType($id)
    {
        $project_type = ProjectType::where('id', $id)->delete();
        if($project_type) {
            return redirect('admin/project/type/all')->with('success', 'Project Type deleted successfully.');
        } else {
            return back()->with('error', 'Failed to delete Project');
        }
    }
}
