<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $configs = Config::all();
        return view('admin.config.index', compact('configs'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function onboard()
    {
        return view('admin.config.onboard');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logoMGT(Request $request)
    {
        $request->validate([
            'site_logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
        ]);
        $imageName = 'site_logo_'.time().'.'.request()->site_logo->getClientOriginalExtension();
        $request->site_logo->storeAs('images', $imageName);

        $config = Config::where('name', 'site_logo')->first();
        if($config) {
            $config->value = $imageName;
            $config->save();
        } else {
            $config = new Config();
            $config->name = 'site_logo';
            $config->value = $imageName;
            $config->save();
        }
        $config->value = $imageName;
        $config->save();
        if ($config) {
            return redirect('admin')->with('Status', 'Logo updated successfully. ');
        } else {
            return back()->with('error', 'Failed to update Logo');
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeOnboard(Request $request)
    {
        $dataArray = [];
        foreach ($request->post() as $key => $value) {
            if($key != '_token' && !is_null($value)) {
                $dataArray[] = $this->storeConfig($key, $value);
            }
        }

        if (sizeof($dataArray) > 0) {
            return redirect('admin')->with('Status', 'Configuration Information updated successfully. ');
        } else {
            return back()->with('error', 'Failed to update Configuration Information');
        }
    }

    /**
     * @param string $name
     * @param string $value
     */
    public function storeConfig(string $name, string $value)
    {
        $config = Config::where('name', $name)->first();
        if($config) {
            $config->value = $value;
            $config->save();
        } else {
            $config = new Config();
            $config->name = $name;
            $config->value = $value;
            $config->save();
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.config.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'unique:statuses', 'max:255',],
            'value' => ['required', 'string',],
            'description' => ['required', 'string',],
        ]);

        if ($validator->fails()) return back()->withErrors($validator)->withInput();

        $config = new Config();
        $config->name = $request->post('name');
        $config->value = $request->post('value');
        $config->description = $request->post('description');
        $config->save();

        if($config) {
            return redirect('/admin/config/all')->with('status', 'Configuration record created successfully. ');
        } else {
            return back()->with('error', 'Failed to create Configuration record.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $config = Config::find($id);
        return view('admin.config.show', compact('config'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $config = Config::find($id);
        return view('admin.config.edit', compact('config'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string',],
            'value' => ['required', 'string',],
            'description' => ['required', 'string',],
        ]);

        if ($validator->fails()) return back()->withErrors($validator)->withInput();

        $config = Config::find($id);
        $config->name = $request->post('name');
        $config->value = $request->post('value');
        $config->description = $request->post('description');
        $config->save();

        if($config) {
            return redirect('/admin/config/all')->with('status', 'Configuration record edited successfully.');
        } else {
            return back()->with('status', 'Failed to update Configuration Record');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $config = Config::where('id', $id)->delete();
        if($config) {
            return redirect('/admin/config/all')->with('status', 'Configuration record deleted successfully.');
        } else {
            return back()->with('status', 'Failed to delete Configuration Record');
        }
    }
}
