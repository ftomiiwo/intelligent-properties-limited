<?php

namespace App\Http\Controllers;

use App\Helper;
use App\Models\Plot;
use App\Models\Project;
use App\Models\Plan;
use App\Models\ProjectPlan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class SiteController extends Controller {

    /**
     * @param int $plot_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function plotDetail(int $plot_id)
    {
        $plot = Plot::where('id', $plot_id)->first();
        $plans = ProjectPlan::where('plot_id', $plot_id)->get();
        $installment = ($plot->price - $plot->initial_deposit);
        $interval = [];
        foreach ($plans as $plan) {
            if($plan->plan->id == 1 || $plan->plan->name == 'Outright Payment') {
                $denominator = $plot->one_time_price;
            } else {
                $cut = (12 * $plan->plan->years) / $plan->plan->interval->months;
                $denominator = $installment / $cut ;
            }
            array_push($interval, ['installment' => $denominator, 'id' => $plan->plan->id, 'name' => $plan->plan->name,]);
        }

        $retArray = [
            'id' => $plot->id,
            'outright_price' => $plot->one_time_price,
            'initial_deposit' => $plot->initial_deposit,
            'installment_price' => $plot->price,
            'distribution' => $interval,
        ];
        return response()->json($retArray, 200);
    }

    /**
     * @param int $project_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function projectAmount(int $project_id)
    {
        $project = Plot::find($project_id);
        return response()->json(['price' => $project->price, 'one_time_price' => $project->one_time_price], 200);
    }

    /**
     * @param $project_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function projectPlots($project_id)
    {
        $plots = Plot::where('project_id', $project_id)->where('status_id', 1)->get();
        $retArray = [];
        foreach ($plots as $plot) {
            array_push($retArray, ['id' => $plot->id, 'name' => $plot->name]);
        }

        return response()->json($retArray, 200);
    }

    /**
     * @param int $plan_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function planDetail(int $plan_id)
    {
        $plan = Plan::where('id', $plan_id)->first();
        return response()->json(['years' => $plan->years, 'months' => $plan->interval->months, 'name' => $plan->name], 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkUserProfileStatus()
    {
        $user = auth()->user();
        $phone = $user->phone;
        $avatar = $user->avatar;
        $address = $user->address;
        $date_of_birth = $user->date_of_birth;
        $occupation = $user->occupation;
        $name = $user->name;
        $next_of_kin_name = $user->next_of_kin_name;
        $nationality = $user->nationality;
        if(
            (is_null($phone) || $phone == "") ||
            (is_null($avatar) || $avatar == "") ||
            (is_null($address) || $address == "") ||
            (is_null($date_of_birth) || $date_of_birth == "") ||
            (is_null($occupation) || $occupation == "") ||
            (is_null($nationality) || $nationality == "") ||
            (is_null($name) || $name == "") ||
            (is_null($next_of_kin_name) || $next_of_kin_name == "")
        )
        {
            $returnArray = ['status' => 'incomplete', 'message' => 'Go to Profile Settings to update your profile.'];
        } else {
            $returnArray = ['status' => 'complete', 'message' => 'Status Updated.'];
        }
        return response()->json($returnArray, 200);

    }

    public function digitalSignature(Request $request)
    {
        $user = \auth()->user();
        $userRecord = User::find($user->id);
        $filename = implode('-', explode(' ', strtolower($user->name))).'-signature.png';
        $signature = 'storage/documents/digital_signatures/'.$filename;
        $imagedata = base64_decode($request->post('img_data'));
        file_put_contents($signature, $imagedata);
        $userRecord->digital_signature = $filename;
        $userRecord->save();

        $returnArray = [
            'status' => 'complete',
            'success' => true,
            'message' => 'Status Updated.'];
        return response()->json($returnArray, 200);
    }
}
