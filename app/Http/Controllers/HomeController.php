<?php

namespace App\Http\Controllers;

use App\Http\Controllers\API\SubscriptionController;
use App\Models\Payment;
use App\Models\Schedule;
use App\Models\Subscription;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return redirect(url('/dashboard'));
    }

    public function dashboard()
    {
        $payments = Payment::where('user_id', auth()->user()->id)->get();//Subscription::where('user_id', auth()->user()->id)->get(); //Schedule:://
        $userSubscriptionProgress = SubscriptionController::subscriptionProgress(auth()->user()->id);
        return view('users.dashboard.index', compact('payments', 'userSubscriptionProgress'));

    }
}
