<?php

namespace App\Http\Middleware;


use Closure;
use App\User;

class CompleteProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        if (is_null($user->digital_signature)) {
            return redirect('/profile')->with('status', 'Update your profile: Digital Signature. ');
        } elseif ($user->avatar == 'user.jpg') {
            return redirect('/profile')->with('status', 'Update your profile: Profile Picture/Passport.');
        } elseif (is_null($user->phone)) {
            return redirect('/profile')->with('status', 'Update your profile: Phone Number.');
        } elseif (is_null($user->address)) {
            return redirect('/profile')->with('status', 'Update your profile: Residential Address.');
        } elseif (is_null($user->gender)) {
            return redirect('/profile')->with('status', 'Update your profile: Gender.');
        } elseif (is_null($user->date_of_birth)) {
            return redirect('/profile')->with('status', 'Update your profile: Date of Birth.');
        } elseif (is_null($user->nationality)) {
            return redirect('/profile')->with('status', 'Update your profile: Nationality.');
        } elseif (is_null($user->state_of_origin)) {
            return redirect('/profile')->with('status', 'Update your profile: State of Origin.');
        } elseif (is_null($user->occupation)) {
            return redirect('/profile')->with('status', 'Update your profile: Occupation.');
        } elseif (is_null($user->next_of_kin_name)) {
            return redirect('/profile')->with('status', 'Update your profile: Next of Kin\'s Full Name.');
        } elseif (is_null($user->next_of_kin_address)) {
            return redirect('/profile')->with('status', 'Update your profile: Next of Kin\'s Residential Address.');
        } elseif (is_null($user->next_of_kin_phone)) {
            return redirect('/profile')->with('status', 'Update your profile: Next of Kin\'s Phone Number.');
        }

        return $next($request);
    }
}
