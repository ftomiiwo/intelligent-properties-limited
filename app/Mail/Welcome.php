<?php

namespace App\Mail;

use App\Models\VerifyUser;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\User;

class Welcome extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->user->name;
        $email = $this->user->email;
        $user = $this->user;
        $token = VerifyUser::where('user_id', $user->id)->first()->token;
        return $this->view('emails.welcome', compact('name', 'email', 'user', 'token'))
            ->with([
                'name' => $this->user->name,
                'email' => $this->user->email,
            ]);
    }
}
