<?php

namespace App\Mail;

use App\Helper;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BulkAccountCreated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, String $password)
    {
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $password = $this->password;
        $user = $this->user;
        return $this->subject('Welcome to '. Helper::config('site_name'))
            ->view('emails.bulk_welcome', compact('user', 'password'))
            ->with([
                'name' => $this->user->name,
                'email' => $this->user->email,
            ]);
    }
}
