<?php

namespace App\Mail;

use App\Helper;
use App\Models\Schedule;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Payment as PaymentModel;

class Payment extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($payment)
    {
        $this->payment = $payment;
    }

    /**
     * Build the message.
     *
     * @return $this
     * @throws \Exception
     */
    public function build()
    {
        $payment = $this->payment;
        $payment = PaymentModel::find($payment->id);
        $date_created = new Carbon($payment->created_at);
        $units = $payment->subscription->units > 1 ? $payment->subscription->units. ' Units' : $payment->subscription->units. ' Unit';
        $date_created = $date_created->format('l, jS \\of F Y');
        $subscription_price = $payment->subscription->plan->id == 1 ?
            $payment->subscription->plot->one_time_price : $payment->subscription->plot->price;
        $subscription_price_in_words = Helper::convert_number_to_words($subscription_price);

        $months = $payment->subscription->plan->interval->months;
        $years = $payment->subscription->plan->years;
        $interval = (12 * $years )/ $months;
        $process_amount = ($subscription_price - $payment->subscription->plot->initial_deposit) * $payment->subscription->units;
        $installment = round($process_amount / $interval, 1, 1);

        $plan_months = $payment->subscription->plan->years * 12;

        $payment_received = round($payment->amount, 1, 1);
        $all_scheduled_payments_sum = round(Schedule::where('subscription_id', $payment->subscription_id)->sum('installment_amount'), 1, 1);
        $all_paid_scheduled_payments_sum = Schedule::where('subscription_id', $payment->subscription_id)->where('status_id', 12)->sum('installment_amount');
        $all_pending_scheduled_payments_sum = Schedule::where('subscription_id', $payment->subscription_id)->where('status_id', 1)->sum('installment_amount');

        $payment_received_in_words = Helper::convert_number_to_words($payment_received);
        $all_paid_scheduled_payments_sum_in_words = Helper::convert_number_to_words($all_paid_scheduled_payments_sum);
        $all_pending_scheduled_payments_sum_in_words = Helper::convert_number_to_words($all_pending_scheduled_payments_sum);
        $all_scheduled_payments_sum_in_words = Helper::convert_number_to_words($all_scheduled_payments_sum);
        $installment_in_words = Helper::convert_number_to_words($installment);

        return $this->view(
//        $confirmation_view = view(
            'admin.payment.confirmation',
            compact(
                'payment',
                'date_created',
                'units',
                'plan_months',
                'installment',
                'subscription_price',
                'subscription_price_in_words',
                'all_scheduled_payments_sum',
                'all_paid_scheduled_payments_sum',
                'all_pending_scheduled_payments_sum',
                'payment_received',

                'all_paid_scheduled_payments_sum_in_words',
                'all_pending_scheduled_payments_sum_in_words',
                'all_scheduled_payments_sum_in_words',
                'payment_received_in_words',
                'installment_in_words'
            ))
            ->with([
            'name' => $this->payment->subscription->user->name,
            'email' => $this->payment->subscription->user->email,
                ])
            ->attach(
                public_path().'/storage/documents/confirmations/'.$this->payment->confirmation_file
                ,[
//                    'as' => 'confirmation.pdf',
                    'mime' => 'application/pdf',
                    ]
            );
//        return $this->view('emails.payment', compact('payment'))
//            ->with([
//                'name' => $this->payment->subscription->user->name,
//                'email' => $this->payment->subscription->user->email,
//            ])
//            ->attach(asset('storage/documents/confirmations/'.$this->payment->confirmation_file), [
//                'as' => 'confirmation.pdf',
//                'mime' => 'application/pdf',
//            ])
            ;
    }
}
