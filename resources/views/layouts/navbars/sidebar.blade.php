<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="{{ route('home') }}">
            @if(\App\Helper::config('site_logo'))
            <img src="{{ asset('storage/images/'.\App\Helper::config('site_logo')) }}" class="navbar-brand-img" alt="...">
            @else
            <img src="{{ asset('logod.png') }}" class="navbar-brand-img" alt="...">
            @endif
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                        <img alt="Image placeholders" src="{{ asset('storage/images/avatars/'.auth()->user()->avatar) }}">
                        </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">{{ __('Welcome!') }}</h6>
                    </div>
                    <a href="{{ route('profile.edit') }}" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>{{ __('My profile') }}</span>
                    </a>
                    <a href="{{ route('profile.security') }}" class="dropdown-item">
                        <i class="ni ni-settings-gear-65"></i>
                        <span>{{ __('Settings') }}</span>
                    </a>
                    <a href="{{ url('dashboard/subscription/all') }}" class="dropdown-item">
                        <i class="ni ni-calendar-grid-58"></i>
                        <span>{{ __('Subscriptions') }}</span>
                    </a>
                    @if(auth()->user()->isAdmin())
                    <a href="{{ url('admin') }}" class="dropdown-item">
                        <i class="ni ni-support-16"></i>
                        <span>{{ __('Backend Access') }}</span>
                    </a>
                    @endif
                    <a href="{{ \App\Helper::config('main_website_url') }}" class="dropdown-item">
                        <i class="fa fa-home"></i>
                        <span>{{ __('Go to website') }}</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <i class="ni ni-user-run"></i>
                        <span>{{ __('Logout') }}</span>
                    </a>
                </div>
            </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('home') }}">
                            @if(\App\Helper::config('site_logo'))
                                <img src="{{ asset('storage/images/'.\App\Helper::config('site_logo')) }}">
                            @else
                                <img src="{{ asset('icon-d.png') }}">
                            @endif
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Form -->
{{--            <form class="mt-4 mb-3 d-md-none">--}}
{{--                <div class="input-group input-group-rounded input-group-merge">--}}
{{--                    <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="{{ __('Search') }}" aria-label="Search">--}}
{{--                    <div class="input-group-prepend">--}}
{{--                        <div class="input-group-text">--}}
{{--                            <span class="fa fa-search"></span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </form>--}}
            <!-- Navigation -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('home') }}">
                        <i class="ni ni-tv-2 text-red"></i> {{ __('Dashboard') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('dashboard/subscription/add') }}">
                        <i class="fa fa-cart-plus text-red"></i> {{ __('Property Shop') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('dashboard/subscription/all') }}">
                        <i class="fa fa-chart-pie text-red"></i> {{ __('My Properties') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('dashboard/transaction/all') }}">
                        <i class="ni ni-calendar-grid-58 text-orange"></i> {{ __('Transactions') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="#navbar-examples" data-toggle="collapse"
                       role="button" aria-expanded="true" aria-controls="navbar-examples">
                        <i class="fa fa-wrench" style="color: #f4645f;"></i>
                        <span class="nav-link-text" style="color: #f4645f;">{{ __('Settings') }}</span>
                    </a>
                    <div class="collapse show" id="navbar-examples">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('profile.edit') }}">
                                    <i class="fa fa-user-alt text-blue"></i>{{ __('Profile Settings') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('profile.security') }}"><i class="fa fa-lock text-blue"></i>{{ __('Security Settings') }}</a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ \App\Helper::config('main_website_url') }}">
                        <i class="fa fa-home text-red"></i>{{ __(' Go to website') }}
                    </a>
                </li>
            @guest()
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">
                        <i class="ni ni-key-25 text-info"></i> {{ __('Login') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">
                        <i class="ni ni-circle-08 text-pink"></i> {{ __('Register') }}
                    </a>
                </li>
                @endauth
                @auth()
                    <li class="nav-item">
                        <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            <i class="ni ni-circle-08 text-pink"></i><span>{{ __('Logout') }}</span>
                        </a>
                    </li>

                @endguest
            </ul>
{{--            <!-- Divider -->--}}
{{--            <hr class="my-3">--}}
{{--            <!-- Heading -->--}}
{{--            <h6 class="navbar-heading text-muted">Documentation</h6>--}}
{{--            <!-- Navigation -->--}}
{{--            <ul class="navbar-nav mb-md-3">--}}
{{--                <li class="nav-item">--}}
{{--                    <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/getting-started/overview.html">--}}
{{--                        <i class="ni ni-spaceship"></i> Getting started--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="nav-item">--}}
{{--                    <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/foundation/colors.html">--}}
{{--                        <i class="ni ni-palette"></i> Foundation--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="nav-item">--}}
{{--                    <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/components/alerts.html">--}}
{{--                        <i class="ni ni-ui-04"></i> Components--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--            </ul>--}}
        </div>
    </div>
</nav>
