<div class="header bg-gradient-primary py-7 py-lg-8">
    <div class="container">
        <div class="header-body text-center mb-7">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-6">
                    @if(\App\Helper::config('site_name'))
                    <h1 class="text-white">{{ __('Welcome to '.\App\Helper::config('site_name')) }}</h1>
                    @else
                    <h1 class="text-white">{{ __('Welcome to `Company Name` Subscription Management Platform.') }}</h1>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="separator separator-bottom separator-skew zindex-100">
    </div>
</div>
