<div class="row align-items-center justify-content-xl-between">
    <div class="col-xl-6">
        <div class="copyright text-center text-xl-left text-muted">
            @if(\App\Helper::config('site_name') && \App\Helper::config('site_url'))
            &copy; {{ now()->year }} <a href="{{ \App\Helper::config('site_url') }}" class="font-weight-bold ml-1" target="_blank">{{ \App\Helper::config('site_name') }}</a>
            @else
            &copy; {{ now()->year }} <a href="https://atfak.com/" class="font-weight-bold ml-1" target="_blank">Subscription Management Platform</a>
            @endif
        </div>
    </div>
</div>

