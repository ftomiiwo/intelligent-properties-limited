<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{ \App\Helper::config('site_name')  }}</title>
        <meta name="description" content="{{ \App\Helper::config('site_description') }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        @if(\App\Helper::config('site_logo'))
            <link rel="apple-touch-icon" href="{{ asset('storage/images/'.\App\Helper::config('site_logo')) }}">
            <link rel="shortcut icon" href="{{ asset('storage/images/'.\App\Helper::config('site_logo')) }}">
            <link href="{{ asset('storage/images/'.\App\Helper::config('site_logo')) }}" rel="shortcut icon" type="image/vnd.microsoft.icon" />
        @else
            <link rel="apple-touch-icon" href="{{ asset('icon-d.png') }}">
            <link rel="shortcut icon" href="{{ asset('icon-d.png') }}">
            <link href="{{ asset('icon-d.png') }}" rel="shortcut icon" type="image/vnd.microsoft.icon" />
        @endif


        <link rel="stylesheet" href="{{ asset('vendors/bootstrap/dist/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vendors/themify-icons/css/themify-icons.css') }}">
        <link rel="stylesheet" href="{{ asset('vendors/flag-icon-css/css/flag-icon.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vendors/selectFX/css/cs-skin-elastic.css') }}">
        <link rel="stylesheet" href="{{ asset('vendors/jqvmap/dist/jqvmap.min.css') }}">


        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    </head>

    <body>


        <!-- Left Panel -->

        <aside id="left-panel" class="left-panel">
            <nav class="navbar navbar-expand-sm navbar-default">

                <div class="navbar-header">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fa fa-bars"></i>
                    </button>
                    @if(\App\Helper::config('site_name'))
                    <a class="navbar-brand" href="{{ url('admin') }}">
                        <img src="{{ asset('storage/images/'.\App\Helper::config('site_logo')) }}"
                             alt="{{ \App\Helper::config('site_name') }}Logo"></a>
                    <a class="navbar-brand hidden" href="{{ url('admin') }}">
                        <img src="{{ asset('storage/images/'.\App\Helper::config('site_logo')) }}"
                             alt="{{ \App\Helper::config('site_name') }} Logo"></a>
                    @else
                    <a class="navbar-brand" href="{{ url('admin') }}"><img src="{{ asset('logod.png') }}" alt="Logo"></a>
                    <a class="navbar-brand hidden" href="{{ url('admin') }}"><img src="{{ asset('logod.png') }}" alt="{{ getenv('APP_NAME') }} Logo"></a>
                    @endif
                </div>

                <div id="main-menu" class="main-menu collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a href="{{ url('admin') }}"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                        </li>
        {{--                <h3 class="menu-title">Projects</h3><!-- /.menu-title -->--}}
                        @if(auth()->user()->isSuperAdmin())
                        <li class="menu-item-has-children dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-suitcase"></i>Admin</a>
                            <ul class="sub-menu children dropdown-menu">
                                <li><i class="fa fa-eye"></i><a href="{{ url('admin/user/admins') }}">Admins</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-suitcase"></i>Projects</a>
                            <ul class="sub-menu children dropdown-menu">
                                <li><i class="fa fa-eye"></i><a href="{{ url('admin/plot/all') }}">All Projects</a></li>
                                <li><i class="fa fa-plus"></i><a href="{{ url('admin/plot/add') }}">Create Project</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-suitcase"></i>Property Locations</a>
                            <ul class="sub-menu children dropdown-menu">
                                <li><i class="fa fa-eye"></i><a href="{{ url('admin/project/all') }}">All Property Locations</a></li>
                                <li><i class="fa fa-plus"></i><a href="{{ url('admin/project/add') }}">Create Property Location</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-suitcase"></i>Property Types</a>
                            <ul class="sub-menu children dropdown-menu">
                                <li><i class="fa fa-puzzle-piece"></i><a href="{{ url('admin/project/type/all') }}">All Property Types</a></li>
                                <li><i class="fa fa-plus-circle"></i><a href="{{ url('admin/project/type/add') }}">Create Property Type</a></li>
                            </ul>
                        </li>
                        @endif
                        <li class="menu-item-has-children dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Users</a>
                            <ul class="sub-menu children dropdown-menu">
                                <li><i class="fa fa-eye"></i><a href="{{ url('admin/user/all') }}">Customers</a></li>
                                @if(auth()->user()->isSuperAdmin())
                                <li><i class="fa fa-eye"></i><a href="{{ url('admin/user/admins') }}">Admins</a></li>
                                @endif
                                <li><i class="fa fa-plus"></i><a href="{{ url('admin/user/add') }}">Create User</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-calculator"></i>Finance</a>
                            <ul class="sub-menu children dropdown-menu">
                                <li><i class="menu-icon fa fa-money"></i><a href="{{ url('admin/payment/all') }}">All Payments</a></li>
                                <li><i class="menu-icon fa fa-pencil"></i><a href="{{ url('admin/payment/add') }}">Create Payment</a></li>
{{--                                <li><i class="menu-icon fa fa-money"></i><a href="{{ url('admin/transaction/all') }}">All Transactions</a></li>--}}
{{--                                <li><i class="menu-icon fa fa-pencil-square-o"></i><a href="{{ url('admin/transaction/add') }}">Create Transaction</a></li>--}}
                            </ul>
                        </li>
                        @if(auth()->user()->isSuperAdmin())
                        <li class="menu-item-has-children dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Payment Plans</a>
                            <ul class="sub-menu children dropdown-menu">
                                <li><i class="menu-icon fa fa-th"></i><a href="{{ url('admin/plan/all') }}">All Plans</a></li>
                                <li><i class="menu-icon fa fa-pencil"></i><a href="{{ url('admin/plan/add') }}">Create Plan</a></li>
                            </ul>
                        </li>
                        @endif
                        <li class="menu-item-has-children dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Subscriptions</a>
                            <ul class="sub-menu children dropdown-menu">
                                <li><i class="menu-icon fa fa-th"></i><a href="{{ url('admin/subscription/all') }}">All Subscriptions</a></li>
                                <li><i class="menu-icon fa fa-th"></i><a href="{{ url('admin/subscription/add') }}">Create Subscriptions</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Schedules</a>
                            <ul class="sub-menu children dropdown-menu">
                                <li><i class="menu-icon fa fa-th"></i><a href="{{ url('admin/schedule/all') }}">All Payment Schedules</a></li>
                                <li><i class="menu-icon fa fa-th"></i><a href="{{ url('admin/schedule/add') }}">Create Schedule</a></li>
                            </ul>
                        </li>

                        @if(auth()->user()->isSuperAdmin())
                        <li class="menu-item-has-children dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>FAQs</a>
                            <ul class="sub-menu children dropdown-menu">
                                <li><i class="menu-icon fa fa-th"></i><a href="{{ url('admin/faq/all') }}">All FAQs</a></li>
                                <li><i class="menu-icon fa fa-th"></i><a href="{{ url('admin/faq/add') }}">Create FAQ</a></li>
                            </ul>
                        </li>
                        <h3 class="menu-title">Utilities</h3> <!-- /.menu-title -->
                        <li class="menu-item-has-children dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Statuses</a>
                            <ul class="sub-menu children dropdown-menu">
                                <li><i class="menu-icon fa fa-th"></i><a href="{{ url('admin/status/all') }}">All Statuses</a></li>
                                <li><i class="menu-icon fa fa-th"></i><a href="{{ url('admin/status/add') }}">Create Status</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Intervals</a>
                            <ul class="sub-menu children dropdown-menu">
                                <li><i class="menu-icon fa fa-th"></i><a href="{{ url('admin/interval/all') }}">All Intervals</a></li>
                                <li><i class="menu-icon fa fa-th"></i><a href="{{ url('admin/interval/add') }}">Create Interval</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>User Roles</a>
                            <ul class="sub-menu children dropdown-menu">
                                <li><i class="menu-icon fa fa-th"></i><a href="{{ url('admin/role/all') }}">All Roles</a></li>
                                <li><i class="menu-icon fa fa-th"></i><a href="{{ url('admin/role/add') }}">Create Role</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Site Configuration</a>
                            <ul class="sub-menu children dropdown-menu">
{{--                                <li><i class="menu-icon fa fa-th"></i><a href="{{ url('admin/config/all') }}">All Configurations</a></li>--}}
                                <li><i class="menu-icon fa fa-th"></i><a href="{{ url('admin/onboarding') }}">Onboard Company</a></li>
{{--                                <li><i class="menu-icon fa fa-th"></i><a href="{{ url('admin/config/add') }}">Create Configuration</a></li>--}}
                            </ul>
                        </li>
                        @endif
                    </ul>
                </div><!-- /.navbar-collapse -->
            </nav>
        </aside><!-- /#left-panel -->

        <!-- Left Panel -->

        <!-- Right Panel -->

        <div id="right-panel" class="right-panel">

            <!-- Header-->
            <header id="header" class="header">

                <div class="header-menu">

                    <div class="col-sm-7">
                        <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
{{--                        <div class="header-left">--}}
{{--                            <button class="search-trigger"><i class="fa fa-search"></i></button>--}}
{{--                            <div class="form-inline">--}}
{{--                                <form class="search-form">--}}
{{--                                    <input class="form-control mr-sm-2" type="text" placeholder="Search ..." aria-label="Search">--}}
{{--                                    <button class="search-close" type="submit"><i class="fa fa-close"></i></button>--}}
{{--                                </form>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>

                    <div class="col-sm-5">
                        <div class="user-area dropdown float-right">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="user-avatar rounded-circle" src="{{ asset('storage/images/avatars/'.auth()->user()->avatar) }}" alt="User Avatar">
                            </a>

                            <div class="user-menu dropdown-menu">
                                <a class="nav-link" href="#"><i class="fa fa-user"></i>{{ Auth::user()->name }}</a>
                                <a class="nav-link" href="{{ url('') }}"><i class="fa fa-home"></i>Go Dashboard</a>
                                <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    <i class="fa fa-power-off"></i> {{ __('Logout') }}</a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </div>

        {{--                <div class="language-select dropdown" id="language-select">--}}
        {{--                    <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">--}}
        {{--                        <i class="flag-icon flag-icon-us"></i>--}}
        {{--                    </a>--}}
        {{--                    <div class="dropdown-menu" aria-labelledby="language">--}}
        {{--                        <div class="dropdown-item">--}}
        {{--                            <span class="flag-icon flag-icon-fr"></span>--}}
        {{--                        </div>--}}
        {{--                        <div class="dropdown-item">--}}
        {{--                            <i class="flag-icon flag-icon-es"></i>--}}
        {{--                        </div>--}}
        {{--                        <div class="dropdown-item">--}}
        {{--                            <i class="flag-icon flag-icon-us"></i>--}}
        {{--                        </div>--}}
        {{--                        <div class="dropdown-item">--}}
        {{--                            <i class="flag-icon flag-icon-it"></i>--}}
        {{--                        </div>--}}
        {{--                    </div>--}}
        {{--                </div>--}}

                    </div>
                </div>

            </header>
            <!-- Header-->
            <div class="breadcrumbs">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Dashboard</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li class="active">Dashboard</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content mt-3">
                @if (session('success'))
                    <div class="col-sm-12">
                        <div class="alert  alert-error alert-dismissible fade show" role="alert">
                            <span class="badge badge-pill badge-success">Success: </span> {{ session('success') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif
                @if (session('error'))
                    <div class="col-sm-12">
                        <div class="alert  alert-error alert-dismissible fade show" role="alert">
                            <span class="badge badge-pill badge-danger">Error: </span> {{ session('error') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="col-sm-12">
                        @foreach ($errors->all() as $error)
                        <div class="alert  alert-error alert-dismissible fade show" role="alert">
                            <span class="badge badge-pissll badge-warning">Error</span> {{ $error }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endforeach
                    </div>
                @endif


                @yield('content')

            </div>
        </div>

        <!-- Right Panel -->

        <script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ asset('vendors/popper.js/dist/umd/popper.min.js') }}"></script>
        <script src="{{ asset('vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/js/main.js') }}"></script>


        <script src="{{ asset('vendors/chart.js/dist/Chart.bundle.min.js') }}"></script>
        <script src="{{ asset('assets/js/dashboard.js') }}"></script>
        <script src="{{ asset('assets/js/widgets.js') }}"></script>
        <script src="{{ asset('vendors/jqvmap/dist/jquery.vmap.min.js') }}"></script>
        <script src="{{ asset('vendors/jqvmap/examples/js/jquery.vmap.sampledata.js') }}"></script>
        <script src="{{ asset('vendors/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>

        @yield('assets')
        <script src="{{ asset('vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
{{--        <script src="{{ asset('vendors/jquery-validation-unobtrusive/dist/jquery.validate.unobtrusive.min.js') }}"></script>--}}
        <script>
            (function($) {
                "use strict";

                jQuery('#vmap').vectorMap({
                    map: 'world_en',
                    backgroundColor: null,
                    color: '#ffffff',
                    hoverOpacity: 0.7,
                    selectedColor: '#1de9b6',
                    enableZoom: true,
                    showTooltip: true,
                    values: sample_data,
                    scaleColors: ['#1de9b6', '#03a9f5'],
                    normalizeFunction: 'polynomial'
                });
            })(jQuery);
        </script>
    </body>

</html>
