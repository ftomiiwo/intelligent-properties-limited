@extends('layouts.app')

@section('content')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <!-- Card stats -->
                <div class="row">

                    <div class="col-xl-3 col-lg-6">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-12 page_">
                                <h3 class="mb-0 page_title ">{{ $project->name }}</h3>
                                <div class="text_under_line"></div>
                            </div>
                            <div class="details_cnt">
                                <img src="{{ asset('storage/images/projects/'.$project->image) }}" class="full_img" alt="Project">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">Plot Size</th>
                                                <th scope="col">Outright Payment</th>
                                                <th scope="col">Installment</th>
                                                <th scope="col">Down Payment</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($plots as $plot)
                                            <tr>
                                                <th scope="row">{{ $plot->name }}</th>
                                                <td>&#8358;{{ number_format($plot->one_time_price) }}</td>
                                                <td>&#8358;{{ number_format($plot->price) }}</td>
                                                <td>&#8358;{{ number_format($plot->initial_deposit) }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <form action="{{ url('dashboard/subscription/create') }}" method="post" class="sub_form" id="subForm">
                                    @csrf
                                    <h4>To subscribe for this project;</h4>
                                    <div class="form_group">
                                        <div class="form_container">
                                            <label for="plotOption">Choose dimension</label>
                                           <select name="plotOption" id="plotOption" onchange="nano()" required>
                                               <option value selected disabled>Select Dimension</option>
                                               @foreach($plots as $plot)
                                               <option value="{{ $plot->id }}">{{ $plot->name }}</option>
                                               @endforeach
                                           </select>
                                        </div>
                                        <div class="form_container">
                                            <label for="unitOption">Unit(s)</label>
                                            <select name="unitOption" id="unitOption" onchange="nano()" required>
                                                @for($counter = 1; $counter < 101; $counter++)
                                                <option value="{{ $counter }}">{{ $counter }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="form_container">
                                            <label for="planOption">Choose payment option</label>
                                           <select name="planOption" id="planOption" onchange="planOp()" required>
                                               <option value  selected disabled>Choose Payment Option</option>
                                           </select>
                                        </div>
                                    </div>
                                    <div class="section" style="margin: 10px;">
                                        <input type="checkbox" name="tandc" id="tandc" required> I have read and agreed to this site's <a data-toggle="modal" data-target="#tancmodal" href="#">Terms and Conditions</a>
                                    </div>
                                    <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target=".bd-example-modal-lg">Review</button>
                                    <button class="btn btn-primary btn-lg pull-right" onclick="checksubmit()">Submit</button>
{{--                                    <button onclick="subModal()" id="formsubmitter">Proceed</button>--}}
                                </form>
                                <div class="description_con">
                                    <h4>Description</h4>
                                    <div class="location">
                                        <p class="itl"><svg width="10" height="16" viewBox="0 0 10 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M4.99995 0.857178C2.23569 0.857178 0 3.09287 0 5.85713C0 9.60709 4.99995 15.1428 4.99995 15.1428C4.99995 15.1428 9.9999 9.60709 9.9999 5.85713C9.9999 3.09287 7.76421 0.857178 4.99995 0.857178ZM1.42856 5.85713C1.42856 3.88572 3.02854 2.28574 4.99995 2.28574C6.97136 2.28574 8.57135 3.88572 8.57135 5.85713C8.57135 7.91425 6.51422 10.9928 4.99995 12.9142C3.51425 11.0071 1.42856 7.89282 1.42856 5.85713Z" fill="#12206D"/>
                                            <path d="M4.99981 7.64268C5.98602 7.64268 6.78551 6.8432 6.78551 5.85699C6.78551 4.87077 5.98602 4.07129 4.99981 4.07129C4.0136 4.07129 3.21411 4.87077 3.21411 5.85699C3.21411 6.8432 4.0136 7.64268 4.99981 7.64268Z" fill="#12206D"/>
                                            </svg>

                                            Location</p>
                                        <p class="addr">{{ $project->location }}, {{ $project->country }}.</p>
                                    </div>
                                    <p class="desc">{{ $project->description }}</p>
                                </div>
                                @if(!is_null($project->map_link))
                                    <iframe src="{{ $project->map_link }}" width="600" height="450" frameborder="0"
                                            style="border:5px;" allowfullscreen="" aria-hidden="false"
                                            tabindex="0"></iframe>
                                @endif
                                <div class="description_con">
                                    <h4>Frequently Asked Questions</h4>


                                    <div id="accordion">
                                        @if($faqs)
                                            @foreach($faqs as $faq)
                                            <div class="card">
                                                <div class="card-header" id="headingOne">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{$faq->id}}" @if($faq->id == 1) aria-expanded="true" @else aria-expanded="false" @endif aria-controls="collapseOne">
                                                            {{ $faq->name }}
                                                        </button>
                                                    </h5>
                                                </div>

                                                <div id="collapse{{$faq->id}}" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                    <div class="card-body">
                                                        {{ $faq->description }}
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>

    <!-- Terms and conditions Modal -->
    <div class="modal fade" id="tancmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">{{ \App\Helper::config('site_name') }}'s Terms and Conditions</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Terms and Conditions will be here
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Terms and conditions modal -->

    <!-- Subscription Preview Modal -->
    <div class="modal fade bd-example-modal-lg" id="subscription-preview" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="container">
                        <div style="text-align: center; width: 100%;vertical-align: middle;">
                            <ul class="list-unstyled">
                                <li class="media">
                                    <div class="media-body">
                                        <h2 class="mt-0 mb-1">{{ \App\Helper::config('site_name') }}</h2>
                                    </div>
                                    <img class="mr-3" style="width: 150px;" src="{{ asset('storage/images/'.\App\Helper::config('site_logo')) }}" alt="Generic placeholder image">
                                </li>
                            </ul>
                        </div>
                       <div class="header_container">
                            <div class="img_herader">
                                <img src="{{ asset('storage/images/avatars/'.auth()->user()->avatar) }}" alt="Generic placeholder image"/>
                            </div>
                            <h3 class="name_user">{{ auth()->user()->name }}</h3>
                            <p class="users_email">{{ auth()->user()->email }}</p>
                            <p class="users_phone">
                            {{ auth()->user()->phone }}</p>
                            <p class="users_address">
                                <svg width="14" height="18" viewBox="0 0 14 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M7 0C3.50891 0 0.671875 2.83711 0.671875 6.32812C0.671875 7.50937 0.998898 8.65902 1.62113 9.65043L6.57102 17.7509C6.65547 17.888 6.78203 17.9724 6.92972 17.9935C7.13004 18.0251 7.35163 17.9408 7.46761 17.7404L12.4317 9.56602C13.0222 8.59574 13.3281 7.46722 13.3281 6.32812C13.3281 2.83711 10.4911 0 7 0ZM7 9.49219C5.2282 9.49219 3.83594 8.05781 3.83594 6.32812C3.83594 4.58786 5.25973 3.16406 7 3.16406C8.74027 3.16406 10.1641 4.58786 10.1641 6.32812C10.1641 8.04727 8.79293 9.49219 7 9.49219Z" fill="#485FE0"/>
                                </svg>



                                {{ auth()->user()->address }}</p>

                        </div>
                        <div class="_line_"></div>

                        <div class="detail_section_container">
                            <div class="grid_cont">
                                <div class="grid__item">
                                    <h6>Sex</h6>
                                    <p>{{ auth()->user()->gender }}</p>
                                </div>
                                <div class="grid__item">
                                <h6>Date of Birth</h6>
                                    <p>{{ auth()->user()->date_of_birth }}</p>
                                </div>
                            </div>
                            <div class="grid_cont">
                                <div class="grid__item">
                                    <h6>Nationality</h6>
                                    <p>{{ auth()->user()->nationality }}</p>
                                </div>
                                <div class="grid__item">
                                <h6>State of Origin	</h6>
                                    <p>{{ auth()->user()->state_of_origin }}</p>
                                </div>
                            </div>
                            <div class="grid_cont">
                                <div class="grid__item">
                                    <h6>Occupation</h6>
                                    <p>{{ auth()->user()->occupation }}</p>
                                </div>
                                <div class="grid__item">
                                <h6>Banker	</h6>
                                    <p>-</p>
                                </div>
                            </div>
                            <div class="_line_1"></div>
                            <div class="grid_cont single_grid">
                                <div class="grid__item">
                                    <h6>Next of Kin Full Name	</h6>
                                    <p>{{ auth()->user()->next_of_kin_name }}</p>
                                </div>
                                <div class="grid__item">
                                    <h6>Next of Kin Residential Address	</h6>
                                    <p>{{ auth()->user()->next_of_kin_name }}</p>
                                </div>
                                <div class="grid__item">
                                    <h6>Next of Kin Phone Number</h6>
                                    <p>{{ auth()->user()->next_of_kin_phone }}</p>
                                </div>
                                </div>
                            </div>
                            <div class="_line_1"></div>
                            <div class="grid_cont single_grid">
                                <div class="grid__item">
                                    <h6>Location</h6>
                                    <p>{{ $project->name }}</p>
                                </div>
                                <div class="grid__item">
                                    <h6>Property</h6>
                                    <p><span id="propertyname"></span></p>
                                </div>
                                <div class="grid__item">
                                    <h6>Unit(s)</h6>
                                    <p><span id="propertyunits"></span></p>
                                </div>
                                <div class="grid__item">
                                    <h6>Payment Plan</h6>
                                    <p><span id="planname"></span></p>
                                </div>
                            </div>
                            <div class="_line_"></div>
                        </div>
                        <P class="agreement_">I <strong>{{ auth()->user()->name }}</strong> hereby affirm that the information provided above in partial fulfilment of the terms of
                            purchase of land with <strong>{{ \App\Helper::config('site_name') }}</strong> is true. I accept that any default in my monthly installment
                            for 3 (three) consecutive months will attract 10% addition of the default amount. Terms and conditions apply</P>


                        <div style="text-align: left;">Signature: <img src="{{ asset('storage/documents/digital_signatures/'.auth()->user()->digital_signature) }}"></div>
                        <div class="footer_sig">
                            <p class="_locat">Plot 408 Omofade Crescent, Omole Phase 1, Ikeja, Lagos.</p>
                            <p><svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g clip-path="url(#clip0)">
                                <path d="M16.3357 3.80492C16.3354 3.79912 16.3348 3.79332 16.3338 3.78755C16.3113 3.75541 16.2836 3.72776 16.2605 3.69626C16.0924 3.46733 15.9145 3.24698 15.7267 3.03519C15.6753 2.97732 15.6238 2.92009 15.5711 2.86349C15.3778 2.65645 15.175 2.45902 14.9628 2.27127C14.9236 2.23653 14.8869 2.20055 14.8477 2.16645C14.3247 1.71842 13.7521 1.33176 13.1411 1.01414C13.1166 1.00129 13.0909 0.990362 13.0658 0.977476C12.7937 0.839197 12.5147 0.714896 12.2299 0.60514C12.1656 0.582005 12.107 0.560114 12.046 0.540823C11.7926 0.449529 11.5354 0.369576 11.2743 0.300964C11.1971 0.280392 11.12 0.259179 11.0415 0.241169C10.7843 0.181373 10.5271 0.135707 10.2641 0.098406C10.1844 0.0868388 10.1059 0.0713907 10.0255 0.0617451C9.34809 -0.0205817 8.66321 -0.0205817 7.98576 0.0617451C7.90539 0.0713907 7.82695 0.0868388 7.74718 0.098406C7.48419 0.135707 7.2244 0.181373 6.96973 0.241169C6.89128 0.259179 6.81412 0.280392 6.73695 0.300964C6.47415 0.369538 6.21696 0.449491 5.9653 0.540823C5.90099 0.562677 5.84247 0.584568 5.7814 0.60514C5.49659 0.714896 5.21754 0.839159 4.94543 0.977476C4.92034 0.990324 4.89464 1.00125 4.87019 1.01414C4.25713 1.33142 3.68257 1.71804 3.15775 2.16645C3.11853 2.20051 3.08187 2.23653 3.04264 2.27127C2.82829 2.45989 2.62551 2.65732 2.43433 2.86349C2.38162 2.92009 2.33015 2.97732 2.27872 3.03519C2.09052 3.24653 1.9126 3.46687 1.74501 3.69622C1.72187 3.72772 1.69422 3.75538 1.67169 3.78751C1.66875 3.79313 1.66615 3.79893 1.66396 3.80488C-0.554003 6.91322 -0.554003 11.0868 1.66396 14.1951C1.66615 14.2011 1.66871 14.2069 1.67169 14.2125C1.69418 14.2446 1.72184 14.2723 1.74501 14.3038C1.91264 14.5327 2.09052 14.7531 2.27872 14.9649C2.33015 15.0227 2.38162 15.08 2.43433 15.1366C2.62811 15.3436 2.83086 15.541 3.04264 15.7288C3.08187 15.7635 3.11853 15.7995 3.15775 15.8336C3.68072 16.2816 4.25336 16.6683 4.86438 16.9859C4.88884 16.9988 4.91453 17.0097 4.93963 17.0226C5.21174 17.1609 5.49075 17.2852 5.77559 17.3949C5.83991 17.418 5.89842 17.4399 5.9595 17.4592C6.21285 17.5505 6.47008 17.6305 6.73115 17.6991C6.80832 17.7197 6.88548 17.7409 6.96393 17.7589C7.22116 17.8187 7.47835 17.8643 7.74138 17.9016C7.82111 17.9132 7.89955 17.9287 7.97996 17.9383C8.65737 18.0206 9.34225 18.0206 10.0197 17.9383C10.1001 17.9287 10.1785 17.9132 10.2583 17.9016C10.5213 17.8643 10.7811 17.8187 11.0357 17.7589C11.1142 17.7409 11.1913 17.7197 11.2685 17.6991C11.5317 17.6305 11.789 17.5506 12.0402 17.4592C12.1045 17.4374 12.163 17.4155 12.2241 17.3949C12.5089 17.2852 12.7879 17.1609 13.06 17.0226C13.0851 17.0097 13.1108 16.9988 13.1353 16.9859C13.7463 16.6683 14.3189 16.2816 14.8419 15.8336C14.8811 15.7995 14.9178 15.7635 14.957 15.7288C15.1714 15.5406 15.3741 15.3432 15.5653 15.1366C15.618 15.08 15.6695 15.0227 15.7209 14.9649C15.9091 14.7531 16.0871 14.5327 16.2547 14.3038C16.2778 14.2723 16.3054 14.2446 16.328 14.2125C16.3309 14.2069 16.3335 14.2011 16.3357 14.1951C18.5537 11.0869 18.5537 6.91326 16.3357 3.80492ZM15.602 5.02159C16.217 6.03442 16.5883 7.17621 16.6868 8.35705H12.835C12.7878 7.59083 12.6672 6.83093 12.4749 6.08777C13.5629 5.884 14.6161 5.52493 15.602 5.02159ZM10.639 1.46104C10.675 1.4694 10.7097 1.48097 10.7457 1.48933C10.9759 1.54336 11.2042 1.60508 11.4274 1.68225C11.4615 1.69381 11.4949 1.70734 11.5283 1.71955C11.7495 1.79671 11.9675 1.88224 12.181 1.97678C12.2183 1.99415 12.255 2.01344 12.2923 2.03081C12.4968 2.12813 12.6965 2.23336 12.8916 2.34655L13.0202 2.42436C13.2054 2.53754 13.3855 2.65864 13.5604 2.78769C13.6054 2.82047 13.6504 2.852 13.6948 2.88799C13.8662 3.01658 14.0304 3.15441 14.1873 3.30147C14.2285 3.33877 14.2709 3.37543 14.3114 3.41401C14.4761 3.57026 14.6329 3.73552 14.7847 3.90658C14.804 3.92907 14.8246 3.94964 14.8439 3.97089C13.9732 4.38584 13.0508 4.68215 12.1013 4.85185C11.6615 3.64268 11.0804 2.48965 10.3703 1.41673C10.4596 1.43278 10.5509 1.44178 10.639 1.46104ZM6.45407 8.35701C6.50478 7.66125 6.62324 6.97207 6.80775 6.29929C7.53633 6.38584 8.2694 6.42876 9.00311 6.42789C9.73749 6.42766 10.4712 6.38365 11.2004 6.29605C11.3846 6.96993 11.5024 7.66019 11.5521 8.35701H6.45407ZM11.5521 9.64312C11.5014 10.3389 11.3829 11.0281 11.1984 11.7008C10.4698 11.6143 9.73678 11.5714 9.00307 11.5722C8.26872 11.5714 7.53501 11.6143 6.80579 11.7008C6.62181 11.028 6.50403 10.3388 6.45403 9.64312H11.5521V9.64312ZM9.00307 1.70286C9.73486 2.74002 10.3367 3.86302 10.7952 5.04668C10.2 5.10991 9.60174 5.14167 9.00307 5.14186C8.40508 5.14114 7.8075 5.10919 7.21283 5.04604C7.6719 3.86317 8.27306 2.74055 9.00307 1.70286ZM3.22214 3.9059C3.37327 3.73484 3.5308 3.56958 3.69542 3.41333C3.73592 3.37475 3.77839 3.33809 3.81953 3.30079C3.97816 3.15633 4.14236 3.0185 4.3121 2.88731C4.35648 2.85321 4.40147 2.82299 4.4465 2.78701C4.6214 2.65841 4.80146 2.53728 4.98665 2.42368L5.11525 2.34587C5.31031 2.23186 5.51008 2.12658 5.71456 2.03013C5.75186 2.01276 5.78852 1.99347 5.82582 1.9761C6.0393 1.87964 6.25731 1.79411 6.47852 1.71887C6.51194 1.70666 6.54284 1.69314 6.57946 1.68157C6.80259 1.60697 7.0296 1.54525 7.26109 1.48866C7.29711 1.48029 7.33182 1.46872 7.36848 1.461C7.45657 1.44171 7.5479 1.4327 7.63727 1.41661C6.92681 2.48973 6.34559 3.64298 5.90555 4.85238C4.95609 4.68267 4.03366 4.38637 3.16299 3.97142C3.18228 3.94897 3.20285 3.92839 3.22214 3.9059ZM2.40419 5.02159C3.38989 5.52489 4.4428 5.88397 5.53065 6.08777C5.33853 6.83097 5.21815 7.59086 5.1712 8.35705H1.31939C1.41789 7.17621 1.78924 6.03442 2.40419 5.02159ZM2.40419 12.9786C1.7892 11.9658 1.41785 10.824 1.31939 9.64312H5.1712C5.21837 10.4093 5.33894 11.1692 5.53129 11.9124C4.44326 12.1161 3.39011 12.4752 2.40419 12.9786ZM7.3672 16.5391C7.33118 16.5307 7.29647 16.5192 7.26045 16.5108C7.03024 16.4568 6.80195 16.395 6.57882 16.3179C6.54472 16.3063 6.5113 16.2928 6.47784 16.2806C6.25663 16.2034 6.03866 16.1179 5.82514 16.0234C5.78784 16.006 5.75118 15.9867 5.71388 15.9693C5.5094 15.872 5.30963 15.7668 5.11457 15.6536L4.98597 15.5758C4.80078 15.4626 4.62072 15.3415 4.44582 15.2124C4.40079 15.1797 4.3558 15.1481 4.31142 15.1121C4.13995 14.9835 3.97574 14.8457 3.81885 14.6987C3.77771 14.6614 3.73524 14.6247 3.69474 14.5861C3.53012 14.4299 3.37323 14.2646 3.22146 14.0936C3.20217 14.0711 3.1816 14.0505 3.16231 14.0292C4.03302 13.6143 4.95542 13.318 5.90487 13.1483C6.34472 14.3574 6.92576 15.5105 7.63592 16.5834C7.54658 16.5674 7.45529 16.5584 7.3672 16.5391ZM9.00307 16.2973C8.27128 15.2601 7.66941 14.1371 7.21091 12.9535C8.4018 12.8253 9.60302 12.8253 10.7939 12.9535L10.7933 12.9541C10.3343 14.137 9.73312 15.2596 9.00307 16.2973ZM14.784 14.0942C14.6329 14.2653 14.4754 14.4305 14.3108 14.5868C14.2703 14.6254 14.2278 14.662 14.1866 14.6993C14.028 14.8443 13.8638 14.9821 13.6941 15.1128C13.6497 15.1469 13.6047 15.1803 13.5597 15.2131C13.3848 15.3417 13.2047 15.4629 13.0195 15.5765L12.8909 15.6543C12.6963 15.7679 12.4965 15.8731 12.2916 15.97C12.2543 15.9874 12.2177 16.0067 12.1804 16.024C11.9669 16.1205 11.7489 16.206 11.5277 16.2813C11.4942 16.2935 11.4633 16.307 11.4267 16.3186C11.2036 16.3932 10.9766 16.4549 10.7451 16.5115C10.7091 16.5198 10.6744 16.5314 10.6377 16.5391C10.5496 16.5584 10.4583 16.5674 10.3689 16.5835C11.0791 15.5106 11.6601 14.3576 12.1 13.1484C13.0494 13.3181 13.9718 13.6144 14.8425 14.0293C14.8239 14.0512 14.8033 14.0717 14.784 14.0942ZM15.602 12.9786C14.6163 12.4753 13.5634 12.1162 12.4755 11.9124C12.6677 11.1692 12.788 10.4093 12.835 9.64312H16.6868C16.5883 10.8239 16.2169 11.9657 15.602 12.9786Z" fill="#485FE0"/>
                                </g>
                                <defs>
                                <clipPath id="clip0">
                                <rect width="18" height="18" fill="white"/>
                                </clipPath>
                                </defs>
                                </svg>
                                <a href="{{ \App\Helper::config('site_url') }}">{{ \App\Helper::config('site_url') }}</a>
                                <span>
                                    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <g clip-path="url(#clip0)">
                                        <path d="M0.00362169 6.88212C0.00548506 6.86145 0.00959877 6.84102 0.0159272 6.82123C0.0200407 6.80355 0.0257712 6.78628 0.0330138 6.76962C0.0414165 6.75334 0.0514716 6.73794 0.0630034 6.7237C0.0745704 6.70679 0.087966 6.69122 0.102908 6.67719C0.107725 6.673 0.109799 6.66727 0.114897 6.66309L3.00014 4.42229V2.09989C3.00014 1.60283 3.40312 1.19985 3.90019 1.19985H7.15146L8.45326 0.18909C8.77485 -0.0617278 9.22582 -0.0617278 9.54741 0.18909L10.8495 1.19985H14.1008C14.5978 1.19985 15.0008 1.60279 15.0008 2.09989V4.42233L17.8851 6.66312C17.8902 6.66734 17.8923 6.67304 17.8971 6.67722C17.9121 6.69122 17.9255 6.70683 17.937 6.72374C17.9486 6.73798 17.9586 6.75334 17.967 6.76965C17.9743 6.78628 17.98 6.80358 17.9841 6.82127C17.9904 6.84078 17.9945 6.86089 17.9964 6.88128C17.9964 6.88757 18 6.89299 18 6.89928V17.1006C17.9996 17.2915 17.9378 17.4771 17.8239 17.6301C17.8221 17.6329 17.8218 17.6362 17.8197 17.6385C17.8176 17.6409 17.8131 17.6436 17.8101 17.647C17.6416 17.8687 17.3794 17.9995 17.1009 18.0007H0.900046C0.620399 17.9998 0.357171 17.8684 0.188413 17.6455C0.186022 17.6425 0.1824 17.6416 0.180326 17.6386C0.178217 17.6356 0.177935 17.6329 0.176107 17.6302C0.06216 17.4771 0.00042242 17.2915 5.23234e-07 17.1007V6.90012C3.56813e-05 6.89383 0.00330527 6.88842 0.00362169 6.88212ZM9.1805 0.662492C9.07573 0.579238 8.92736 0.579238 8.82259 0.662492L8.13047 1.19981H9.87055L9.1805 0.662492ZM17.0253 17.4007L9.18047 11.307C9.07563 11.2239 8.92736 11.2239 8.82256 11.307L0.975671 17.4007H17.0253ZM0.600043 16.9329L8.45326 10.8336C8.77492 10.583 9.22575 10.583 9.54741 10.8336L17.4009 16.9329V7.35704L12.4845 11.1747C12.3535 11.2763 12.165 11.2525 12.0635 11.1215C11.9619 10.9905 11.9857 10.802 12.1167 10.7004L17.1114 6.82211L15.0008 5.18192V7.50013C15.0008 7.66583 14.8664 7.80014 14.7008 7.80014C14.5351 7.80014 14.4008 7.6658 14.4008 7.50013V2.09986C14.4008 1.93416 14.2665 1.79985 14.1008 1.79985H3.90026C3.73456 1.79985 3.60025 1.93416 3.60025 2.09986V7.50013C3.60025 7.66583 3.46595 7.80014 3.30025 7.80014C3.13455 7.80014 3.00025 7.6658 3.00025 7.50013V5.18192L0.88964 6.82211L5.89298 10.7073C5.97905 10.7725 6.02345 10.879 6.00918 10.9861C5.9949 11.0931 5.9242 11.1842 5.82403 11.2246C5.72387 11.265 5.60971 11.2485 5.52516 11.1813L0.600113 7.35704V16.9329H0.600043Z" fill="#485FE0"/>
                                        <path d="M5.40072 7.1994V5.99935C5.40072 4.01102 7.01257 2.39917 9.0009 2.39917C10.9892 2.39917 12.6011 4.01102 12.6011 5.99935C12.6011 7.98768 10.9892 9.59953 9.0009 9.59953C8.8352 9.59953 8.7009 9.46523 8.7009 9.29953C8.7009 9.13383 8.8352 8.99953 9.0009 8.99953C10.6578 8.99953 12.001 7.65632 12.001 5.99939C12.001 4.34246 10.6578 2.99925 9.0009 2.99925C7.34397 2.99925 6.00076 4.34246 6.00076 5.99939V7.19944C6.00076 7.53084 6.26941 7.79948 6.60081 7.79948C6.9322 7.79948 7.20085 7.53084 7.20085 7.19944V5.99939C7.20085 5.83369 7.33515 5.69938 7.50085 5.69938C7.66655 5.69938 7.80085 5.83369 7.80085 5.99939C7.80085 6.66215 8.33814 7.19944 9.0009 7.19944C9.66366 7.19944 10.2009 6.66215 10.2009 5.99939C10.2009 5.33662 9.66366 4.79934 9.0009 4.79934C8.8352 4.79934 8.7009 4.66503 8.7009 4.49934C8.7009 4.33364 8.8352 4.19933 9.0009 4.19933C9.80654 4.19754 10.5153 4.73127 10.7361 5.50609C10.9569 6.2809 10.636 7.10806 9.95041 7.53122C9.26483 7.95439 8.38159 7.8705 7.78795 7.3258C7.72263 7.96026 7.17117 8.43201 6.53425 8.39829C5.89729 8.36454 5.39872 7.83724 5.40072 7.1994Z" fill="#485FE0"/>
                                        </g>
                                        <defs>
                                        <clipPath id="clip0">
                                        <rect width="18" height="18" fill="white" transform="matrix(-1 0 0 1 18 0)"/>
                                        </clipPath>
                                        </defs>
                                        </svg>
                                         <a href="mailto:{{ \App\Helper::config('site_email') }}">{{ \App\Helper::config('site_email') }}</a>
                                    </span>
                            </p>

                                <p> <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M16.5789 11.8516C15.4822 11.8537 14.3923 11.6804 13.3503 11.3381C12.8631 11.168 12.3218 11.2826 11.9454 11.6356L9.90097 13.1779C7.7054 12.0756 5.92393 10.2945 4.82119 8.09907L6.32276 6.10391C6.70187 5.72454 6.83744 5.16531 6.67425 4.65444C6.33071 3.61108 6.1564 2.51948 6.15791 1.42105C6.15791 0.636232 5.52168 0 4.73686 0H1.42105C0.636232 0 0 0.636232 0 1.42105C0.0104359 10.573 7.42696 17.9896 16.5789 18C17.3638 18 18 17.3638 18 16.5789V13.2726C18 12.4878 17.3638 11.8516 16.5789 11.8516Z" fill="#009688"/>
                                </svg>&nbsp;
                                <a href="tel:+2348034770607" >08034770607</a>  ,<a href="tel:+2348168034525" >08168034525</a> </p>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
            </div>
        </div>
    </div>
    <!-- End Subscription Preview Modal -->
    <script src="https://unpkg.com/vue"></script>
@endsection
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js"></script>

<script type="application/javascript">
    function nano()
    {
        let plot_id = document.getElementById('plotOption').value;
        axios.get('/plot-detail/'+plot_id).then(function (plot) {
            var output = '<option value = "">Choose Payment Option</option>';
            let units = document.getElementById('unitOption').value;
            $.each(plot.data.distribution, function (key, value) {
                let displayAmount = value.installment * units;
                output += "<option value = \"".concat(value.id, "\">").concat(value.name +" (&#8358;"+ displayAmount.toLocaleString()+")", "</option>");
            });
            $('#planOption').html(output);
            // $('#installment').html(plot.data.initial_deposit);

        })["catch"](function (error) {
            console.log(error);
        });
        dodo();

    }

    function dodo()
    {
        let plotdd = document.getElementById('plotOption');
        let plotName = plotdd.options[plotdd.selectedIndex].text;
        let unitdd = document.getElementById('unitOption');
        let unitName = unitdd.options[unitdd.selectedIndex].text;
        let plandd = document.getElementById('planOption');
        let planName = plandd.options[plandd.selectedIndex].text;

        document.getElementById('propertyname').innerText = plotName;
        document.getElementById('propertyunits').innerText = unitName;
        document.getElementById('planname').innerText = planName;

    }


    function planOp()
    {
        let plan_id = document.getElementById('planOption').value;
        axios.get('/plan-detail/'+plan_id).then(function (plan) {
            let months = (plan.data.years * 12) / plan.data.months;
            console.log(months)
            // $('#months').html(months)
        })
        dodo();
    }

    /**
     * Confirm before submitting a form
     */
    function checksubmit() {
        var txt;
        var r = confirm("Are you sure you want to submit?");
        if (r == true) {
            document.getElementById("subForm").addEventListener("click", function () {
                form.submit();
            });
        } else {
            txt = "You pressed Cancel!";
        }
    }
    var form = document.getElementById("subForm");
</script>

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush
