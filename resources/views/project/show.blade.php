@extends('layouts.app')

@section('content')
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <!-- Card stats -->
                <div class="row">

                    <div class="col-xl-3 col-lg-6">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0 page_title">Properties</h3>
                                <div class="text_under_line"></div>
                            </div>
                            <div class="project_card_grid" id="pPage">

                            @foreach($projects as $project)
                            <a href="{{ url('dashboard/subscription/project/'.$project->slug) }}" class="card_item">
                                <div class="card_header">
                                    <div class="img_container">
                                        <img src="{{ asset('storage/images/projects/'.$project->image) }}" alt="project">
                                        <span>Subscribe</span>
                                    </div>
                                    <h4 class="title">{{ $project->name }}</h4>
                                    <div class="details-container" data-maxlength="100">
                                        <p class="details">{{ $project->description }}</p>
                                    </div>
                                </div>
                               <div class="card_footer">
                                    <svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M4.00001 0C1.79445 0 0 1.79445 0 4.00001C0 4.66212 0.165539 5.3186 0.480234 5.90088L3.78127 11.8711C3.82521 11.9507 3.90895 12 4.00001 12C4.09106 12 4.1748 11.9507 4.21875 11.8711L7.521 5.89891C7.83448 5.3186 8.00002 4.66209 8.00002 3.99998C8.00002 1.79445 6.20557 0 4.00001 0ZM4.00001 6C2.89723 6 2.00002 5.10279 2.00002 4.00001C2.00002 2.89723 2.89723 2.00002 4.00001 2.00002C5.10279 2.00002 6 2.89723 6 4.00001C6 5.10279 5.10279 6 4.00001 6Z" fill="#162683"/>
                                    </svg>

                                    <small> {{ $project->location }}, {{ $project->country }}</small>
                               </div>
                            </a>
                            @endforeach
                        </div>

                    </div>

                    <div class="col-12"></div>

                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-center" aria-label="...">
                            {{ $projects->links() }}
                        </nav>
                    </div>
                </div>
            </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>

@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush
