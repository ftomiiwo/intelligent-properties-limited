@extends('layouts.admin')

@section('content')
    <link rel="stylesheet" href="{{ asset('vendors/chosen/chosen.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <div class="col-lg-9">
        <div class="card">
            <div class="card-header">
                <strong>Project</strong> Form
            </div>
            <div class="card-body card-block">
                <form action="{{ url('admin/plot/update', $plot->id) }}" method="post" enctype="multipart/form-data" class="">
                    @csrf
                    <div class="form-group">
                        <label for="nf-name" class=" form-control-label">Project Name</label>
                        <input type="text" id="nf-name" name="name"
                               value="{{ old(' name', $plot->name) }}"
                               placeholder="450 sqm" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-project_id" class="form-control-label">Project Location</label>
                        <select name="project_id" id="nf-project_id" class="form-control">
                            <option value selected disabled>Please select location</option>
                            @foreach($project_locations as $project_location)
                                <option value="{{ $project_location->id }}" @if(old('project_id') == $project_location->id) selected @elseif($plot->project_id == $project_location->id) selected  @endif
                                >{{ $project_location->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nf-price" class=" form-control-label">Price (in Naira) for Installmental Payment Plans</label>
                        <input type="number" id="nf-price" name="price" value="{{ old('price', $plot->price) }}"
                               placeholder="₦1,000,000" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-one_time_price" class=" form-control-label">Price (in Naira) for One-Time Payment</label>
                        <input type="number" id="nf-one_time_price" name="one_time_price" value="{{ old('price', $plot->one_time_price) }}"
                               placeholder="₦1,000,000" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-initial_deposit" class=" form-control-label">Initial Deposit (in Naira)</label>
                        <input type="number" id="nf-initial_deposit" name="initial_deposit" value="{{ old('initial_deposit', $plot->initial_deposit) }}"
                               placeholder="₦100,000" class="form-control">
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <label for="nf-payment_plans" class=" form-control-label">Select Payment Plans (Multiple plans can be selected)</label>
                        </div>

                        <div class="card-body">
                            <select data-placeholder="Choose a country..." multiple class="standardSelect form-control"
                                    id="nf-payment_plans" name="payment_plans[]" required>
{{--                                <option value selected disabled></option>--}}
                                @foreach($plans as $plan)
                                    <option @if(in_array($plan->id, $project_plans)) selected @endif value="{{ $plan->id }}">{{ $plan->name }}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nf-status" class="form-control-label">Project Status</label>
                        <select name="status_id" id="nf-status" class="form-control" required>
                            <option value selected disabled>Please select status</option>
                            @foreach($statuses as $status)
                                <option value="{{ $status->id }}" @if(old('status_id') == $status->id) selected @elseif($plot->status_id == $status->id) selected  @endif
                                >{{ $status->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                        <a href="{{ url('admin/plot/all') }}" type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Reset
                        </a>
                    </div>
                </form>

            </div>

        </div>
    </div>
@endsection

@section('assets')
    <script src="{{ asset('vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendors/jquery-validation-unobtrusive/dist/jquery.validate.unobtrusive.min.js') }}"></script>

    <script src="{{ asset('vendors/chosen/chosen.jquery.min.js') }}"></script>

    <script>
        jQuery(document).ready(function() {
            jQuery(".standardSelect").chosen({
                disable_search_threshold: 10,
                no_results_text: "Oops, nothing found!",
                width: "100%"
            });
        });
    </script>
@endsection
