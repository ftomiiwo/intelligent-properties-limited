@extends('layouts.admin')

@include('layouts.table')
@section('content')
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Payments Table</strong>
                        </div>
                        <div class="card-body">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{ url('admin/payment/add')  }}"class="btn btn-outline-danger btn-lg"
                                       type="button" ><i class="fa fa-plus"></i> Create Payment</a>
                                </div>
                            </div>
                            <!-- /# card -->
                            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <td>ID</td>
                                    <th>Reference</th>
                                    <th>Customer Name</th>
                                    <th>Customer Email</th>
                                    <th>Amount</th>
                                    <th>Date Created</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($payments as $payment)
                                    <tr>
                                        <td>{{ $payment->id }}</td>
                                        <td>#{{ $payment->reference }}</td>
                                        <td><a href="{{ url('admin/user/edit/'.$payment->user->id)  }}">{{ $payment->user->name }}</a></td>
                                        <td><a href="{{ url('admin/user/edit/'.$payment->user->id)  }}">{{ $payment->user->email }}</a></td>
                                        <td>₦{{ number_format($payment->amount) }}</td>
                                        <td>{{ $payment->created_at }}</td>
                                        <td>{{ $payment->status->name }}</td>
                                        <td>
                                            <div class="dropdown for-notification">
                                                <button class="btn btn-outline-danger dropdown-toggle" type="button" id="notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-gear"></i>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="notification">
{{--                                                    <a class="dropdown-item media" href="{{ url('admin/payment/show/'.$payment->id)  }}">--}}
{{--                                                        <i class="fa fa-eye"></i>--}}
{{--                                                        <p>View</p>--}}
{{--                                                    </a>--}}
                                                    <a class="dropdown-item media" href="{{ url('admin/payment/edit/'.$payment->id)  }}">
                                                        <i class="fa fa-wrench"></i>
                                                        <p>View/Edit</p>
                                                    </a>
                                                    @if(is_null($payment->confirmation_file))
                                                    <a class="dropdown-item media" href="{{ url('admin/payment/confirmation/'.$payment->id)  }}">
                                                        <i class="fa fa-wrench"></i>
                                                        <p>Generate Confirmation Letter</p>
                                                    </a>
                                                    @else
                                                    <a class="dropdown-item media" target="_blank" href="{{ asset('storage/documents/confirmations/'.$payment->confirmation_file) }}">
                                                        <i class="fa fa-wrench"></i>
                                                        <p>Download Confirmation Document</p>
                                                    </a>
                                                    @endif
                                                    <a class="dropdown-item media" onclick='confirm("Are you sure you want to delete payment #{{ $payment->reference }}?")' href="{{ url('admin/payment/delete/'.$payment->id)  }}">
                                                        <i class="fa fa-times"></i>
                                                        <p>Delete</p>
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection
{{--@section('assets')--}}
{{--    <script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/jszip/dist/jszip.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.colVis.min.js') }}"></script>--}}
{{--    <script src="{{ asset('assets/js/init-scripts/data-table/datatables-init.js') }}"></script>--}}
{{--@endsection--}}
