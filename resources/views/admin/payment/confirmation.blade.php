<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>{{ \App\Helper::config('site_name') }} Receipt</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('assets/css/dom.css') }}">
    </head>
    <body>
        <div class="container">
            <div style="text-align: right;"><img src="https://dashboard.intelligenceproperties.com/storage/images/{{ \App\Helper::config('site_logo') }}"></div>
            <p id="address">
                {{ $date_created }}<strong> <br style="text-transform: capitalize"> {{ strtoupper($payment->user->name) }}, </strong><br>
                        <strong>{{ strtoupper($payment->user->address) }}.</strong><br><br>
                @if($payment->user->gender == 'Male')
                Dear Sir,
                @elseif($payment->user->gender == 'Female')
                Dear Ma,
                @endif
            </p>
            <div>
                <br>
                <div id="topic" style="text-align: center;"><h4>ACKNOWLEDGMENT OF RECEIPT</h4></div>
                <P>This is to ascertain that <strong>{{ $payment->user->name }}</strong>
                    has made a part payment for {{ $units }} of <strong>{{ $payment->subscription->plot->name }}</strong> at
                    {{ $payment->subscription->plot->project->name }}, {{ $payment->subscription->plot->project->location }}.
                    This is a part of the parcel of land belonging to {{ \App\Helper::config('site_name') }} </P>

                <p>A sum of <strong>{{ $payment_received_in_words }} Naira (N{{number_format($payment_received)}})</strong> has been received.
                    @if($payment->subscription->plan->id != 1)
                    A balance of <strong>{{ $all_pending_scheduled_payments_sum_in_words }} Naira (N{{ number_format($all_pending_scheduled_payments_sum) }})</strong> for a total sum of
                    <strong>{{ $all_scheduled_payments_sum_in_words }} Naira (N{{ number_format($all_scheduled_payments_sum) }})</strong> is expected to be paid
                    within {{ $plan_months }} Months. This is with a <strong style="text-transform: lowercase;">{{ $payment->subscription->plan->interval->name }}</strong>
                    installment of <strong>{{ $installment_in_words }} Naira Only (N{{ number_format($installment) }}).</strong>
                    @endif
                    This is the agreed sum with the subscriber for this plot of land at this location as at the time of purchase.</p>

                <p>Thanks for sharing in the vision of securing a good living environment for a million people.</p>
            </div>
            <div style="text-align: left;"><img src="https://dashboard.intelligenceproperties.com/storage/images/ceo_signature.png"></div>
            <p>................................................<br>
                <strong>BUKOLA OLUGBENGA</strong>
                <br>Chief Executive Officer<br>Intelligence Global Projects Ltd.
            </p>

            <div style="text-align:center;background:grey;color:black;position:fixed;left:0px;bottom:50px;width:100%;">
                <p><strong>Plot 408 Omofade Crescent, Omole Phase 1, Ikeja, Lagos.</strong><br>
                Website: {{ \App\Helper::config('main_website_url') }} Email: intelligenceglobalproperties@gmail.com Tel:08034770607,08168034525</p>
            </div>
        </div>
    </body>
</html>
