@extends('layouts.admin')

@section('content')

    <div class="col-lg-9">
        <div class="card">
            <div class="card-header">
                <strong>Payment</strong> Form
            </div>
            <div class="card-body card-block">
                <form action="{{ url('admin/payment/update/'.$payment->id) }}" method="post" enctype="multipart/form-data" class="">
                    @csrf
{{--                    <div class="form-group">--}}
{{--                        <label for="nf-name" class=" form-control-label">Customer Email</label>--}}
{{--                        <input type="email" id="nf-email" name="email"--}}
{{--                               value="@if(old('email')){{ old('email') }}@else{{$payment->user->email}}@endif"--}}
{{--                               placeholder="customer@google.com" class="form-control">--}}
{{--                    </div>--}}
                    <div class="form-group">
                        <label for="nf-subscription_id" class="form-control-label">Subscription Record</label>
                        <select name="subscription_id" id="nf-subscription_id" class="form-control">
                            <option value selected disabled>Please select subscription record</option>
                            @foreach($subscriptions as $subscription)
                                <option value="{{ $subscription->id }}" @if(old('subscription_id', $payment->subscription_id) == $subscription->id) selected @endif>
                                    {{ $subscription->plot->name }} - {{ $subscription->plot->project->name }} - {{ $subscription->user->name }} - {{ $subscription->units }} Unit(2)</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nf-amount" class=" form-control-label">Amount (in Naira)</label>
                        <input type="number" id="nf-price" name="amount" step="0.01"
                               value="@if(old('amount')){{ old('amount') }}@else{{$payment->amount}}"@endif
                               placeholder="₦1,000,000" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-gateway" class="form-control-label">Payment Gateway</label>
                        <select name="gateway_id" id="nf-gateway" class="form-control">
                            <option value selected disabled>Please select gateway</option>
                            @foreach($gateways as $gateway)
                                <option value="{{ $gateway->id }}" @if(old('gateway_id') == $gateway->id || $payment->gateway_id == $gateway->id) selected @endif
                                >{{ $gateway->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="nf-status" class="form-control-label">Project Status</label>
                        <select name="status_id" id="nf-status" class="form-control">
                            <option value selected disabled>Please select status</option>
                            @foreach($statuses as $status)
                                <option value="{{ $status->id }}" @if(old('status_id') == $status->id || $payment->status_id == $status->id) selected @endif
                                >{{ $status->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    {{--                    <div class="form-group">--}}
                    {{--                        <label for="nf-remark" class=" form-control-label">Payment remark</label>--}}
                    {{--                        <textarea name="remark" id="nf-remark" cols="30" rows="10" placeholder=""--}}
                    {{--                                  class="form-control">@if(old('remark')) {{ old('remark') }} @endif</textarea>--}}
                    {{--                    </div>--}}
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                        <a href="{{ url('admin/payment/all') }}" type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Reset
                        </a>
                    </div>
                </form>

            </div>

        </div>
    </div>
@endsection
@section('assets')

    <script src="{{ asset('vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendors/jquery-validation-unobtrusive/dist/jquery.validate.unobtrusive.min.js') }}"></script>
@endsection
