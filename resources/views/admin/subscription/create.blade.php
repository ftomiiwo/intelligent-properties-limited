@extends('layouts.admin')

@section('content')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <div class="col-lg-9">
        <div class="card">
            <div class="card-header">
                <strong>Subscription</strong> Form
            </div>
            <div class="card-body card-block">
                <form action="{{ url('admin/subscription/create') }}" method="post" enctype="multipart/form-data" class="">
                    @csrf
                    <div class="form-group">
                        <label for="nf-customer" class="form-control-label">Customer Name</label>
                        <select name="user_id" id="nf-customer" class="form-control">
                            <option value selected disabled>Please select Customer Name</option>
                            @foreach($customers as $customer)
                                <option value="{{ $customer->id }}" @if(old('user_id') == $customer->id) selected @endif
                                >{{ $customer->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nf-project" class="form-control-label">Property Location</label>
                        <select name="project_id" id="nf-project" class="form-control" onchange="listPlots()" required>
                            <option value selected disabled>Please select Property Location</option>
                            @foreach($projects as $project)
                                <option value="{{ $project->id }}" @if(old('project_id') == $project->id) selected @endif
                                >{{ $project->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="plot_id" class="form-control-label">Property Dimension</label>
                        <select name="plot_id" id="plot_id" class="form-control" onchange="nano()" required>
                            <option value selected disabled>Please select Property Location</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="unitOption" class="form-control-label">Unit(s)</label>
                        <select name="units" id="unitOption" class="form-control" onchange="nano()" required>
                            @for($counter = 1; $counter < 101; $counter++)
                                <option value="{{ $counter }}" @if(old('units') == $counter) selected @endif
                                >{{ $counter }}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nf-plan" class="form-control-label">Subscription Plan</label>
                        <select name="plan_id" id="nf-plan" class="form-control" onchange="setAmount()">
                            <option value selected disabled>Please select Subscription Plan</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="nf-amount" class="form-control-label">Total Amount</label>
                        <input type="text" id="nf-amount" name="amount"
                               value="@if(old('amount')){{ old('amount') }}@else{{ $project->price }}@endif"
                               placeholder="Example: 1000000" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="nf-start_date" class="form-control-label">Start Date</label>
                        <input type="date" id="nf-start_date" name="start_date" value="{{ old('start_date') }}"
                               class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-status" class="form-control-label">Subscription Status (<strong>Hint:</strong> new Subscriptions should be set to <strong>Scheduled</strong>)</label>
                        <select name="status_id" id="nf-status" class="form-control">
                            <option value selected disabled>Please select status </option>
                            @foreach($statuses as $status)
                                <option value="{{ $status->id }}" @if(old('status_id') == $status->id) selected @endif
                                >{{ $status->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                        <a href="{{ url('admin/subscription/all') }}" type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Back
                        </a>
                    </div>
                </form>

            </div>

        </div>
    </div>
@endsection
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="application/javascript">
    function listPlots()
    {
        let project_id = document.getElementById('nf-project').value;
        axios.get('/project-plot/' + project_id).then(
            function (plots) {
                var options = '<option value selected disabled>Please select Property Location</option>';
                $.each(plots.data, function (key, value) {
                    options += '<option value=\"'+ value.id +'\">'+ value.name +'</option>';
                });
                $('#plot_id').html(options)
            }

        )
    }

    function nano()
    {
        let plot_id = document.getElementById('plot_id').value;
        axios.get('/plot-detail/'+plot_id).then(function (plot) {
            var output = '<option value selected disabled>Please select Subscription Plan</option>';
            let units = document.getElementById('unitOption').value;
            $.each(plot.data.distribution, function (key, value) {
                let displayAmount = value.installment * units;
                output += "<option value = \"".concat(value.id, "\">").concat(value.name +" (&#8358;"+ displayAmount.toLocaleString()+")", "</option>");
            });
            $('#nf-plan').html(output);
            // $('#installment').html(plot.data.initial_deposit);

        })["catch"](function (error) {
            console.log(error);
        });
    }

    function setAmount() {
        let plan_id = document.getElementById('nf-plan').value;
        let plot_id = document.getElementById('plot_id').value;
        let units = document.getElementById('unitOption').value;
        let totalAmount = 0;
        $('#nf-amount').val(totalAmount);
        axios.get('/project-amount/' + plot_id).then(function(project) {
            if(plan_id == 1) {
                totalAmount = project.data.one_time_price * units;
            } else {
                totalAmount = project.data.price * units;
            }
            $('#nf-amount').val(totalAmount);
        })
    }

    function planOp()
    {
        let plan_id = document.getElementById('nf-plan').value;
        axios.get('/plan-detail/'+plan_id).then(function (plan) {
            let months = (plan.data.years * 12) / plan.data.months;
            console.log(months)
        })
    }

</script>
@section('assets')

    <script src="{{ asset('vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendors/jquery-validation-unobtrusive/dist/jquery.validate.unobtrusive.min.js') }}"></script>
@endsection
