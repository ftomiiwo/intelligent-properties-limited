@extends('layouts.admin')
@include('layouts.table')
@section('content')


    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Customers Table</strong>
                        </div>
                        <div class="card-body">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{ url('admin/user/add')  }}" type="button" class="btn btn-outline-danger btn-lg"><i class="fa fa-plus"></i> Add User</a>
                                </div>
                            </div>
                            <!-- /# card -->
                            <table id="bootstrap-data-table-export" class="display nowrap table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>

                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone Number</th>
                                         <th>Verified</th>
                                        <th>Action</th>

                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($customers as $customer)
                                        <tr>
                                            <td>{{ $customer->id }}</td>
                                            <td>{{ $customer->name }}</td>
                                            <td>{{ $customer->email }}</td>
                                            <td>{{ $customer->phone }}</td>
                                            <td> @if(!is_null($customer->email_verified_at))<span class="badge badge-info">Verified</span>@else <span class="badge badge-warning">Unverified</span>@endif </td>
                                            <td>
                                                <div class="dropdown for-notification">
                                                    <button class="btn btn-outline-danger dropdown-toggle" type="button"
                                                            id="notification" data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        <i class="fa fa-gear"></i>
                                                    </button>
                                                    <div class="dropdown-menu" aria-labelledby="notification">
{{--                                                        <a class="dropdown-item media" href="{{ url('admin/user/show/'.$customer->id)  }}">--}}
{{--                                                            <i class="fa fa-eye"></i>--}}
{{--                                                            <p>View</p>--}}
{{--                                                        </a>--}}
                                                        <a class="dropdown-item media" href="{{ url('admin/user/edit/'.$customer->id)  }}" onclick="return confirm('Are you sure you want to view/edit user record?');">
                                                            <i class="fa fa-wrench"></i>
                                                            <p>View/Edit</p>
                                                        </a>
                                                        <a class="dropdown-item" href="{{ route('resend-signup-mail', $customer->id ) }}" onclick="return confirm('Are you sure you want to resend verification email?');">
                                                            <i class="fa fa-reply"></i>
                                                            <p>Resend Verification Email</p>
                                                        </a>

                                                        <a class="dropdown-item media" href="{{ url('admin/user/delete/'.$customer->id)  }}" onclick="return confirm('Are you sure you want to delete this user record. all user record will be deleted.');">
                                                            <i class="fa fa-times"></i>
                                                            <p>Delete</p>
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <div class="container">
                            <div class="card bg-light mt-3">
                                <div class="card-header">
                                    Users bulk upload. Excel file should have rows <strong>Name, Email, Phone</strong>
                                </div>
                                <div class="card-body">
                                    <form action="{{ route('users-import') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <input type="file" name="file" class="form-control" required>
                                        <br>
                                        <button class="btn btn-outline-danger btn-lg">Import User Data</button>
                                        <a class="btn btn-outline-warning btn-lg" href="{{ route('export') }}">Export Users Records</a>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

{{--@section('assets')--}}
{{--    <script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/jszip/dist/jszip.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.colVis.min.js') }}"></script>--}}
{{--    <script src="{{ asset('assets/js/init-scripts/data-table/datatables-init.js') }}"></script>--}}
{{--@endsection--}}
