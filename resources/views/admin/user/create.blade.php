@extends('layouts.admin')

@section('content')

    <div class="col-lg-9">
        <div class="card">
            <div class="card-header">
                <strong>Customer</strong> Form
            </div>
            <div class="card-body card-block">
                <form action="{{ url('admin/user/create') }}" method="post" enctype="multipart/form-data" class="">
                    @csrf
                    @if(auth()->user()->isRealtor())
                        <input type="hidden" name="role_id" value="1">
                    @else
                    <div class="form-group">
                        <label for="nf-role_id" class="form-control-label">Role</label>

                        <select name="role_id" id="nf-role_id" class="form-control">
                            <option value selected disabled>Please select role</option>
                            @foreach($roles as $role)
                            <option value="{{ $role->id }}" @if(old('role_id') == $role->id) selected @endif>{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    @endif
                    <div class="form-group">
                        <label for="nf-name" class=" form-control-label">Full Name</label>
                        <input type="text" id="nf-name" name="name" value="{{ old('name') }}"
                               placeholder="Name.." class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-phone" class=" form-control-label">Phone Number</label>
                        <input type="phone" id="nf-phone" name="phone" value="{{ old('phone') }}"
                               placeholder="081XXXXXXXX" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-email" class=" form-control-label">Email</label>
                        <input type="email" id="nf-email" name="email" value="{{ old('email') }}"
                               placeholder="Enter Email.." class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-avatar" class=" form-control-label">Profile Picture</label>
                        <input type="file" id="nf-avatar" name="avatar" value="{{ old('avatar') }}" class="form-control-file">
                    </div>
                    <div class="form-group">
                        <label for="nf-occupation" class="form-control-label">Occupation</label>
                        <input type="text" id="nf-occupation" name="occupation" value="{{ old('occupation') }}"
                               class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-gender" class="form-control-label">Gender</label>
                        <select name="gender" id="nf-gender" class="form-control">
                            <option value selected disabled>Please select gender</option>
                            <option value="Female" @if(old('gender') == 'Female') selected @endif>Female</option>
                            <option value="Male" @if(old('gender') == 'Male') selected @endif>Male</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nf-date_of_birth" class="form-control-label">Date of Birth</label>
                        <input type="date" id="nf-date_of_birth" name="date_of_birth" value="{{ old('date_of_birth') }}"
                               class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-nationality" class="form-control-label">Country</label>
                        <input type="text" id="nf-nationality" name="nationality" value="{{ old('nationality') }}"
                               class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-state_of_origin" class="form-control-label">State of Origin</label>
                        <input type="text" id="nf-state_of_origin" name="state_of_origin" value="{{ old('state_of_origin') }}"
                               class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-name" class=" form-control-label">Address</label>
                        <textarea name="address" id="nf-address" cols="30" rows="2"
                                  placeholder="2 Laurel Estate..." class="form-control">@if(old('address')) {{ old('address') }} @endif</textarea>
{{--                        <span class="help-block">Please enter Customers full address</span>--}}
                    </div>
                    <div class="form-group">
                        <label for="nf-next_of_kin_name" class=" form-control-label">Next of Kin Full Name</label>
                        <input type="text" id="nf-next_of_kin_name" name="next_of_kin_name" value="{{ old('next_of_kin_name') }}"
                               placeholder="Name.." class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-next_of_kin_name" class=" form-control-label">Next of Kin Address</label>
                        <textarea name="next_of_kin_address" id="nf-next_of_kin_address" cols="30" rows="2"
                                  placeholder="2 Laurel Estate..."
                                  class="form-control">@if(old('next_of_kin_address')) {{ old('next_of_kin_address') }} @endif</textarea>
                        {{--                        <span class="help-block">Please enter Customers full address</span>--}}
                    </div>
                    <div class="form-group">
                        <label for="nf-next_of_kin_phone" class=" form-control-label">Next of Kin Phone Number</label>
                        <input type="phone" id="nf-next_of_kin_phone" name="next_of_kin_phone"
                               value="{{old('next_of_kin_phone')}}"
                               placeholder="081XXXXXXXX" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-password" class=" form-control-label">Password</label>
                        <input type="password" id="nf-password" name="password" placeholder="Enter Password.." class="form-control">
{{--                        <span class="help-block">Please enter your password</span>--}}
                    </div>
                    <div class="form-group">
                        <label for="nf-status_id" class="form-control-label">Status</label>

                        <select name="status_id" id="nf-status_id" class="form-control">
                            <option value selected disabled>Please select Status</option>
                            @foreach($statuses as $status)
                                <option value="{{ $status->id }}" @if(old('status_id') == $status->id) selected @endif>{{ $status->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                        <a href="{{ url('admin/user/all') }}" type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Reset
                        </a>
                    </div>
                </form>

            </div>

        </div>
    </div>
@endsection
@section('assets')

    <script src="{{ asset('vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendors/jquery-validation-unobtrusive/dist/jquery.validate.unobtrusive.min.js') }}"></script>
@endsection
