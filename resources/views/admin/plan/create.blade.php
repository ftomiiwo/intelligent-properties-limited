@extends('layouts.admin')

@section('content')

    <div class="col-lg-9">
        <div class="card">
            <div class="card-header">
                <strong>Payment Plans</strong> Form
            </div>
            <div class="card-body card-block">
                <form action="{{ url('admin/plan/create') }}" method="post" enctype="multipart/form-data" class="">
                    @csrf
                    <div class="form-group">
                        <label for="nf-name" class=" form-control-label">Name</label>
                        <input type="text" id="nf-name" name="name" value="{{ old('name') }}"
                               placeholder="Quarterly Payment Plan" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-years" class=" form-control-label">Duration (in Years)</label>
                        <input type="number" id="nf-years" name="years" value="{{ old('years') }}"
                               step="0.01" placeholder="1" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-interval_id" class=" form-control-label">Interval (in Months)</label>
                        <select name="interval_id" id="nf-interval_id" class="form-control">
                            <option value selected disabled>Please select Interval</option>
                            @foreach($intervals as $interval)
                                <option value="{{ $interval->id }}" @if(old('$interval_id') == $interval->id) selected @endif>
                                    {{ $interval->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="nf-status" class="form-control-label">Project Status</label>
                        <select name="status_id" id="nf-status" class="form-control">
                            <option value selected disabled>Please select status</option>
                            @foreach($statuses as $status)
                                <option value="{{ $status->id }}" @if(old('status_id') == $status->id) selected @endif>
                                    {{ $status->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nf-description" class=" form-control-label">Project Description</label>
                        <textarea name="description" id="nf-description" cols="30" rows="5" placeholder=""
                                  class="form-control">@if(old('description')) {{ old('description') }} @endif</textarea>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                        <a href="{{ url('admin/plan/all') }}" type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Back
                        </a>
                    </div>
                </form>

            </div>

        </div>
    </div>
@endsection
@section('assets')

    <script src="{{ asset('vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendors/jquery-validation-unobtrusive/dist/jquery.validate.unobtrusive.min.js') }}"></script>
@endsection
