@extends('layouts.admin')

@include('layouts.table')
@section('content')


    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Plans Table</strong>
                        </div>
                        <div class="card-body">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{ url('admin/plan/add')  }}"class="btn btn-outline-danger btn-lg"
                                       type="button" ><i class="fa fa-plus"></i> Create Payment Plan</a>
                                </div>
                            </div>
                            <!-- /# card -->
                            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <td>ID</td>
                                    <th>Name</th>
                                    <th>Years</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($plans as $plan)
                                    <tr>
                                        <td>{{ $plan->id }}</td>
                                        <td>{{ $plan->name }}</td>
                                        <td>{{ $plan->years }}</td>
                                        <td>{{ $plan->description }}</td>
                                        <td>{{ $plan->status->name }}</td>
                                        <td>
                                            <div class="dropdown for-notification">
                                                <button class="btn btn-outline-danger dropdown-toggle" type="button" id="notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-gear"></i>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="notification">
{{--                                                    <a class="dropdown-item media" href="{{ url('admin/plan/show/'.$plan->id)  }}">--}}
{{--                                                        <i class="fa fa-eye"></i>--}}
{{--                                                        <p>View</p>--}}
{{--                                                    </a>--}}
                                                    <a class="dropdown-item media" href="{{ url('admin/plan/edit/'.$plan->id)  }}">
                                                        <i class="fa fa-wrench"></i>
                                                        <p>View/Edit</p>
                                                    </a>
                                                    <a class="dropdown-item media" onclick='confirm("Are you sure you want to delete payment plan #{{ $plan->name }}?")' href="{{ url('admin/plan/delete/'.$plan->id)  }}">
                                                        <i class="fa fa-times"></i>
                                                        <p>Delete</p>
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection
{{--@section('assets')--}}
{{--    <script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/jszip/dist/jszip.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.colVis.min.js') }}"></script>--}}
{{--    <script src="{{ asset('assets/js/init-scripts/data-table/datatables-init.js') }}"></script>--}}
{{--@endsection--}}
