@extends('layouts.admin')

@section('content')

    <div class="col-lg-9">
        <div class="card">
            <div class="card-header">
                <strong>Status</strong> Form
            </div>
            <div class="card-body card-block">
                <form action="{{ url('admin/interval/update/'.$interval->id) }}" method="post" enctype="multipart/form-data" class="">
                    @csrf
                    <div class="form-group">
                        <label for="nf-name" class=" form-control-label">Name</label>
                        <input type="text" id="nf-name" name="name" value="@if(old('name')) {{ old('name') }} @else {{ $interval->name }} @endif"
                               placeholder="Example: Quarterly" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-months" class=" form-control-label">Months</label>
                        <input type="text" id="nf-months" name="months" step="0.01" value="@if(old('months')) {{ old('months') }} @else {{ $interval->months }} @endif"
                               placeholder="Example: 3" class="form-control">
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                        <a href="{{ url('admin/interval/all') }}" type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Reset
                        </a>
                    </div>
                </form>

            </div>

        </div>
    </div>
@endsection
@section('assets')

    <script src="{{ asset('vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendors/jquery-validation-unobtrusive/dist/jquery.validate.unobtrusive.min.js') }}"></script>
@endsection
