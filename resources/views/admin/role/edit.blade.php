@extends('layouts.admin')

@section('content')

    <div class="col-lg-9">
        <div class="card">
            <div class="card-header">
                <strong>Role</strong> Form
            </div>
            <div class="card-body card-block">
                <form action="{{ url('admin/role/update/'.$role->id) }}" method="post" enctype="multipart/form-data" class="">
                    @csrf
                    <div class="form-group">
                        <label for="nf-name" class=" form-control-label">Name</label>
                        <input type="text" id="nf-name" name="name" value="@if(old('name')) {{ old('name') }} @else {{ $role->name }} @endif"
                               placeholder="Example: Agent" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-description" class=" form-control-label">Role Description</label>
                        <textarea name="description" id="nf-description" cols="30" rows="5" placeholder=""
                                  class="form-control">@if(old('description')) {{ old('description') }} @else {{ $role->description }} @endif</textarea>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                        <a href="{{ url('admin/role/all') }}" type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Reset
                        </a>
                    </div>
                </form>

            </div>

        </div>
    </div>
@endsection
@section('assets')

    <script src="{{ asset('vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendors/jquery-validation-unobtrusive/dist/jquery.validate.unobtrusive.min.js') }}"></script>
@endsection
