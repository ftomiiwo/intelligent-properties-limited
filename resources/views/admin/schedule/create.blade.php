@extends('layouts.admin')

@section('content')

    <div class="col-lg-9">
        <div class="card">
            <div class="card-header">
                <strong>Payment Schedule</strong> Form
            </div>
            <div class="card-body card-block">
                <form action="{{ url('admin/schedule/create') }}" method="post" enctype="multipart/form-data" class="">
                    @csrf
                    <div class="form-group">
                        <label for="nf-subscription" class="form-control-label">User Subscription</label>
                        <select name="subscription_id" id="nf-subscription" class="form-control">
                            <option value selected disabled>Please select User Subscription </option>
                            @foreach($subscriptions as $subscription)
                                <option value="{{ $subscription->id }}" @if(old('subscription_id') == $subscription->id) selected @endif
                                >{{ $subscription->user->name }} - {{ $subscription->plot->project->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nf-installment_amount" class="form-control-label">Installment Amount</label>
                        <input type="text" id="nf-installment_amount" name="installment_amount"
                               value="@if(old('installment_amount')){{ old('installment_amount') }}@endif"
                               placeholder="Example: 1000000" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-status" class="form-control-label">Subscription Status</label>
                        <select name="status_id" id="nf-status" class="form-control">
                            <option value selected disabled>Please select status</option>
                            @foreach($statuses as $status)
                                <option value="{{ $status->id }}" @if(old('status_id') == $status->id) selected @endif
                                >{{ $status->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nf-schedule_date" class="form-control-label">Schedule Date</label>
                        <input type="date" id="nf-schedule_date" name="schedule_date" value="{{ old('schedule_date') }}"
                               class="form-control">
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                        <a href="{{ url('admin/schedule/all') }}" type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Back
                        </a>
                    </div>
                </form>

            </div>

        </div>
    </div>
@endsection
@section('assets')

    <script src="{{ asset('vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendors/jquery-validation-unobtrusive/dist/jquery.validate.unobtrusive.min.js') }}"></script>
@endsection
