@extends('layouts.admin')

@include('layouts.table')
@section('content')


    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Payment Schedules Table</strong>
                        </div>
                        <div class="card-body">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{ url('admin/schedule/add')  }}"class="btn btn-outline-danger btn-lg"
                                       type="button" ><i class="fa fa-plus"></i> Create Schedule</a>
                                </div>
                            </div>
                            <!-- /# card -->
                            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <td>ID</td>
                                    <th>Customer Name</th>
                                    <th>Project</th>
                                    <th>Amount Payable</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($schedules as $schedule)
                                    <tr>
                                        <td>{{ $schedule->id }}</td>
                                        <td>{{ $schedule->subscription->user->name }}</td>
                                        <td>{{ $schedule->subscription->plot->project->name }}</td>
                                        <td>₦{{ number_format($schedule->installment_amount) }}</td>
                                        <td>{{ $schedule->schedule_date }}</td>
                                        <td>{{ $schedule->status->name }}</td>
                                        <td>
                                            <div class="dropdown for-notification">
                                                <button class="btn btn-outline-danger dropdown-toggle" type="button" id="notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-gear"></i>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="notification">
{{--                                                    <a class="dropdown-item media" href="{{ url('admin/schedule/show/'.$schedule->id)  }}">--}}
{{--                                                        <i class="fa fa-eye"></i>--}}
{{--                                                        <p>View</p>--}}
{{--                                                    </a>--}}
                                                    <a class="dropdown-item media" href="{{ url('admin/schedule/edit/'.$schedule->id)  }}">
                                                        <i class="fa fa-wrench"></i>
                                                        <p>View/Edit</p>
                                                    </a>
                                                    @if(!$schedule->invoice_code)
                                                    <a class="dropdown-item media" href="{{ url('admin/schedule/send_invoice/'.$schedule->id)  }}">
                                                        <i class="fa fa-mail-forward"></i>
                                                        <p>Send Invoice</p>
                                                    </a>
                                                    <a class="dropdown-item media" href="{{ url('admin/schedule/generate/payment/'.$schedule->id)  }}"
                                                       onclick='confirm("Are you sure you want generate the Payment Record for the schedule with ID #{{ $schedule->id }}?, it will be assumed that the customer has made payment.")'>
                                                        <i class="fa fa-money"></i>
                                                        <p>Generate Payment Record</p>
                                                    </a>
                                                    @endif
                                                    @if($schedule->invoice_code && $schedule->status_id != 6)
                                                    <a class="dropdown-item media" href="{{ url('admin/schedule/verify_invoice/'.$schedule->invoice_code) }}">
                                                        <i class="fa fa-certificate"></i>
                                                        <p>Verify Invoice</p>
                                                    </a>
                                                    <a class="dropdown-item media" href="{{ url('admin/schedule/send_invoice/'.$schedule->id)  }}">
                                                        <i class="fa fa-mail-forward"></i>
                                                        <p>Re-send Invoice</p>
                                                    </a>
                                                    @endif
                                                    <a class="dropdown-item media" onclick='confirm("Are you sure you want to delete this Payment Schedule record #{{ $schedule->id }}?")' href="{{ url('admin/schedule/delete/'.$schedule->id)  }}">
                                                        <i class="fa fa-times"></i>
                                                        <p>Delete</p>
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection
{{--@section('assets')--}}
{{--    <script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/jszip/dist/jszip.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.colVis.min.js') }}"></script>--}}
{{--    <script src="{{ asset('assets/js/init-scripts/data-table/datatables-init.js') }}"></script>--}}
{{--@endsection--}}
