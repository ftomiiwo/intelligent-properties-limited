@extends('layouts.admin')

@section('content')

    <div class="col-lg-12">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <strong>Configuration</strong> Form
                </div>
                <div class="card-body card-block">
                    <form action="{{ url('admin/config/onboard') }}" method="post" enctype="multipart/form-data" class="">
                        @csrf
                        <div class="form-group">
                            <label for="nf-site_name" class=" form-control-label">Company Name</label>
                            <input type="text" id="nf-site_name" name="site_name" value="{{ old('site_name', \App\Helper::config('site_name')) }}"
                                   placeholder="Example: The Real Estate Company" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="nf-site_email" class=" form-control-label">Company Email</label>
                            <input type="text" id="nf-site_email" name="site_email" value="{{ old('site_email', \App\Helper::config('site_email')) }}"
                                   placeholder="Example: info@therealestatecompany.com" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="nf-site_url" class=" form-control-label">App URL</label>
                            <input type="text" id="nf-site_url" name="site_url" class="form-control"
                                   value="{{ old('site_url', \App\Helper::config('site_url')) }}"
                                   placeholder="Example: https://app.the-real-estate-company.com/" required>
                        </div>
                        <div class="form-group">
                            <label for="nf-site_url" class=" form-control-label">Main Website URL</label>
                            <input type="text" id="nf-main_website_url" name="main_website_url" class="form-control"
                                   value="{{ old('main_website_url', \App\Helper::config('main_website_url')) }}"
                                   placeholder="Example: https://www.the-real-estate-company.com/" required>
                        </div>
                        <div class="form-group">
                            <label for="nf-description" class=" form-control-label">Short Description</label>
                            <textarea id="nf-description" name="site_description" class="form-control"
                            >{{ old('site_description', \App\Helper::config('site_description')) }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="nf-paystack_secret_key" class=" form-control-label">Paystack Secret Key</label>
                            <input type="text" id="nf-paystack_secret_key" name="paystack_secret_key"
                                   value="{{ old('paystack_secret_key', \App\Helper::config('paystack_secret_key')) }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="nf-paystack_public_key" class=" form-control-label">Paystack Public Key</label>
                            <input type="text" id="nf-paystack_public_key" name="paystack_public_key"
                                   value="{{ old('paystack_public_key', \App\Helper::config('paystack_public_key')) }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="nf-sendgrid_api_key" class=" form-control-label">Sendgrid API Key</label>
                            <input type="text" id="nf-sendgrid_api_key" name="sendgrid_api_key"
                                   value="{{ old('sendgrid_api_key', \App\Helper::config('sendgrid_api_key')) }}" class="form-control">
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Submit
                            </button>
                            <a href="{{ url('admin/config/all') }}" type="reset" class="btn btn-danger btn-sm">
                                <i class="fa fa-ban"></i> Reset
                            </a>
                        </div>
                    </form>

                </div>

            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header">
                    <strong>Organization</strong> Logo
                </div>
                <div class="card-body card-block">
                    <form action="{{ url('admin/config/site_logo') }}" method="post" enctype="multipart/form-data" class="">
                        @csrf
                        <div class="form-group">
                            <label for="nf-site_logo" class=" form-control-label">Company Logo</label>
                            <input type="file" id="nf-site_logo" name="site_logo" value="{{ old('site_logo', \App\Helper::config('site_logo')) }}"
                                   pclass="form-control">
                        </div>

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Submit
                            </button>
                            <a href="{{ url('admin/config/all') }}" type="reset" class="btn btn-danger btn-sm">
                                <i class="fa fa-ban"></i> Reset
                            </a>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
@endsection
@section('assets')

    <script src="{{ asset('vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendors/jquery-validation-unobtrusive/dist/jquery.validate.unobtrusive.min.js') }}"></script>
@endsection
