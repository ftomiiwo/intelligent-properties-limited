@extends('layouts.admin')

@section('content')
    @if(auth()->user()->isSuperAdmin())
        <div class="col-sm-6 col-lg-3">
            <a href="{{ url('/admin/project/all') }}">
                <div class="card text-white bg-flat-color-1">
                    <div class="card-body pb-0">
                        <h4 class="mb-0">
                            <span class="count">{{ $projects }}</span>
                        </h4>
                        <p class="text-light">Projects</p>

                        <div class="chart-wrapper px-0" style="height:70px;" height="70">
                            <canvas id="widgetChart1"></canvas>
                        </div>

                    </div>
                </div>
            </a>
        </div>
    @endif
    <!--/.col-->
    <div class="col-sm-6 @if(auth()->user()->isSuperAdmin()) col-lg-3 @else col-lg-6 @endif">
        <a href="{{ url('/admin/user/all') }}">
            <div class="card text-white bg-flat-color-2">
                <div class="card-body pb-0">
                    <h4 class="mb-0">
                        <span class="count">{{ $customers }}</span>
                    </h4>
                    <p class="text-light">Customers</p>

                    <div class="chart-wrapper px-0" style="height:70px;" height="70">
                        <canvas id="widgetChart2"></canvas>
                    </div>

                </div>
            </div>
        </a>
    </div>
    <!--/.col-->
    @if(auth()->user()->isSuperAdmin())
{{--    <div class="col-sm-6 col-lg-3">--}}
{{--        <a href="{{ url('admin/transaction/all') }}">--}}
{{--            <div class="card text-white bg-flat-color-3">--}}
{{--                <div class="card-body pb-0">--}}
{{--                    <h4 class="mb-0">--}}
{{--                        <span class="count">{{ $transactions }}</span>--}}
{{--                    </h4>--}}
{{--                    <p class="text-light">Transactions</p>--}}

{{--                </div>--}}

{{--                <div class="chart-wrapper px-0" style="height:70px;" height="70">--}}
{{--                    <canvas id="widgetChart3"></canvas>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </a>--}}
{{--    </div>--}}
    @endif
    <!--/.col-->

    <div class="col-sm-6 col-lg-6">
        <a href="{{ url('admin/payment/all') }}">
            <div class="card text-white bg-flat-color-4">
                <div class="card-body pb-0">
                    <h4 class="mb-0">
                        ₦<span>{{ number_format($payments) }}</span>
                    </h4>
                    <p class="text-light">Payments</p>

                    <div class="chart-wrapper px-3" style="height:70px;" height="70">
                        <canvas id="widgetChart4"></canvas>
                    </div>

                </div>
            </div>
        </a>
    </div>
    <!--/.col-->


    @if(auth()->user()->isSuperAdmin())
    <div class="col-xl-3 col-lg-6">
        <a href="{{ url('admin/plan/all') }}">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-one">
                        <div class="stat-icon dib"><i class="ti-money text-success border-success"></i></div>
                        <div class="stat-content dib">
                            <div class="stat-text">Payment Plans</div>
                            <div class="stat-digit">{{ $plans }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    @endif

    <div class="col-xl-3 col-lg-6">
        <a href="{{ url('admin/subscription/all') }}">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-one">
                        <div class="stat-icon dib"><i class="ti-pie-chart text-warning border-warning"></i></div>
                        <div class="stat-content dib">
                            <div class="stat-text">Subscriptions</div>
                            <div class="stat-digit">{{ $subscriptions }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-xl-3 col-lg-6">
        <a href="{{ url('admin/schedule/all') }}">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-one">
                        <div class="stat-icon dib"><i class="ti-list-ol text-warning border-warning"></i></div>
                        <div class="stat-content dib">
                            <div class="stat-text"> Schedules</div>
                            <div class="stat-digit">{{ $schedules }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    @if(auth()->user()->isSuperAdmin())
    <div class="col-xl-3 col-lg-6">
        <a href="{{ url('admin/user/admins') }}">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-one">
                        <div class="stat-icon dib"><i class="ti-user text-success border-success"></i></div>
                        <div class="stat-content dib">
                            <div class="stat-text">Admins</div>
                            <div class="stat-digit">{{ $admins }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    @endif


 <!-- .content -->

@endsection
