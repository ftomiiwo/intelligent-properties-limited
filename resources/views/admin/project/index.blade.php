@extends('layouts.admin')

@include('layouts.table')
@section('content')


    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Projects Table</strong>
                        </div>
                        <div class="card-body">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{ url('admin/project/add')  }}"class="btn btn-outline-danger btn-lg"
                                       type="button" ><i class="fa fa-plus"></i> Create Project</a>
                                </div>
                            </div>
                            <!-- /# card -->
                            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                <thead>
                                <tr>

                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Location</th>
                                    <th>Country</th>
                                    <th>Image</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($projects as $project)
                                    <tr>
                                        <td>{{ $project->id }}</td>
                                        <td>{{ $project->name }}</td>
                                        <td>{{ $project->projecttype->name}}</td>
                                        <td>{{ $project->location }}</td>
                                        <td>{{ $project->country }}</td>
                                        <td>
                                            <img
                                                src="{{ asset('storage/images/projects/'.$project->image) }}"
                                                class="align-bottom w-8 h-8 rounded"
                                                style="object-fit: cover;"></td>
                                        <td>{{ $project->status->name }}</td>
                                        <td>
                                            <div class="dropdown for-notification">
                                                <button class="btn btn-outline-danger dropdown-toggle" type="button" id="notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-gear"></i>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="notification">
{{--                                                    <a class="dropdown-item media" href="{{ url('admin/project/show/'.$project->id)  }}">--}}
{{--                                                        <i class="fa fa-eye"></i>--}}
{{--                                                        <p>View</p>--}}
{{--                                                    </a>--}}
                                                    <a class="dropdown-item media" href="{{ url('admin/project/edit/'.$project->id)  }}">
                                                        <i class="fa fa-wrench"></i>
                                                        <p>View/Edit</p>
                                                    </a>
                                                    <a class="dropdown-item media" href="{{ url('admin/project/delete/'.$project->id)  }}">
                                                        <i class="fa fa-times"></i>
                                                        <p>Delete</p>
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection
{{--@section('assets')--}}
{{--    <script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/jszip/dist/jszip.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.colVis.min.js') }}"></script>--}}
{{--    <script src="{{ asset('assets/js/init-scripts/data-table/datatables-init.js') }}"></script>--}}
{{--@endsection--}}
