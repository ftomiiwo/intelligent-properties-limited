@extends('layouts.admin')

@section('content')

    <div class="col-lg-9">
        <div class="card">
            <div class="card-header">
                <strong>Project Location</strong> Form
            </div>
            <div class="card-body card-block">
                <form action="{{ url('admin/project/update', $project->id) }}" method="post" enctype="multipart/form-data" class="">
                    @csrf
                    <div class="form-group">
                        <label for="nf-name" class=" form-control-label">Project Name</label>
                        <input type="text" id="nf-name" name="name"
                               value="@if(old('name')){{old(' name')}}@elseif($project){{ $project->name }}@endif"
                               placeholder="Highrise Estate.." class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-type" class="form-control-label">Project Type</label>
                        <select name="project_type_id" id="nf-type" class="form-control">
                            <option value selected disabled>Please select type</option>
                            @foreach($project_types as $type)
                                <option value="{{ $type->id }}" @if(old('project_type_id') == $type->id) selected @elseif($project->project_type_id == $type->id) selected  @endif
                                >{{ $type->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="nf-status" class="form-control-label">Project Status</label>
                        <select name="status_id" id="nf-status" class="form-control">
                            <option value selected disabled>Please select status</option>
                            @foreach($statuses as $status)
                                <option value="{{ $status->id }}" @if(old('status_id') == $status->id) selected @elseif($project->status_id == $status->id) selected  @endif
                                >{{ $status->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="nf-description" class=" form-control-label">Project Description</label>
                        <textarea name="description" id="nf-description" cols="30" rows="5" placeholder=""
                                  class="form-control">{{ old('description', $project->description) }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="nf-location" class=" form-control-label">Location</label>
                        <input type="text" id="nf-location" name="location" value="{{ old('location', $project->location) }}"
                               placeholder="Ibeju-Lekki" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="nf-country" class=" form-control-label">Country</label>
                        <input type="text" id="nf-country" name="country" value="{{ old('country', $project->country) }}"
                               placeholder="Nigeria" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-map_link" class=" form-control-label">Map Link (optional)</label>
                        <textarea name="map_link" id="nf-map_link" cols="10" rows="2" placeholder=""
                                  class="form-control">{{ old('map_link', $project->map_link) }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="nf-image" class=" form-control-label">Image</label>
                        <img
                            src="{{ asset('storage/images/projects/'.$project->image) }}"
                            class="align-bottom w-8 h-8 rounded"
                            style="object-fit: cover;">
                        <input type="file" id="nf-image" name="image" class="form-control" value="{{ $project->image }}">
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                        <a href="{{ url('admin/project/all') }}" type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Reset
                        </a>
                    </div>
                </form>

            </div>

        </div>
    </div>
@endsection

@section('assets')
    <script src="{{ asset('vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendors/jquery-validation-unobtrusive/dist/jquery.validate.unobtrusive.min.js') }}"></script>
@endsection
