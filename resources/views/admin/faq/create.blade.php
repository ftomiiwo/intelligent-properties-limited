@extends('layouts.admin')

@section('content')

    <div class="col-lg-9">
        <div class="card">
            <div class="card-header">
                <strong>FAQ</strong> Form
            </div>
            <div class="card-body card-block">
                <form action="{{ url('admin/faq/create') }}" method="post" enctype="multipart/form-data" class="">
                    @csrf
                    <div class="form-group">
                        <label for="nf-name" class=" form-control-label">Question</label>
                        <input type="text" id="nf-name" name="name" value="{{ old('name') }}"
                               placeholder="Question" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-description" class="form-control-label">Answer</label>
                        <textarea name="description" id="nf-description" cols="30" rows="5" placeholder=""
                                  class="form-control">@if(old('description')) {{ old('description') }} @endif</textarea>
                    </div>
                    <div class="form-group">
                        <label for="nf-status_id" class=" form-control-label">Status</label>
                        <select name="status_id" id="nf-status_id" class="form-control">
                            <option value selected disabled> Select a Status</option>
                            @foreach($statuses as $status)
                            <option value="{{ $status->id }}">{{ $status->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                        <a href="{{ url('admin/faq/all') }}" type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Back
                        </a>
                    </div>
                </form>

            </div>

        </div>
    </div>
@endsection
@section('assets')

    <script src="{{ asset('vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendors/jquery-validation-unobtrusive/dist/jquery.validate.unobtrusive.min.js') }}"></script>
@endsection
