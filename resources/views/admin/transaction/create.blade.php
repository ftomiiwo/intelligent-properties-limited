@extends('layouts.admin')

@section('content')

    <div class="col-lg-9">
        <div class="card">
            <div class="card-header">
                <strong>Transaction</strong> Form
            </div>
            <div class="card-body card-block">
                <form action="{{ url('admin/transaction/create') }}" method="post" enctype="multipart/form-data" class="">
                    @csrf
                    <div class="form-group">
                        <label for="nf-reference" class=" form-control-label">Payment Reference</label>
                        <input type="text" id="nf-reference" name="reference" value="{{ old('reference') }}"
                               placeholder="ipl-XXXXX" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nf-status" class="form-control-label">Project Status</label>
                        <select name="status_id" id="nf-status" class="form-control">
                            <option value selected disabled>Please select status</option>
                            @foreach($statuses as $status)
                                <option value="{{ $status->id }}" @if(old('status_id') == $status->id) selected @endif
                                >{{ $status->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                        <a href="{{ url('admin/transaction/all') }}" type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Back
                        </a>
                    </div>
                </form>

            </div>

        </div>
    </div>
@endsection
@section('assets')

    <script src="{{ asset('vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendors/jquery-validation-unobtrusive/dist/jquery.validate.unobtrusive.min.js') }}"></script>
@endsection
