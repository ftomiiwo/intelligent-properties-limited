@extends('layouts.app', ['title' => __('User Profile')])

@section('content')
    @include('users.partials.header', [
        'title' => __('Hello') . ' '. auth()->user()->name,
        'description' => __('This is your profile page. You can see the progress you\'ve made with your real estate investments.'),
        'class' => 'col-lg-7'
    ])
    <style type="text/css">

        #signArea{
            width:304px;
            margin: 50px auto;
        }
        .sign-container {
            width: 60%;
            margin: auto;
        }
        .sign-preview {
            width: 150px;
            height: 50px;
            border: solid 1px #CFCFCF;
            margin: 10px 5px;
        }
        .tag-ingo {
            font-family: cursive;
            font-size: 12px;
            text-align: left;
            font-style: oblique;
        }
    </style>
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
                <div class="card card-profile shadow">
                    <div class="card-header bg-white border-0" style="margin-bottom: 10px;">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Passport Photograph') }}</h3>
                        </div>
                    </div>
                    <div class="card-header text-center border-0 pt-md-4 pb-0 pb-md-4 col-md-12" >
                        <div>
                            <img style="max-width: 60%;max-height: 60%;" src="{{ asset('storage/images/avatars/'.auth()->user()->avatar) }}">
                        </div>

                        <div class="d-flex justify-content-between">
                            <a href="#" class="btn btn-sm btn-default float-right" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-pencil"></i>Change</a>
                        </div>
                    </div>
                    <div class="text-center">
                        <h3>
                            {{ auth()->user()->name }}<span class="font-weight-light">, {{ \Carbon\Carbon::parse(auth()->user()->date_of_birth)->age }}</span>
                        </h3>
                        <div class="h5 font-weight-300">
                            <i class="ni location_pin mr-2"></i>{{ auth()->user()->state_of_origin }}, {{ auth()->user()->nationality }}
                        </div>
                        <div class="h5 mt-4">
                            <i class="ni business_briefcase-24 mr-2"></i>{{ auth()->user()->occupation }}
                        </div>
                    </div>
{{--                    <div class="card-body pt-0 pt-md-4">--}}
{{--                        --}}
{{--                    </div>--}}
                </div>
                <br><br>
                <div class="card card-profile shadow">
                    <div class="card-header bg-white border-0" style="margin-bottom: 10px;">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Manage Digital Signature') }}</h3>
                        </div>
                    </div>
                    <div class="card-header text-center border-0 pt-md-4 pb-0 pb-md-4 col-md-12" >
                        <div>
                            @if(!is_null(auth()->user()->digital_signature))
                            <img style="max-width: 60%;max-height: 60%;"
                                 src="{{ asset('storage/documents/digital_signatures/'.auth()->user()->digital_signature) }}">
                            @else
                                No signature found.
                            @endif
                        </div>
                        <div class="d-flex justify-content-between">
                            @if(is_null(auth()->user()->digital_signature))
                            <a href="#" class="btn btn-sm btn-default float-right" data-toggle="modal" data-target="#dsigdraw"><i class="fa fa-pencil"></i>Create Signature</a>
                            @else
                            <a href="#" class="btn btn-sm btn-default float-right" data-toggle="modal" data-target="#dsigdraw"><i class="fa fa-pencil"></i>Edit Signature</a>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-8 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Edit Profile') }}</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('profile.update') }}" autocomplete="off">
                            @csrf
                            @method('put')

                            <h6 class="heading-small text-muted mb-4">{{ __('User information') }}</h6>

                            @if (session('status'))
                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Name') }}</label>
                                    <input type="text" name="name" id="input-name" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Name') }}" value="{{ old('name', auth()->user()->name) }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-email">{{ __('Email') }}</label>
                                    <input type="email" name="email" id="input-email" class="form-control form-control-alternative{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Email') }}" value="{{ old('email', auth()->user()->email) }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('phone') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-phone">{{ __('Phone Number') }}</label>
                                    <input type="phone" name="phone" id="input-phone" class="form-control form-control-alternative{{ $errors->has('phone') ? ' is-invalid' : '' }}" placeholder="{{ __('Phone Number') }}" value="{{ old('phone', auth()->user()->phone) }}" required>

                                    @if ($errors->has('phone'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('date_of_birth') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-date_of_birth">{{ __('Date of Birth') }}</label>
                                    <input type="date" name="date_of_birth" id="input-date_of_birth"
                                           class="form-control form-control-alternative{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}"
                                           value="{{ old('date_of_birth', auth()->user()->date_of_birth) }}" required>

                                    @if ($errors->has('date_of_birth'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('date_of_birth') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('gender') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-gender">{{ __('Gender') }}</label>
                                    <select name="gender" id="input-gender" class="form-control">
                                        <option value selected disabled>Please select gender</option>
                                        <option value="Female" @if(old('gender') == 'Female') selected @elseif(auth()->user()->gender == 'Female') selected @endif>Female</option>
                                        <option value="Male" @if(old('gender') == 'Male') selected @elseif(auth()->user()->gender == 'Male') selected @endif>Male</option>
                                    </select>
                                    @if ($errors->has('gender'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('gender') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('occupation') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-occupation">{{ __('Occupation') }}</label>
                                    <input type="text" name="occupation" id="input-occupation"
                                           class="form-control form-control-alternative{{ $errors->has('occupation') ? ' is-invalid' : '' }}"
                                           placeholder="{{ __('Example: Business Man') }}" value="{{ old('occupation', auth()->user()->occupation) }}" required>

                                    @if ($errors->has('occupation'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('occupation') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-outline-danger mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                        <hr class="my-4" />
                        <form method="post" action="{{ url('profile/contact') }}" autocomplete="off">
                            @csrf
                            <h6 class="heading-small text-muted mb-4">{{ __('Contact') }}</h6>
                            <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('address') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-address">{{ __('Residential Address') }}</label>
                                    <textarea class="form-control" id="input-address" name="address" required>{{ old('address', auth()->user()->address) }}</textarea>

                                    @if ($errors->has('address'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('state_of_origin') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="state_of_origin">{{ __('State Of Origin') }}</label>
                                    <input type="text" name="state_of_origin" id="state_of_origin"
                                           class="form-control form-control-alternative{{ $errors->has('state_of_origin') ? ' is-invalid' : '' }}"
                                           placeholder="{{ __('Example: Lagos State') }}" value="{{ old('state_of_origin', auth()->user()->state_of_origin) }}" required>

                                    @if ($errors->has('state_of_origin'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('state_of_origin') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('nationality') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="nationality">{{ __('Country') }}</label>
                                    <input type="text" name="nationality" id="nationality"
                                           class="form-control form-control-alternative{{ $errors->has('nationality') ? ' is-invalid' : '' }}"
                                           placeholder="{{ __('Example: Nigeria') }}" value="{{ old('nationality', auth()->user()->nationality) }}" required>

                                    @if ($errors->has('nationality'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('nationality') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-outline-danger mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                        <hr class="my-4" />
                        <form method="post" action="{{ url('profile/next_of_kin') }}" autocomplete="off">
                            @csrf
                            <h6 class="heading-small text-muted mb-4">{{ __('Next of Kin') }}</h6>
                            <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('next_of_kin_name') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="next_of_kin_name">{{ __('Next of Kin\'s Full Name') }}</label>
                                    <input type="text" name="next_of_kin_name" id="next_of_kin_name" class="form-control form-control-alternative{{ $errors->has('next_of_kin_name') ? ' is-invalid' : '' }}" placeholder="{{ __('Next of Kin\'s Name') }}" value="{{ old('next_of_kin_name', auth()->user()->next_of_kin_name) }}" required autofocus>

                                    @if ($errors->has('next_of_kin_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('next_of_kin_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('next_of_kin_address') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="next_of_kin_address">{{ __('Next of Kin\'s Residential Address') }}</label>
                                    <input type="text" name="next_of_kin_address" id="next_of_kin_address" class="form-control form-control-alternative{{ $errors->has('next_of_kin_address') ? ' is-invalid' : '' }}" placeholder="{{ __('Example: 3 Ikeja GRA, Lagos, Nigeria') }}" value="{{ old('next_of_kin_address', auth()->user()->next_of_kin_address) }}" required>

                                    @if ($errors->has('next_of_kin_address'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('next_of_kin_address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('next_of_kin_phone') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="next_of_kin_phone">{{ __('Next of Kin\'s Phone Number') }}</label>
                                    <input type="phone" name="next_of_kin_phone" id="next_of_kin_phone" class="form-control form-control-alternative{{ $errors->has('next_of_kin_phone') ? ' is-invalid' : '' }}" placeholder="{{ __('Example: 081XXXXXXXX') }}" value="{{ old('next_of_kin_phone', auth()->user()->next_of_kin_phone) }}" required>

                                    @if ($errors->has('next_of_kin_phone'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('next_of_kin_phone') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-outline-danger mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--  Profile Picture Modal -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Profile Picture</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="{{ url('profile/avatar') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">

                            <div class="form-group{{ $errors->has('avatar') ? ' has-danger' : '' }}">
                                <input type="file" name="avatar" id="avatar" class="form-control form-control-alternative{{ $errors->has('avatar') ? ' is-invalid' : '' }}" value="" required>

                                @if ($errors->has('avatar'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('avatar') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-outline-success">Upload</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--  Profile Picture Modal -->
        <!--  Digital Signature Upload Modal -->
        <div class="modal fade" id="dsigupload" tabindex="-1" role="dialog" aria-labelledby="dsiguploadTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Upload Digital Signature</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="{{ url('profile/avatar') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">

                            <div class="form-group{{ $errors->has('avatar') ? ' has-danger' : '' }}">
                                <input type="file" name="avatar" id="avatar" class="form-control form-control-alternative{{ $errors->has('avatar') ? ' is-invalid' : '' }}" value="" required>

                                @if ($errors->has('avatar'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('avatar') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-success" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-outline-danger">Upload</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--  Digital Signature Modal -->
        <!--  Profile Picture Modal -->
        <div class="modal fade" id="dsigdraw" tabindex="-1" role="dialog" aria-labelledby="dsigdrawTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Create Digital Signature</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div id="signArea" >
                        <h2 class="tag-ingo">Sign below,</h2>
                        <div class="sig sigWrapper" style="height:auto;">
                            <div class="typed"></div>
                            <canvas class="sign-pad" id="sign-pad" width="300" height="100"></canvas>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btnSaveSign" class="btn btn-outline-success">Save Signature</button>
                        <button id="btnClearSign" class="btn btn-outline-default">Clear</button>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
    @section('signassets')
        <script src="{{ asset('docu_sign/js/numeric-1.2.6.min.js') }}"></script>
        <script src="{{ asset('docu_sign/js/bezier.js') }}"></script>
        <script src="{{ asset('docu_sign/js/jquery.signaturepad.js') }}"></script>
        <script type='text/javascript' src="https://github.com/niklasvh/html2canvas/releases/download/0.4.1/html2canvas.js"></script>
        <script src="{{ asset('docu_sign/js/json2.min.js') }}"></script>
        <script>
            $(document).ready(function() {
                $('#signArea').signaturePad({drawOnly:true, drawBezierCurves:true, lineTop:90});
            });
            $("#btnClearSign").click(function(e){
                $('#signArea').signaturePad().clearCanvas ();
            });

            $("#btnSaveSign").click(function(e){
                html2canvas([document.getElementById('sign-pad')], {
                    onrendered: function (canvas) {
                        var canvas_img_data = canvas.toDataURL('image/png');
                        var img_data = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
                        $.ajax({
                            url: '{{ url('digital-signature') }}',
                            data: {
                                img_data:img_data,
                                _token: "{{ csrf_token() }}",
                            },
                            type: 'post',
                            dataType: 'json',
                            success: function (response) {
                                if(response.success == true){
                                    setTimeout(function(){
                                        window.location.reload();
                                    }, 2000);
                                }
                            }
                        });
                    }
                });
            });
        </script>
    @endsection
@endsection
