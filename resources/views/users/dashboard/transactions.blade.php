@extends('layouts.app')

@section('content')
    @include('layouts.headers.cards')
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Transaction History</h3>
                            </div>
{{--                            <div class="col-4 text-right">--}}
{{--                                <a href="{{ url('dashboard/transaction/add') }}" class="btn btn-sm btn-primary">Add Transa</a>--}}
{{--                            </div>--}}
                        </div>
                    </div>

                    <div class="col-12"></div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Payment Reference</th>
                                <th scope="col">Amount (in Naira)</th>
                                <th scope="col">Gateway</th>
                                <th scope="col">Status</th>
                                <th scope="col">Date</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($payments as $payment)
                                <tr>
                                    <td></td>
                                    <td>{{ $payment->reference }}</td>
                                    <td>
                                        <a href="#">₦{{ number_format($payment->amount) }}</a>
                                    </td>
                                    <td>{{ $payment->gateway->name }}</td>
                                    <td>{{ $payment->status->name }}</td>
                                    <td>{{ $payment->updated_at }}</td>
                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            @if($payment->confirmation_file)
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a class="dropdown-item" target="_blank" href="{{ asset('storage/documents/confirmations/'.$payment->confirmation_file) }}">Download Payment Confirmation</a>
                                            </div>
                                            @endif
                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">

                        </nav>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>

@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush
