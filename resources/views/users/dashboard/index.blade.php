@extends('layouts.app')

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
{{--        <div class="row">--}}
{{--            <div class="col-xl-8 mb-5 mb-xl-0">--}}
{{--                <div class="card bg-gradient-default shadow">--}}
{{--                    <div class="card-header bg-transparent">--}}
{{--                        <div class="row align-items-center">--}}
{{--                            <div class="col">--}}
{{--                                <h6 class="text-uppercase text-light ls-1 mb-1">Overview</h6>--}}
{{--                                <h2 class="text-white mb-0">Sales value</h2>--}}
{{--                            </div>--}}
{{--                            <div class="col">--}}
{{--                                <ul class="nav nav-pills justify-content-end">--}}
{{--                                    <li class="nav-item mr-2 mr-md-0" data-toggle="chart" data-target="#chart-sales" data-update='{"data":{"datasets":[{"data":[0, 20, 10, 30, 15, 40, 20, 60, 60]}]}}' data-prefix="$" data-suffix="k">--}}
{{--                                        <a href="#" class="nav-link py-2 px-3 active" data-toggle="tab">--}}
{{--                                            <span class="d-none d-md-block">Month</span>--}}
{{--                                            <span class="d-md-none">M</span>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                    <li class="nav-item" data-toggle="chart" data-target="#chart-sales" data-update='{"data":{"datasets":[{"data":[0, 20, 5, 25, 10, 30, 15, 40, 40]}]}}' data-prefix="$" data-suffix="k">--}}
{{--                                        <a href="#" class="nav-link py-2 px-3" data-toggle="tab">--}}
{{--                                            <span class="d-none d-md-block">Week</span>--}}
{{--                                            <span class="d-md-none">W</span>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="card-body">--}}
{{--                        <!-- Chart -->--}}
{{--                        <div class="chart">--}}
{{--                            <!-- Chart wrapper -->--}}
{{--                            <canvas id="chart-sales" class="chart-canvas"></canvas>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-xl-4">--}}
{{--                <div class="card shadow">--}}
{{--                    <div class="card-header bg-transparent">--}}
{{--                        <div class="row align-items-center">--}}
{{--                            <div class="col">--}}
{{--                                <h6 class="text-uppercase text-muted ls-1 mb-1">Performance</h6>--}}
{{--                                <h2 class="mb-0">Total orders</h2>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="card-body">--}}
{{--                        <!-- Chart -->--}}
{{--                        <div class="chart">--}}
{{--                            <canvas id="chart-orders" class="chart-canvas"></canvas>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="row mt-5">
            <div class="col-xl-7 mb-5 mb-xl-0">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Payments Record</h3>
                            </div>
{{--                            <div class="col text-right">--}}
{{--                                <a href="#!" class="btn btn-sm btn-primary">See all</a>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Project Name</th>
                                    <th scope="col">Subscription</th>
                                    <th scope="col">Unit(s)</th>
                                    <th scope="col">Amount</th>
                                    <th scope="col">Date</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($payments as $payment)
                                <tr>
                                    <td>{{ $payment->subscription->plot->project->name }}</td>
                                    <td>{{ $payment->subscription->plan->name}}</td>
                                    <td>{{ $payment->subscription->units }}</td>
                                    <td>₦{{ number_format($payment->amount) }}</td>
                                    <td>{{ $payment->created_at }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-xl-5">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Properties</h3>
                            </div>
{{--                            <div class="col text-right">--}}
{{--                                <a href="#!" class="btn btn-sm btn-primary">See all</a>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Plan</th>
                                    <th scope="col">Percentage</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($userSubscriptionProgress as $us)
                                <tr>
                                    <th scope="row">
                                        {{ $us->plot->project->name }}
                                    </th>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <span class="mr-2">{{ $us->completion_percentage }}%</span>
                                            <div>
                                                <div class="progress">
                                                <div class="progress-bar bg-gradient-danger" role="progressbar" aria-valuenow="{{ $us->completion_percentage }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $us->completion_percentage }}%;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush
