<p align="center">
<img src="https://atfak.com/assests/Asset%201.png" width="400">
</p>

<p align="center">
 
</p>

## Automated Invoicing and Payment Management System.
 
This product is designed to effectively automate and manage the Invoicing, Payment, and Receipt needs of any Business. 

## How to Install
- composer install 
- php artisan vendor:publish
- php artisan migrate
- php artisan db:seed
- php artisan storage:link
- php artisan optimize

## About Company

Atfak Technologies Limited is a Nigerian-based Technology Company that creates disruptive, affordable, out-of-the-box, 
and easy to use technology solutions that are aimed at increasing the efficiency in the systems of their clients while 
cutting cost, so that they can focus on the quality of their client service deliveries.  

## Security Vulnerabilities

If you discover a security vulnerability within this product, please send an e-mail to Tomiiwo Fakinlede 
via [atfaktech@gmail.com](mailto:atfaktech@gmail.com). All security vulnerabilities will be promptly addressed.

## License

Software licensed under the [MIT license](https://opensource.org/licenses/MIT).
