<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
Route::get('/', 'HomeController@index');
Route::get('/user/verify/{token}', 'Admin\UsersController@verifyUser');
Route::get('resend-signup-mail/{id}', 'Admin\UsersController@resend_signup_mail')->name('resend-signup-mail');

Route::prefix('admin')
    ->middleware('auth', \App\Http\Middleware\CheckAdmin::class)
    ->group(function () {

    Route::get('/', 'Admin\IndexController@index');
    Route::get('onboarding', 'Admin\ConfigController@onboard');

    Route::prefix('status')->group(function() {
        Route::get('all', 'Admin\StatusController@index');
        Route::get('show/{status_id}', 'Admin\StatusController@show');
        Route::get('add', 'Admin\StatusController@create');
        Route::post('create', 'Admin\StatusController@store');
        Route::get('edit/{status_id}', 'Admin\StatusController@edit');
        Route::post('update/{status_id}', 'Admin\StatusController@update');
        Route::get('delete/{status_id}', 'Admin\StatusController@destroy');
    });

    Route::prefix('role')->group(function() {
        Route::get('all', 'Admin\RoleController@index');
        Route::get('show/{role_id}', 'Admin\RoleController@show');
        Route::get('add', 'Admin\RoleController@create');
        Route::post('create', 'Admin\RoleController@store');
        Route::get('edit/{role_id}', 'Admin\RoleController@edit');
        Route::post('update/{role_id}', 'Admin\RoleController@update');
        Route::get('delete/{role_id}', 'Admin\RoleController@destroy');
    });

    Route::prefix('faq')->group(function() {
        Route::get('all', 'Admin\FaqController@index');
        Route::get('show/{role_id}', 'Admin\FaqController@show');
        Route::get('add', 'Admin\FaqController@create');
        Route::post('create', 'Admin\FaqController@store');
        Route::get('edit/{role_id}', 'Admin\FaqController@edit');
        Route::post('update/{role_id}', 'Admin\FaqController@update');
        Route::get('delete/{role_id}', 'Admin\FaqController@destroy');
    });

    Route::prefix('config')->group(function() {
        Route::post('onboard', 'Admin\ConfigController@storeOnboard');
        Route::post('site_logo', 'Admin\ConfigController@logoMGT');
        Route::get('all', 'Admin\ConfigController@index');
        Route::get('show/{config_id}', 'Admin\ConfigController@show');
        Route::get('add', 'Admin\ConfigController@create');
        Route::post('create', 'Admin\ConfigController@store');
        Route::get('edit/{config_id}', 'Admin\ConfigController@edit');
        Route::post('update/{config_id}', 'Admin\ConfigController@update');
        Route::get('delete/{config_id}', 'Admin\ConfigController@destroy');
    });

    Route::prefix('interval')->group(function() {
        Route::get('all', 'Admin\IntervalController@index');
        Route::get('show/{status_id}', 'Admin\IntervalController@show');
        Route::get('add', 'Admin\IntervalController@create');
        Route::post('create', 'Admin\IntervalController@store');
        Route::get('edit/{status_id}', 'Admin\IntervalController@edit');
        Route::post('update/{status_id}', 'Admin\IntervalController@update');
        Route::get('delete/{status_id}', 'Admin\IntervalController@destroy');
    });

    Route::prefix('user')->group(function() {
        Route::get('all', 'Admin\UsersController@index');
        Route::get('admins', 'Admin\UsersController@userlist');
        Route::get('show/{user_id}', 'Admin\UsersController@show');
        Route::get('add', 'Admin\UsersController@create');
        Route::post('create', 'Admin\UsersController@store');
        Route::get('edit/{user_id}', 'Admin\UsersController@edit');
        Route::post('update', 'Admin\UsersController@update');
        Route::post('upline-update', 'Admin\UsersController@updateUpline');
        Route::get('delete/{user_id}', 'Admin\UsersController@delete');

        Route::get('export', 'Admin\UsersController@export')->name('export');
        Route::get('importExportView', 'Admin\UsersController@importExportView');
        Route::post('users-import', 'Admin\UsersController@import')->name('users-import');
    });

    Route::prefix('project')->group(function() {
        Route::prefix('type')->group(function() {
            Route::get('all', 'Admin\ProjectTypeController@indexType');
            Route::get('add', 'Admin\ProjectTypeController@createType');
            Route::post('create', 'Admin\ProjectTypeController@storeType');
            Route::get('edit/{project_id}', 'Admin\ProjectTypeController@editType');
            Route::post('update/{project_id}', 'Admin\ProjectTypeController@updateType');
            Route::get('delete/{user_id}', 'Admin\ProjectTypeController@destroyType');
        });

        Route::get('all', 'Admin\ProjectsController@index');
        Route::get('show/{project_id}', 'Admin\ProjectsController@show');
        Route::get('add', 'Admin\ProjectsController@create');
        Route::post('create', 'Admin\ProjectsController@store');
        Route::get('edit/{project_id}', 'Admin\ProjectsController@edit');
        Route::post('update/{project_id}', 'Admin\ProjectsController@update');
        Route::get('delete/{project_id}', 'Admin\ProjectsController@destroy');
    });

    Route::prefix('plot')->group(function() {
        Route::get('all', 'Admin\PlotController@index');
        Route::get('show/{project_id}', 'Admin\PlotController@show');
        Route::get('add', 'Admin\PlotController@create');
        Route::post('create', 'Admin\PlotController@store');
        Route::get('edit/{project_id}', 'Admin\PlotController@edit');
        Route::post('update/{project_id}', 'Admin\PlotController@update');
        Route::get('delete/{project_id}', 'Admin\PlotController@destroy');
    });

    Route::prefix('payment')->group(function() {
        Route::get('all', 'Admin\PaymentController@index');
        Route::get('show/{payment_id}', 'Admin\PaymentController@show');
        Route::get('add', 'Admin\PaymentController@create');
        Route::post('create', 'Admin\PaymentController@store');
        Route::get('edit/{payment_id}', 'Admin\PaymentController@edit');
        Route::post('update/{payment_id}', 'Admin\PaymentController@update');
        Route::get('delete/{payment_id}', 'Admin\PaymentController@destroy');
        Route::get('confirmation/{payment_id}', 'Admin\PaymentController@confirmation');

    });

    Route::prefix('transaction')->group(function() {
        Route::get('all', 'Admin\TransactionController@index');
        Route::get('show/{payment_id}', 'Admin\TransactionController@show');
        Route::get('add', 'Admin\TransactionController@create');
        Route::post('create', 'Admin\TransactionController@store');
        Route::get('edit/{payment_id}', 'Admin\TransactionController@edit');
        Route::post('update/{payment_id}', 'Admin\TransactionController@update');
        Route::get('delete/{payment_id}', 'Admin\TransactionController@destroy');
    });

    Route::prefix('plan')->group(function() {
        Route::get('all', 'Admin\PlanController@index');
        Route::get('show/{plan_id}', 'Admin\PlanController@show');
        Route::get('add', 'Admin\PlanController@create');
        Route::post('create', 'Admin\PlanController@store');
        Route::get('edit/{plan_id}', 'Admin\PlanController@edit');
        Route::post('update/{plan_id}', 'Admin\PlanController@update');
        Route::get('delete/{plan_id}', 'Admin\PlanController@destroy');
    });

    Route::prefix('subscription')->group(function() {
        Route::get('all', 'Admin\SubscriptionController@index');
        Route::get('show/{subscription_id}', 'Admin\SubscriptionController@show');
        Route::get('add', 'Admin\SubscriptionController@create');
        Route::post('create', 'Admin\SubscriptionController@store');
        Route::get('edit/{subscription_id}', 'Admin\SubscriptionController@edit');
        Route::post('update/{subscription_id}', 'Admin\SubscriptionController@update');
        Route::get('delete/{subscription_id}', 'Admin\SubscriptionController@destroy');
    });

    Route::prefix('schedule')->group(function() {
        Route::get('all', 'Admin\ScheduleController@index');
        Route::get('show/{schedule_id}', 'Admin\ScheduleController@show');
        Route::get('add', 'Admin\ScheduleController@create');
        Route::post('create', 'Admin\ScheduleController@store');
        Route::get('edit/{schedule_id}', 'Admin\ScheduleController@edit');
        Route::get('invoice/{schedule_id}', 'Admin\ScheduleController@sendInvoice');
        Route::post('update/{schedule_id}', 'Admin\ScheduleController@update');
        Route::get('delete/{schedule_id}', 'Admin\ScheduleController@destroy');
        Route::get('send_invoice/{schedule_id}', 'Admin\ScheduleController@initiateInvoice');
        Route::get('generate/payment/{schedule_id}', 'Admin\ScheduleController@createPaymentRecordForSchedule');
        Route::get('verify_invoice/{request_code}', 'Admin\ScheduleController@verifyPaystackInvoice');
    });


});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard')->middleware('verified');
Route::get('plot-detail/{plot_id}', 'SiteController@plotDetail')->name('plot-detail');
Route::get('plan-detail/{plan_id}', 'SiteController@planDetail')->name('plan-detail');
Route::get('project-plot/{project_id}', 'SiteController@projectPlots')->name('project-plot');
Route::get('project-amount/{project_id}', 'SiteController@projectAmount')->name('project-amount');
Route::post('digital-signature', 'SiteController@digitalSignature')->name('digital-signature');
Route::get('check-profile', 'SiteController@checkUserProfileStatus')->name('check-profile-status');

Route::group(['middleware' => ['auth', 'verified']], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::prefix('profile')->group(function() {
        Route::get('/', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
        Route::put('/', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
        Route::get('security', ['as' => 'profile.security', 'uses' => 'ProfileController@passwordEdit']);
        Route::put('password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
        Route::post('avatar', ['as' => 'profile.avatar.upload', 'uses' => 'ProfileController@changeProfilePicture']);
        Route::post('next_of_kin', ['as' => 'profile.nextOfKin', 'uses' => 'ProfileController@userNextOfKin']);
        Route::post('contact', ['as' => 'profile.contact', 'uses' => 'ProfileController@contact']);

    });

	Route::prefix('dashboard')->group(function() {

	    Route::prefix('subscription')->group(function () {
            Route::get('all', 'API\SubscriptionController@index');
            Route::get('add', 'API\SubscriptionController@create');
            Route::get('project/{project_slug}', 'API\ProjectController@show');
            Route::middleware('updateprofile')->post('create', 'API\SubscriptionController@store');
        });

        Route::prefix('transaction')->group(function () {
            Route::get('all', 'API\TransactionController@index');
            Route::get('add', 'API\TransactionController@create');
        });
    });
//    Route::prefix('project')->group(function () {
//        Route::get('all', 'API\ProjectController@index');
//        Route::get('add', 'API\ProjectController@create');
//    });

//    Route::prefix()
});

