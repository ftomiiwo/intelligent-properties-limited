<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->unsignedBigInteger('project_type_id')->nullable();
            $table->longText('description')->nullable();
            $table->string('slug')->nullable();
            $table->string('image')->nullable();
            $table->string('location')->nullable();
            $table->string('country')->nullable();
//            $table->double('price')->nullable();
//            $table->double('one_time_price')->nullable();
//            $table->double('initial_deposit')->default(0);
            $table->boolean('active')->default(false);
            $table->unsignedBigInteger('status_id')->nullable();
            $table->timestamps();

            $table->foreign('status_id')->references('id')->on('statuses')->onDelete('set null')->nullable();
            $table->foreign('project_type_id')->references('id')->on('project_types')->onDelete('set null')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
