<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('email')->unique();
            $table->string('phone')->nullable();
            $table->longText('address')->nullable();
            $table->enum('gender', ['Male', 'Female', 'Others'])->nullable();

            $table->date('date_of_birth')->nullable(); //-
            $table->string('nationality')->nullable(); //-
            $table->string('state_of_origin')->nullable(); //-
            $table->string('occupation')->nullable(); //-
            $table->string('avatar')->default('user.jpg'); //-
            $table->string('next_of_kin_name')->nullable(); //-
            $table->string('next_of_kin_address')->nullable(); //-
            $table->string('next_of_kin_phone')->nullable(); //-
            $table->string('paystack_customer_code')->nullable();
            $table->string('paystack_customer_id')->nullable();
            $table->boolean('phone_verified')->default(false);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->integer('created_by_user_id')->default(1);
            $table->rememberToken();
            $table->timestamps();

            // phone, gender, address
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::dropIfExists('users');
    }
}
