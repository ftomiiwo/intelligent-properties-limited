<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_plans', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('plot_id')->nullable();
            $table->unsignedBigInteger('plan_id')->nullable();
            $table->timestamps();

            $table->foreign('plot_id')->references('id')
                ->on('plots')->onDelete('set null')->nullable();
            $table->foreign('plan_id')->references('id')
                ->on('plans')->onDelete('set null')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_plans');
    }
}
