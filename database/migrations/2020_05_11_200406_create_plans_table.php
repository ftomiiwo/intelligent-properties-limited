<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable(); // 2 Years - Monthly Plan, 1 Year - Quarterly Plan,
            $table->longText('description')->nullable();
            $table->unsignedBigInteger('status_id')->default(1)->nullable();
            $table->double('years')->nullable(); // Number of years
            $table->unsignedBigInteger('interval_id')->nullable(); // 1 -> Monthly, 3 -> Quarterly
            $table->timestamps();

            $table->foreign('status_id')->references('id')->on('statuses')->onDelete('set null')->nullable();
            $table->foreign('interval_id')->references('id')->on('intervals')->onDelete('set null')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
