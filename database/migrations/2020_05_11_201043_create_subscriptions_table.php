<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('plot_id')->nullable();
            $table->unsignedBigInteger('status_id')->nullable(); // In-Progress, Completed
            $table->unsignedBigInteger('plan_id')->nullable(); // in months
            $table->double('units')->nullable(); // Number/Integer/Double
            $table->dateTime('start_date')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null')->nullable();
            $table->foreign('plot_id')->references('id')->on('plots')->onDelete('set null')->nullable();
            $table->foreign('status_id')->references('id')->on('statuses')->onDelete('set null')->nullable();
            $table->foreign('plan_id')->references('id')->on('plans')->onDelete('set null')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
