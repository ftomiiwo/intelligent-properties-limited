<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'Customer', 'Realtor', 'Super Admin'
        ];

        foreach ($roles as $role){
            $roleObject = new Role();
            $roleObject->name = $role;
            $roleObject->description = 'lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century.';
            $roleObject->save();
        }
    }
}
