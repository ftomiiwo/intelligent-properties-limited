<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Payment;

class PaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $payments = [
            [
                'user_id' => 1,
                'amount' => 25000,
                'gateway_id' => 1,
                'status_id' => 3,
                'subscription_id' => 1,
                'plot_id' => 1,
                'reference' => 'ipl-'.Str::random(5),
            ],
            [
                'user_id' => 2,
                'amount' => 50000,
                'gateway_id' => 1,
                'status_id' => 5,
                'subscription_id' => 1,
                'plot_id' => 1,
                'reference' => 'ipl-'.Str::random(5),
            ],
            [
                'user_id' => 1,
                'amount' => 100000,
                'gateway_id' => 1,
                'status_id' => 3,
                'subscription_id' => 1,
                'plot_id' => 1,
                'reference' => 'ipl-'.Str::random(5),
            ],
            [
                'user_id' => 2,
                'amount' => 75000,
                'gateway_id' => 1,
                'status_id' => 5,
                'subscription_id' => 1,
                'plot_id' => 1,
                'reference' => 'ipl-'.Str::random(5),
            ],
        ];

        foreach ($payments as $payment) {
            $paymentObject = new Payment();
            $paymentObject->user_id = $payment['user_id'];
            $paymentObject->amount = $payment['amount'];
            $paymentObject->gateway_id = $payment['gateway_id'];
            $paymentObject->status_id = $payment['status_id'];
            $paymentObject->subscription_id = $payment['subscription_id'];
            $paymentObject->plot_id = $payment['plot_id'];
            $paymentObject->reference = $payment['reference'];
            $paymentObject->save();
        }
    }
}
