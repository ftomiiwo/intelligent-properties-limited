<?php

use Illuminate\Database\Seeder;
use App\Models\ProjectType;

class ProjectTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ['Land Project', 'Building Project'];
        foreach ($types as $type) {
            $typeObject = new ProjectType();
            $typeObject->name = $type;
            $typeObject->description = $type;
            $typeObject->save();
        }
    }
}
