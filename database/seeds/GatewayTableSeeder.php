<?php

use Illuminate\Database\Seeder;
use App\Models\Gateway;

class GatewayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gateways = ['Paystack', 'Monnify'];
        foreach ($gateways as $gateway) {
            $gatewayObject = new Gateway();
            $gatewayObject->name = $gateway;
            $gatewayObject->description = 'lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown.';
            $gatewayObject->save();
        }
    }
}
