<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Status;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            'Active', 'Inactive', 'Scheduled', 'Ongoing', 'Pending', 'Completed', 'Success', 'Failed', 'Cancelled', 'Admin Completed', 'Conflict', 'Paid',
        ];

        foreach ($statuses as $status){
            $statusObject = new Status();
            $statusObject->name = $status;
            $statusObject->description = 'lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs.';
            $statusObject->save();
        }
    }
}
