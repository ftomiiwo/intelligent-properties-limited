<?php

use Illuminate\Database\Seeder;
use App\Models\Subscription;

class SubscriptionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subscriptions = [
            [
                'user_id' => 1,
                'plot_id' => 1,
                'plan_id' => 1,
                'units' => 1,
                'status_id' => 4,
            ],
            [
                'user_id' => 2,
                'plot_id' => 1,
                'plan_id' => 1,
                'units' => 1,
                'status_id' => 6,
            ],
            [
                'user_id' => 1,
                'plot_id' => 2,
                'plan_id' => 2,
                'units' => 1,
                'status_id' => 4,
            ],
        ];

        foreach ($subscriptions as $subscription) {
            $subObject = new Subscription();
            $subObject->user_id = $subscription['user_id'];
            $subObject->plot_id = $subscription['plot_id'];
            $subObject->plan_id = $subscription['plan_id'];
            $subObject->units = $subscription['units'];
            $subObject->status_id = $subscription['status_id'];
            $subObject->save();
        }
    }
}
