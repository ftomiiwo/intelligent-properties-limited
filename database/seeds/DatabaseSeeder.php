<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(StatusTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(ConfigsTableSeeder::class);
        $this->call(IntervalTableSeeder::class);
        $this->call(ProjectTypesTableSeeder::class);
        $this->call(ProjectTableSeeder::class);
        $this->call(PlotTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PlanTableSeeder::class);
        $this->call(GatewayTableSeeder::class);
//        $this->call(SubscriptionTableSeeder::class);
//        $this->call(PaymentTableSeeder::class);
//        $this->call(TransactionTableSeeder::class);
    }
}
