<?php

use Illuminate\Database\Seeder;
use App\Models\Project;
use Illuminate\Support\Str;

class ProjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $projects = [
            [
                'name' => 'Laurels Garden Phase II (Grange Park) Shimawa',
                'description' => 'Laurels Garden Shimawa',
                'project_type_id' => 1,
                'description' => 'Shimawa Grange Park is planned to be a family estate designed to have children playground,
                basketball court and fountain square.  This is to enhance communal living. It\'s located in the heart of Shimawa
                town alongside big hotels, city of David, golf estate, new RCCG Auditorium with good network of roads to access
                central business districts alausa Ikeja and Victoria Island. Grange Park location makes it easy to access Ikorodu,
                Sagamu and proposed 4th mainland bridge to link Ibeju Lekki growing business hub. It\'s a place you can build country HOME.
                It\'s appropriate location for Business owners and Retirees ',
                'image' => 'dummy.jpg',
                'location' => 'Shimawa, Lagos State',
                'country' => 'Nigeria',
                'active' => true,
                'status_id' => 1,
            ],
            [
                'name' => 'Laurels Estate Newtown Ofada Dry Land',
                'description' => 'Laurels Highbrow Estate Ibeju-Lekki',
                'project_type_id' => 1,
                'description' => 'This is a place where you can build home with gardens. It\'s rightly located on a major road to ease access to different cities centers and close to Ogun state proposed airport.
                Here you can easily join the neighborhood cities like Lagos island, Ikeja Abeokuta, and any part of the country. Benefits are: market, available power supply, train stations, proposed airport.
                This Location is Prime and we advice you  to grab opportunities along this corridor.',
                'image' => 'dummy.jpg',
                'location' => 'Ofada, Lagos State',
                'country' => 'Nigeria',
                'active' => true,
                'status_id' => 1,
            ],
        ];

        foreach ($projects as $project) {
            $proj = new Project();
            $proj->name = $project['name'];
            $proj->slug = Str::random(7);
            $proj->description = $project['description'];
            $proj->project_type_id = $project['project_type_id'];
            $proj->image = $project['image'];
            $proj->location = $project['location'];
            $proj->country = $project['country'];
            $proj->active = $project['active'];
            $proj->status_id = $project['status_id'];
            $proj->save();

        }
    }
}
