<?php

use Illuminate\Database\Seeder;

class PlotTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plots = [
            [
                'project_id' => 1,
                'name' => '600 sqm',
                'price' => 1500000,
                'one_time_price' => 1300000,
                'initial_deposit' => 200000,
                'status_id' => 1,
            ],
            [
                'project_id' => 1,
                'name' => '300 sqm',
                'price' => 800000,
                'one_time_price' => 700000,
                'initial_deposit' => 100000,
                'status_id' => 1,
            ],
            [
                'project_id' => 2,
                'name' => '600 sqm',
                'price' => 700000,
                'one_time_price' => 600000,
                'initial_deposit' => 100000,
                'status_id' => 1,
            ],
        ];

        foreach ($plots as $plot) {
            $pl = new \App\Models\Plot();
            $pl->name = $plot['name'];
            $pl->project_id = $plot['project_id'];
            $pl->price = $plot['price'];
            $pl->one_time_price = $plot['one_time_price'];
            $pl->initial_deposit = $plot['initial_deposit'];
            $pl->status_id = $plot['status_id'];
            $pl->save();
        }
    }
}
