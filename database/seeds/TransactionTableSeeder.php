<?php

use Illuminate\Database\Seeder;
use App\Models\Transaction;

class TransactionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $transactions = [
            [
                'user_id' => 1,
                'payment_id' => 1,
                'status_id' => 7,
            ],
            [
                'user_id' => 1,
                'payment_id' => 2,
                'status_id' => 3,
            ],
            [
                'user_id' => 2,
                'payment_id' => 2,
                'status_id' => 4,
            ],
            [
                'user_id' => 2,
                'payment_id' => 3,
                'status_id' => 5,
            ],
        ];

        foreach ($transactions as $transaction) {
            $transObject = new Transaction();
            $transObject->user_id = $transaction['user_id'];
            $transObject->payment_id = $transaction['payment_id'];
            $transObject->status_id = $transaction['status_id'];
            $transObject->save();
        }
    }
}
