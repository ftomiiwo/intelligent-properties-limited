<?php

use Illuminate\Database\Seeder;
use App\Models\Plan;

class PlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = [
            [
                'name' => 'Outright Payment',
                'description' => 'Payments on are made on the subscription plan monthly.',
                'status_id' => 1,
                'interval_id' => 1,
                'years' => 1,
            ],
            [
                'name' => '1 Year - Monthly Plan',
                'description' => 'Payments on are made on the subscription plan monthly.',
                'status_id' => 1,
                'interval_id' => 1,
                'years' => 1,
            ],
            [
                'name' => '1 Year - Quarterly Plan',
                'description' => 'Payments on are made on the subscription plan once in 3 months.',
                'status_id' => 1,
                'interval_id' => 2,
                'years' => 1,
            ],
            [
                'name' => '1 Year - Bi-Annual Plan',
                'description' => 'Payments on are made on the subscription plan once in 3 months.',
                'status_id' => 2,
                'interval_id' => 3,
                'years' => 1,
            ]
        ];

        foreach ($plans as $plan) {
            $planObject = new Plan();
            $planObject->name = $plan['name'];
            $planObject->description = $plan['description'];
            $planObject->interval_id = $plan['interval_id'];
            $planObject->years = $plan['years'];
            $planObject->save();
        }

    }
}
