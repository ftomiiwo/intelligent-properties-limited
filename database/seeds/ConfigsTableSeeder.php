<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Config;

class ConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $configs = [
            [
                'name' => 'paystack_secret_key',
                'value' => 'sk_test_4f9a12fc53f674f1e91998e8063d9ae24ff66627',
                'description' => 'Paystack Secret Key',
            ],
            [
                'name' => 'paystack_public_key',
                'value' => 'pk_test_cf05cddca00b3043f363676f12821f4b22f4fbd0',
                'description' => 'Paystack Public Key',
            ],
            [
                'name' => 'site_email',
                'value' => 'info@intelligenceproperties.com',
                'description' => 'The General Site Email',
            ],
            [
                'name' => 'site_name',
                'value' => 'Intelligence Global Properties',
                'description' => 'The General Site Name',
            ],
            [
                'name' => 'sendgrid_api_key',
                'value' => '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',
                'description' => 'Sendgrid API key, useful for sending emails from the site.',
            ]
        ];

        foreach ($configs as $config){
            $configObject = new Config();
            $configObject->name = $config['name'];
            $configObject->value = $config['value'];
            $configObject->description =  $config['description'];
            $configObject->save();
        }
    }
}
