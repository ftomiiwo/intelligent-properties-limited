<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Http\Controllers\UserController;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Tomiiwo Fakinlede',
                'email' => 'ftomiiwo@gmail.com',
                'phone' => '08153731891',
                'address' => '10a Leo Street, Akure, Ondo State',
                'gender' => 'Male',
                'status_id' => 1,
                'password' => 'coldsugar',
                'role_id' => 3
            ],
            [
                'name' => 'Atfak Tech',
                'email' => 'tomiiwo.fakinlede@farmcrowdy.com',
                'phone' => '08137288381',
                'address' => '10a Leo Street, Akure, Ondo State',
                'gender' => 'Male',
                'status_id' => 1,
                'password' => 'coldsugar',
                'role_id' => 1
            ],
            [
                'name' => 'Odeniyi Adefolaoluwami',
                'email' => 'tomiiwo.f@gmail.com',
                'phone' => '08137288381',
                'address' => '10a Leo Street, Akure, Ondo State',
                'gender' => 'Male',
                'status_id' => 2,
                'password' => 'coldsugar',
                'role_id' => 1
            ],
        ];

        foreach ($users as $user) {
            $userObject = new User();
            $paystack = new UserController();
            $pk = $paystack->createPaystackUser($user['email']);
            $customer_code = $pk->customer_code;
            $paystack_customer_id = $pk->id;

            $userObject->name = $user['name'];
            $userObject->email = $user['email'];
            $userObject->phone = $user['phone'];
            $userObject->address = $user['address'];
            $userObject->gender = $user['gender'];
            $userObject->status_id = $user['status_id'];
            $userObject->paystack_customer_code = $customer_code;
            $userObject->paystack_customer_id = $paystack_customer_id;
            $userObject->password = $userObject::protectPassword($user['password']);
            $userObject->role_id = $user['role_id'];
            $userObject->save();
        }

    }
}
