<?php

use Illuminate\Database\Seeder;
use App\Models\Interval;

class IntervalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $intervals = [
            [
                'name' => 'Monthly',
                'months' => 1,
            ],
            [
                'name' => 'Quarterly',
                'months' => 3,
            ],
            [
                'name' => 'Bi-Annually',
                'months' => 6,
            ],
            [
                'name' => 'Annually',
                'months' => 12,
            ],
        ];

        foreach ($intervals as $interval) {
            $intervalObject = new Interval();
            $intervalObject->name = $interval['name'];
            $intervalObject->months = $interval['months'];
            $intervalObject->save();
        }
    }
}
